# Install Hamachi (mostly) portable

## Download and extract the MSI

Download the newest Hamachi MSI from the offical site
<https://secure.logmein.com/hamachi.msi>

Extract the MSI with the following command
> msiexec /a hamachi.msi /qb TARGETDIR=C:\Temp\Hamachi

(or into a path of your desires)

After that you have a new folder `C:\Temp\Hamachi\PFiles\LogMeIn Hamachi\`. In this folder you need only the following six files:

**x64**

```bash
hamachi.lng
hamachi-2-ui.exe
x64\
    hamachi-2.exe
    hamdrv.cat
    hamdrv.inf
    hamdrv.sys
```

#### x86 (not tested)

```bash
hamachi.lng
hamachi-2-ui.exe
hamachi-2.exe
hamdrv.cat
hamdrv.inf
hamdrv.sys
```

If you are using Windows 8 or earlier, you need `hamachi.cat/inf/sys` insted of `hamdrv.cat/inf/sys`.

Somehow the language files are sometimes in chinese, but after the second start it was magically the system language. To be sure, I added the English language file.

<b>Copy the files to the path for your programs or any path you like</b><br>
`P:\Hamachi\` in my case.

## Create a ("Legacy") Network Adapter

### This is the part that i dont now how to automate

1. Open the Device Manager (hdwwiz.cpl or devmgmt.msc) and click on `Network adapters`
2. Click on `Action` and `Add legacy hardware`
3. In the wizard click `Next`, then `Install the hardware that i manually select from a list (Advanced)` and again on `Next`
4. In the list select `Network adapters` and again click `Next`
5. Now click on `Have Disk...` and in the popup window on `Browse...`
6. Here navigate to your Hamachi files - in my case `P:\Hamachi\` - and select the only displayed file `hamdrv.inf`
7. Again in the popup window press `OK`
8. The popup window closes and in thex previous window there is now the Model `LogMeIn Hamachi Virtual Ethernet Adapter`
9. Press `Next` and again `Next` to finally install the Ethernet Adapter
10. In the last Window it should be saying `The following hardware was installed: LogMeIn... Windows has finished installing the software for this device`. Press `Finish`

![installation gif](hamachi_portable_network_adapter.gif)

In the Network Connections (Control Panel\Network and Internet\Network Connections) there should now be new `Ethernet <Nr>` Adapter. You should rename it to `Hamachi`!

## Create the Hamachi Service

Next you need to create the Hamachi Service `Hamachi2Svc`

Open a new powershell window and execute the following command:

```powershell
New-Service -Name "Hamachi2Svc" -DisplayName "LogMeIn Hamachi Tunneling Engine" -BinaryPathName '"P:\Hamachi\LogMeIn Hamachi\x64\hamachi-2.exe" -s' -StartupType Automatic
```

Start the Service

```powershell
Start-Service -Name "Hamachi2Svc"
```

## Start and use Hamachi

Now you can start and use Hamachi normally by simply double clicking on `hamachi-2-ui.exe`.

Disable the auto-update feature!

## What does not work?

Nothing that affects the function (From my testings)

- The diagnostic tool under Help says that the drivers are not found and you need to reinstall Hamachi - You can ignore this error
- I have not tested the auto-update function - whether everything still works after an update
