﻿[CmdletBinding()]
param 
(
    [Parameter()]
    [Alias("u")]
    [switch] $uninstall
)

if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) { Start-Process powershell.exe "-NoProfile -ExecutionPolicy Bypass -File `"$PSCommandPath`"" -Verb RunAs; exit }

#! --- parameter and paths
# pPath = portable Path
$pPath = "C:\hamachi"

# svcfg = service configuration path - maybe it would be better to set this  into subdirectory of the pPath via Registry
$svcfg = "C:\Windows\ServiceProfiles\LocalService\AppData\Local\LogMeIn Hamachi"

# drvpath # to get rid of diagnostic tool error provided in UI 
$drvpath ="C:\Program Files (x86)\LogMeIn Hamachi\x64"

$msiURL = "https://secure.logmein.com/hamachi.msi"
$devconURL = "https://gitlab.com/simonlichtlein/infrastructure/-/raw/main/windows/bin/devcon.exe?ref_type=heads&inline=false"
$lngURL= "https://gitlab.com/simonlichtlein/infrastructure/-/blob/main/windows/tools/hamachi_portable/hamachi.lng"
$ecfgURL = "https://gitlab.com/simonlichtlein/infrastructure/-/blob/main/windows/tools/hamachi_portable/h2-engine.cfg"
$owncfg = "$pPath\config"

#! --- Start of Script ---

Write-Host "------------------------------------"
Write-Host "- Starting Hamachi Protable Setup - "
Write-Host "------------------------------------"
Write-Host 

if ($uninstall) 
{
    Write-Host " - uninstalling hamachi -"
    Write-Host 
    
    $hamachiProcess = Get-Process -Name "hamachi-2-ui" -ErrorAction SilentlyContinue
    if ($hamachiProcess) 
    {
        $hamachiProcess | Stop-Process -Force
        Write-Host "hamachi-2-ui process stopped."
    } 
    else 
    {
        Write-Host "hamachi-2-ui process not found."
    }
    
    #Get-Process "hamachi-2" | Stop-Process -Force  => not necessary

    Write-Host "stopping service"
    Stop-Service -DisplayName "LogMeIn Hamachi Tunneling Engine" -Force

    Write-Host "deleting service"
    $proc = Start-Process -FilePath "sc.exe" -ArgumentList "delete Hamachi2Svc" -WindowStyle Hidden -Wait -PassThru
    if ($proc.ExitCode -eq 0) 
    {
        Write-Host "service deleted"
    }
    else 
    {
        Write-Host "service could not be deleted"
        Exit 1
    }

    Write-Host "removing hamachi network adatapter"
    $devconParams = 'remove hamachi'
    $proc = Start-Process -Filepath "$pPath\devcon.exe" -Argumentlist $devconParams -WindowStyle Hidden -Wait -PassThru
    if ($proc.ExitCode -eq 0) 
    {
        Write-Host "  driver uninstallation successfull"
    }
    else 
    {
        Write-Host "  driver uninstallation failed"
        Exit 1
    }

    Write-Host "cleanup filesystem & registry"
    #not used if portable app sure if next command necessary
    #Remove-Item -Recurse -Force "C:\Windows\System32\DriverStore\FileRepository\LogMeIn Hamachi"
    #CUsersDefaultAppDataLocal not used if portable app
    Remove-Item -Recurse -Force -Path "$env:LOCALAPPDATA\LogMeIn Hamachi"
    Remove-Item -Recurse -Force -Path $pPath
    Remove-Item -Recurse -Force -Path $svcfg 
    Remove-Item -Recurse -Force -Path $drvpath
    $regKey = "HKLM:\SOFTWARE\WOW6432Node\LogMeIn Hamachi"
    if (Test-Path -Path $regKey) 
    {
        Remove-Item -Path $regKey -Recurse -Force
        Write-Host "Registry key '$regKey' deleted successfully."
    } 
    else 
    {
        Write-Host "Registry key '$regKey' not found."
    }

    Write-Host " - hamachi portable has been successfully removed" -ForegroundColor Red
    Write-Host "Please press Enter to exit"
    [void][System.Console]::ReadLine()
    
}
else 
{
    Write-Host " - installing hamachi -"
    Write-Host
    Write-Host "creating portable directory $pPath"

    if (!(Test-Path -Path $pPath)) 
    {   
        New-Item -Path $pPath -ItemType Directory -Force | Out-Null
    }
    else 
    {
        Write-Host "directory already exists - use with care"
    }

    #* PowerShell 5 doesn't handle the progressbar correctly so the download is extremly slow -> we change the Progressbar Preference
    Write-Host "getting hamachi"
    try 
    {
        $ProgressPreference = 'SilentlyContinue'
        Write-Host "  downloading..."
        Invoke-WebRequest -Uri $msiURL -OutFile "$pPath\hamachi.msi" -ErrorAction Stop
        Write-Host "  download successfull to $pPath\hamachi.msi"
        $ProgressPreference = 'Continue'
    }
    catch 
    {
        Write-Host "  failed to download the file, check your internetconnection or update the Variable msiURL. Error: $_"
        Exit 1
    }

    Write-Host "  extracting msi"
    $msiParams = "/a `"$pPath\hamachi.msi`" /qn TARGETDIR=`"$pPath\extracted`""

    $proc = Start-Process -FilePath "msiexec.exe" -ArgumentList $msiParams -WindowStyle Hidden -Wait -PassThru
    if ($proc.ExitCode -eq 0) 
    {
        Write-Host "  extraction successfull"
    }
    else 
    {
        Write-Host "  extraction failed"
        Exit 1
    }

    Write-Host "  moving only needed files from extracted dir"
    Move-Item -Path "$pPath\extracted\PFiles\LogMeIn Hamachi\hamachi-2-ui.exe" -Destination $pPath -Force
    Move-Item -Path "$pPath\extracted\PFiles\LogMeIn Hamachi\x64\hamachi-2.exe" -Destination $pPath -Force
    <#
    Move-Item -Path "$pPath\extracted\PFiles\LogMeIn Hamachi\x64\hamdrv.cat" -Destination $pPath -Force
    Move-Item -Path "$pPath\extracted\PFiles\LogMeIn Hamachi\x64\hamdrv.inf" -Destination $pPath -Force
    Move-Item -Path "$pPath\extracted\PFiles\LogMeIn Hamachi\x64\hamdrv.sys" -Destination $pPath -Force
    #>

    ####### NEW ######### u need this files to get rid of the error occuring in builtin diagnostic tool, see comment above
    
    $drvpath ="C:\Program Files (x86)\LogMeIn Hamachi\x64" 
    New-Item $drvpath -ItemType Directory -Force | Out-Null
    Move-Item -Path "$pPath\extracted\PFiles\LogMeIn Hamachi\x64\hamdrv.cat" -Destination $drvpath -Force
    Move-Item -Path "$pPath\extracted\PFiles\LogMeIn Hamachi\x64\hamdrv.inf" -Destination $drvpath -Force
    Move-Item -Path "$pPath\extracted\PFiles\LogMeIn Hamachi\x64\hamdrv.sys" -Destination $drvpath -Force
    Move-Item -Path "$pPath\extracted\PFiles\LogMeIn Hamachi\x64\hamachi.cat" -Destination $drvpath -Force
    Move-Item -Path "$pPath\extracted\PFiles\LogMeIn Hamachi\x64\hamachi.inf" -Destination $drvpath -Force
    Move-Item -Path "$pPath\extracted\PFiles\LogMeIn Hamachi\x64\hamachi.sys" -Destination $drvpath -Force
    #### NEW END ######

    Write-Host "  cleaning up extracted dir"
    Remove-Item -Path "$pPath\extracted" -Recurse -Force
    Write-Host

    Write-Host "installing driver with devcon"
    Write-Host "  getting devcon.exe"
    try 
    {
        $ProgressPreference = 'SilentlyContinue'
        Write-Host "  downloading.."
        Invoke-WebRequest -Uri $devconURL -OutFile "$pPath\devcon.exe" -ErrorAction Stop
        Write-Host "  download successfull to $pPath\devcon.exe"
        $ProgressPreference = 'Continue'
    }
    catch 
    {
        Write-Host "  failed to download the file, check your internetconnection or update the Variable devconURL. Error: $_"
        Exit 1
    }

    Write-Host "  starting driver installation.."
    #$devconParams = "install `"$pPath\hamdrv.inf`" hamachi"
    $devconParams = "install `"$drvpath\hamdrv.inf`" hamachi"
    $proc = Start-Process -Filepath "$pPath\devcon.exe" -Argumentlist $devconParams -WindowStyle Hidden -Wait -PassThru
    if ($proc.ExitCode -eq 0) 
    {
        Write-Host "  driver installation successfull"
    }
    else 
    {
        Write-Host "  driver installation failed"
        Exit 1
    }
    
    # changing network apdater name to hamachi & set networkconnectiontype to private
    $timeout = 70  # Timeout in seconds needs approx. 1min 
    $elapsed = 0
    $interval = 1  # Check interval in seconds
    
    while ($elapsed -lt $timeout) 
    {
        $networkProfiles = Get-NetConnectionProfile
        $identifyingProfiles = $networkProfiles | Where-Object { $_.Name -match "Netzwerkidentifizierung..." -or $_.Name -match "Identifying..." }
    
        if ($identifyingProfiles.Count -eq 0) 
        {
            Write-Host "No network profiles are in the 'Identifying' state."
            break
        } else 
        {
            #Write-Host "Network is still identifying. Waiting..."
            Write-Progress -Activity "waiting for completion of network identifying..." -Status "$elapsed% Complete:" -PercentComplete $elapsed
        }
    
        Start-Sleep -Seconds $interval
        $elapsed += $interval
    }
    
    if ($elapsed -ge $timeout) 
    {
        Write-Host "Timeout reached. Network is still in 'Identifying' state."
    } 
    else 
    {
        write-host "renaming NetAdapter to Hamachi and setting networktype to private"
        (get-netadapter | where InterfaceDescription -Match "Hamachi" | Rename-NetAdapter -NewName Hamachi)
        Set-NetConnectionProfile -InterfaceAlias "Hamachi" -NetworkCategory Private


        Write-Host "copy config"

        if (Test-Path -Path $svcfg) 
        {   
            Remove-Item -Recurse -Force $svcfg    
        }
        New-Item $svcfg -ItemType Directory -Force | Out-Null

        # if no config files were found use plain ones | maybe change to pPath & move files

        if (Test-Path -Path $owncfg) 
        {
            Move-Item -Path "$owncfg\h2-engine.cfg" -Destination "$svcfg\h2-engine.cfg" -Force
            Move-Item -Path "$owncfg\h2-engine.ini" -Destination "$svcfg\h2-engine.ini" -Force
            Move-Item -Path "$owncfg\hamachi.lng" -Destination "$pPath\hamachi.lng" -Force
        }

        else 
        {
        
            Write-Host "  getting hamachi.lng"
            try 
            {
                $ProgressPreference = 'SilentlyContinue'
                Write-Host "  downloading.."
                Invoke-WebRequest -Uri $lngURL -OutFile "$pPath\hamachi.lng" -ErrorAction Stop
                Write-Host "  download successfull to $pPath\hamachi.lng"
                $ProgressPreference = 'Continue'
            }
            catch 
            {
                Write-Host "  failed to download the file, check your internetconnection or update the Variable lngURL. Error: $_"
                Exit 1
            }
        
            # use parameter /not public
       
            Write-Host "  getting h2-engine.cfg"
            try 
            {
                $ProgressPreference = 'SilentlyContinue'
                Write-Host "  downloading.."
                Invoke-WebRequest -Uri $ecfgURL -OutFile "$svcfg\h2-engine.cfg" -ErrorAction Stop
                Write-Host "  download successfull to $svcfg\h2-engine.cfg"
                $ProgressPreference = 'Continue'
            }
            catch 
            {
                Write-Host "  failed to download the file, check your internetconnection or update the Variable lngURL. Error: $_"
                Exit 1
            }
        }

        Write-Host "creating Hamachi Service"
        New-Service -Name "Hamachi2Svc" -DisplayName "LogMeIn Hamachi Tunneling Engine" -BinaryPathName "`"$pPath\hamachi-2.exe`" -s" -StartupType Automatic

        Write-Host "starting hamachi service" 
        $proc = Start-Process -FilePath "sc.exe" -ArgumentList "start Hamachi2Svc" -WindowStyle Hidden -Wait -PassThru
        if ($proc.ExitCode -eq 0) 
        {
         Write-Host "  service started"
        }
        else 
        {
            Write-Host "  service could not be started"
            Exit 1
        }
    }
    Write-Host " - hamachi portable has been successfully deployed" -ForegroundColor Green
    Write-Host "Please press Enter to exit"
    [void][System.Console]::ReadLine()
}
# Das ist grad geil wenn man breit ist - Max Baumann 13.11.2024 