# check if winget is installed
if (-not (Get-Command winget -ErrorAction SilentlyContinue)) {
    PrintC -c "DarkGray" -t "    winget is not installed.. installing with dependencies.."
    
    $appx = @(
        "Microsoft.VCLibs.140.00.UWPDesktop_14.0.33728.0_x64__8wekyb3d8bbwe.Appx", 
        "Microsoft.VCLibs.140.00_14.0.33519.0_x64__8wekyb3d8bbwe.Appx", 
        "Microsoft.UI.Xaml.2.8_8.2310.30001.0_x64__8wekyb3d8bbwe.Appx"
    )

    foreach ($app in $appx) {
        Invoke-WebRequest -Uri "https://gitlab.com/simonlichtlein/infrastructure/-/raw/main/windows/bin/$app" -OutFile "$app"
        Add-AppxPackage $app
        Remove-Item $app
    }
    
    Invoke-WebRequest -Uri https://aka.ms/getwinget -OutFile Microsoft.DesktopAppInstaller_8wekyb3d8bbwe.msixbundle
    Add-AppxPackage Microsoft.DesktopAppInstaller_8wekyb3d8bbwe.msixbundle
    Remove-Item Microsoft.DesktopAppInstaller_8wekyb3d8bbwe.msixbundle
}

PrintC -c "DarkGray" -t "    removing msstore source from winget sources"
Start-Process "winget" -ArgumentList "source remove msstore" -Wait -NoNewWindow 
PrintC

PrintC -c "DarkGray" -t "    installing Microsoft VCRedist 2015+ x86 / x64"
Start-Process "winget" -ArgumentList "install --id Microsoft.VCRedist.2015+.x86 --silent" -Wait -NoNewWindow 
Start-Process "winget" -ArgumentList "install --id Microsoft.VCRedist.2015+.x64 --silent" -Wait -NoNewWindow 

PrintC -c "DarkGray" -t "    installing Microsoft .NET Runtimes"
# https://dotnet.microsoft.com/en-us/platform/support/policy/dotnet-core
# EOL December 13, 2022
# winget install --id Microsoft.DotNet.Runtime.3_1 --silent | Out-Null
# winget install --id Microsoft.DotNet.DesktopRuntime.3_1 --silent | Out-Null
# EOL May 10, 2022
# winget install --id Microsoft.DotNet.Runtime.5 --silent | Out-Null
# winget install --id Microsoft.DotNet.DesktopRuntime.5 --silent | Out-Null
# EOL November 12, 2024
# Start-Process "winget" -ArgumentList "install --id Microsoft.DotNet.Runtime.6 --silent" -Wait -NoNewWindow 
# Start-Process "winget" -ArgumentList "install --id Microsoft.DotNet.DesktopRuntime.6 --silent" -Wait -NoNewWindow 
# EOL May 14, 2024
# winget install --id Microsoft.DotNet.Runtime.7 --silent | Out-Null
# winget install --id Microsoft.DotNet.DesktopRuntime.7 --silent | Out-Null

# EOL November 10, 2026
Start-Process "winget" -ArgumentList "install --id Microsoft.DotNet.Runtime.8 --silent" -Wait -NoNewWindow 
Start-Process "winget" -ArgumentList "install --id Microsoft.DotNet.DesktopRuntime.8 --silent" -Wait -NoNewWindow 
# EOL May 12, 2026
Start-Process "winget" -ArgumentList "install --id Microsoft.DotNet.Runtime.9 --silent" -Wait -NoNewWindow 
Start-Process "winget" -ArgumentList "install --id Microsoft.DotNet.DesktopRuntime.9 --silent" -Wait -NoNewWindow 

PrintC -c "DarkGray" -t "    installing 7zip"
Start-Process "winget" -ArgumentList "install --id 7zip.7zip --silent" -Wait -NoNewWindow 
PrintC -c "DarkGray" -t "    installing K-Lite CodecPack with Player"
Start-Process "winget" -ArgumentList "install --id CodecGuide.K-LiteCodecPack.Standard --silent" -Wait -NoNewWindow 
PrintC -c "DarkGray" -t "    installing Notepad++"
Start-Process "winget" -ArgumentList "install --id Notepad++.Notepad++ --silent" -Wait -NoNewWindow 
PrintC -c "DarkGray" -t "    installing Windows Terminal (if not already installed)"
Start-Process "winget" -ArgumentList "install --id Microsoft.WindowsTerminal --silent" -Wait -NoNewWindow 
PrintC -c "DarkGray" -t "    installing PowerShell 7"
Start-Process "winget" -ArgumentList "install --id Microsoft.PowerShell --silent" -Wait -NoNewWindow 

PrintC -c "DarkGray" -t "    installing Brave Browser"
Start-Process "winget" -ArgumentList "install --id Brave.Brave --silent" -Wait -NoNewWindow 

foreach ($program in $global:PIconfig.programs) {
    PrintC -c "DarkGray" -t "    installing $($program)"
    Start-Process "winget" -ArgumentList "install --id $program --silent" -Wait -NoNewWindow 
}

PrintC -c "Gray" -t "  installing windows features"
foreach ($feature in $global:PIconfig.windowsfeatures) {
    switch ($feature) {
        "NFS" {
            PrintC -c "DarkGray" -t "    enable NFS Client"
            Enable-WindowsOptionalFeature -FeatureName ServicesForNFS-ClientOnly, ClientForNFS-Infrastructure -Online -NoRestart
        }
        "VMHost" {
            PrintC -c "DarkGray" -t "    enable Virtual Machine Platform"
            Enable-WindowsOptionalFeature -FeatureName VirtualMachinePlatform -Online -NoRestart
        }
        "WSL" {
            PrintC -c "DarkGray" -t "    installing AMD Chipset drivers"
            Enable-WindowsOptionalFeature -FeatureName Microsoft-Windows-Subsystem-Linux -Online -NoRestart
        }
        default {
            PrintC -c "Red" -t "    $feature not available via post-install the moment - please request it"
        }
    }
}

<#
# TODO installing drivers
PrintC -c "Gray" -t "  installing drivers"
foreach ($driver in $global:PIconfig.drivers) {
    switch ($driver) {
        "Nvidia.Graphics" {
            PrintC -c "DarkGray" -t "    installing NVIDIA Graphics drivers"
            # Invoke-Expression "& '$PSScriptRoot\drivers\nvidia.ps1'"
        }
        "AMD.Chipset" {
            PrintC -c "DarkGray" -t "    installing AMD Chipset drivers"
            # Invoke-Expression "& '$PSScriptRoot\drivers\intel.ps1'"
        }
        default {
            PrintC -c "Red" -t "    $driver not available via post-install the moment - please request it"        
        }
    }
}



# update store apps 
PrintC -c "DarkGray" -t "    start update progress for store apps"
Get-CimInstance -Namespace "Root\cimv2\mdm\dmmap" -ClassName "MDM_EnterpriseModernAppManagement_AppManagement01" | Invoke-CimMethod -MethodName UpdateScanMethod
#>