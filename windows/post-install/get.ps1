$location = "C:\post-install"
$repo = "simonlichtlein/infrastructure"
$branch = "main"
$folder = "windows/post-install"

$apiUrl = "https://gitlab.com/api/v4/projects/$( [uri]::EscapeDataString($repo) )/repository/tree?path=$folder&ref=$branch"
$rawUrlBase = "https://gitlab.com/$repo/-/raw/$branch"

$response = Invoke-RestMethod -Uri $apiUrl -Method Get

if (!(Test-Path $location)) { 
    New-Item -ItemType Directory -Path $location | Out-Null 
} 
Set-Location $location

# disable progressbar status because of bad handling of multiple actions #fuckMS
$ProgressPreference = "SilentlyContinue"

foreach ($file in $response) {
    $fileName = $file.name
    $filePath = "$folder/$fileName" -replace " ", "%20" # Ensure spaces are encoded
    $fileUrl = "$rawUrlBase/$filePath"
    $destinationPath = Join-Path -Path $location -ChildPath $fileName

    Write-Output "Downloading $fileName..."
    Invoke-WebRequest -Uri $fileUrl -OutFile $destinationPath
}

Write-Output "Download completed!"

Start-Process -FilePath "$location\_start.bat"
Exit