# post-install for Windows 11

# how to use
1. change the `config.json` file to your needs
2. start the script with double-click on `_start.bat`
3. follow the instructions, if any appears (like UAC or connection to the internet)
4. enjoy 😇

## `config.json`
The file needs to be in the same directory as the script and named `config.json`
```json
{
    "tweaks": [
        {
            "explorer": [
                {
                    "showDesktopFolder": true
                },
                {
                    "showDocumentsFolder": true
                },
                {
                    "showDownloadsFolder": true
                },
                {
                    "showMusicFolder": false
                },
                {
                    "showVideosFolder": false
                },
                {
                    "showPicturesFolder": true
                }
            ],
            "taskbar": [
                {
                    "hideSearchButton": true
                },
                {
                    "hideTaskViewButton": true
                },
                {
                    "hideCopilotButton": true
                },            
                {
                    "hideWidgetsButton": true
                },
                {
                    "alignLeft": true
                }
            ]
        }
    ],
    "xboxfeatures": false,
    "windowsfeatures": [
        "NFS",
        "VMHost",
        "WSL"
    ],
    "programs": [
        "Microsoft.PowerToys",
        "Microsoft.VisualStudioCode"
        "any other programm what is available in the winget repository - please use the 'id'"
    ]
}
```
### tweaks
Many tweaks are default activated via the `tweaks.ps1` script. 
In this section you can define some tweaks that i think should be left to the user.

#### explorer
showXXXFolder with this you can enable or disable the appearance of the named Folders in the "This-PC" view in your Explorer
I personally like the `Documents`, `Downloads` and `Desktop` folders to be shown for quick and easy access.

#### taskbar
hideXXXButton with this you can hide the named buttons from the taskbar.

### windowsfeatures
The only currently supported features are:
- `NFS` ≙ `ServicesForNFS-ClientOnly`, `ClientForNFS-Infrastructure`
- `VMHost`  ≙ `VirtualMachinePlatform`
- `WSL` ≙ `WindowsSubsystemForLinux`

### xboxfeatures
`xboxfeatures` set to `true` only works with non-LTSC Windows Editions.

### programs

# Tested on Windows 11
- ✅ IoT Enterprise LTSC 2024 (24H2)
- ✅ Enterprise LTSC 2024 (24H2)
- ✅ Enterprise 24H2

## this does not work on 
❌ Windows 10
