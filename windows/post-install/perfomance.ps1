# PrintC -c "Gray" -t "  disabling scheduled tasks"
$scheduledTasks = @(
    # "Microsoft\Windows\Autochk\Proxy",
)
foreach ($task in $scheduledTasks) {    
    if ((Get-ScheduledTask -TaskName $task -ErrorAction SilentlyContinue)) {
        PrintC -c "DarkGray" -t "    disabling $task"
        schtasks /Change /TN $task /Disable
    }
    else {
        PrintC -c "yellow" -t "TODO task '$task' doesnt exist"

    }
}

function setServices {
    param (
        [string]$startuptype,
        [array]$services
    )

    foreach ($service in $services) {
        # PrintC -c "DarkGray" -t "    $service"
        Set-Service -Name $service -StartupType $startuptype -ErrorAction Continue
    }
}

PrintC -c "Gray" -t "  set services to manual startup"
$servicesmanual = @(
    'ALG' 
    # 'AppIDSvc' access denied
    'AppMgmt' 
    'AppReadiness' 
    # 'AppXSvc' access denied 
    'Appinfo' 
    'AxInstSV' 
    'BDESVC' 
    'BTAGService' 
    # 'BcastDVRUserService_*' TODO not found
    # 'Browser' not found
    'BITS' 
    'COMSysApp' 
    # 'CaptureService_*' TODO not found 
    'CertPropSvc' 
    # 'ClipSVC' access denied 
    # 'ConsentUxUserSvc_*' TODO not found
    'CDPSvc' 
    'CscService' 
    # 'DcpSvc' not found
    'DevQueryBroker' 
    # 'DeviceAssociationBrokerSvc_*' TODO not found
    'DeviceAssociationService' 
    'DeviceInstall' 
    # 'DevicePickerUserSvc_*' TODO not found
    # 'DevicesFlowUserSvc_*' TODO not found
    'DisplayEnhancementService' 
    'DmEnrollmentSvc' 
    # 'DoSvc' access dnied 
    'DsSvc' 
    'DsmSvc' 
    'EFS' 
    'EapHost' 
    # 'EntAppSvc' access denied
    'FDResPub' 
    # 'Fax' not found
    'FrameServer' 
    'FrameServerMonitor' 
    'GraphicsPerfSvc' 
    # 'HomeGroupListener' not found
    # 'HomeGroupProvider' not found
    'HvHost' 
    # 'IEEtwCollectorService' not found
    'IKEEXT' 
    'InstallService' 
    'InventorySvc' 
    'IpxlatCfgSvc' 
    'LicenseManager' 
    'LxpSvc' 
    'MSDTC' 
    'MSiSCSI' 
    'MapsBroker' 
    'McpManagementService' 
    # 'MessagingService_*' TODO not found
    # 'MicrosoftEdgeElevationService' not found
    # 'MixedRealityOpenXRSvc' not found 
    'MsKeyboardFilter' 
    # 'NPSMSvc_*' TODO not found
    'NaturalAuthentication' 
    'NcaSvc' 
    'NcbService' 
    'NcdAutoSetup' 
    'NetSetupSvc' 
    'Netman' 
    # 'NgcCtnrSvc' access denied
    # 'NgcSvc' access denied
    'NlaSvc' 
    # 'P9RdrService_*' TODO not found
    # 'PNRPAutoReg' not found
    # 'PNRPsvc' not found
    'PcaSvc' 
    'PeerDistSvc' 
    # 'PenService_*' TODO not found 
    'PerfHost' 
    'PhoneSvc' 
    # 'PimIndexMaintenanceSvc_*' TODO not found
    'PlugPlay' 
    'PolicyAgent' 
    'PrintNotify' 
    # 'PrintWorkflowUserSvc_*' TODO not found
    'PushToInstall' 
    'QWAVE' 
    'RasAuto' 
    'RasMan' 
    'RetailDemo' 
    'RmSvc' 
    'SCPolicySvc' 
    'SCardSvr' 
    'SDRSVC'  
    'SEMgrSvc' 
    'SNMPTRAP' 
    'SNMPTrap' 
    'SSDPSRV' 
    'ScDeviceEnum' 
    # 'SecurityHealthService' access denied
    # 'Sense' access denied0
    'SensorDataService' 
    'SensorService' 
    'SensrSvc' 
    'SessionEnv' 
    'SharedAccess' 
    # 'SharedRealitySvc' not found
    'SmsRouter' 
    'SstpSvc' 
    # 'StateRepository' access denied
    'StiSvc' 
    'StorSvc' 
    # 'TabletInputService' not found
    'TapiSrv' 
    'VSS' 
    # 'VacSvc' not found
    'W32Time' 
    'WEPHOSTSVC' 
    'WFDSConMgrSvc' 
    'WMPNetworkSvc' 
    'WManSvc' 
    'WPDBusEnum' 
    # 'WSService' not found
    'WSearch' 
    # 'WaaSMedicSvc' access denied
    'WalletService' 
    'WarpJITSvc' 
    'WbioSrvc' 
    # 'TextInputManagementService' access denied
    'TieringEngineService' 
    # 'TimeBroker' not found
    # 'TimeBrokerSvc' access denied
    'TokenBroker' 
    'TroubleshootingSvc' 
    'TrustedInstaller' 
    # 'UI0Detect' not found
    # 'UdkUserSvc_*' TODO not found
    'UmRdpService' 
    # 'UnistoreSvc_*' TODO not found
    # 'UserDataSvc_*' TODO not found
    'UsoSvc' 
    # 'WcsPlugInService' not found
    # 'WdNisSvc' access denied
    'WdiServiceHost' 
    'WdiSystemHost' 
    'WebClient' 
    'Wecsvc' 
    'WerSvc' 
    'WiaRpc' 
    # 'WinHttpAutoProxySvc' access denied
    'WinRM' 
    'WpcMonSvc' 
    'WpnService' 
    'WwanSvc' 
    'XblAuthManager' 
    'XblGameSave' 
    'XboxGipSvc' 
    'XboxNetApiSvc' 
    'autotimesvc' 
    'bthserv' 
    'camsvc' 
    # 'cbdhsvc_*' TODO not found
    'cloudidsvc' 
    'dcsvc' 
    'defragsvc' 
    # 'diagnosticshub.standardcollector.service' not found
    'diagsvc' 
    'dmwappushservice' 
    'dot3svc' 
    # 'edgeupdate' not found
    # 'edgeupdatem' not found
    # 'embeddedmode' access denied
    'fdPHost' 
    'fhsvc' 
    'hidserv' 
    'icssvc' 
    'lfsvc' 
    'lltdsvc' 
    'lmhosts' 
    # 'msiserver' access denied
    'netprofm' 
    # 'p2pimsvc' not found
    # 'p2psvc' not found
    'perceptionsimulation' 
    'pla' 
    'seclogon' 
    'smphost' 
    # 'spectrum' not found
    # 'sppsvc' access denied
    'svsvc' 
    'swprv' 
    'upnphost' 
    'vds' 
    # 'vm3dservice' not found
    'vmicguestinterface' 
    'vmicheartbeat' 
    'vmickvpexchange' 
    'vmicrdv' 
    'vmicshutdown' 
    'vmictimesync' 
    'vmicvmsession' 
    'vmicvss' 
    # 'vmvss' not found
    'wbengine' 
    'wcncsvc' 
    'webthreatdefsvc' 
    'wercplsupport' 
    'wisvc' 
    'wlidsvc' 
    'wlpasvc' 
    'wmiApSrv' 
    'workfolderssvc' 
    'wuauserv' 
    # 'wudfsvc' not found
)

setServices -startuptype Manual -services $servicesmanual

PrintC -c "Gray" -t "  set services disabled at startup"
$servicesdisabled = @(
    'AppVClient' 
    # 'AJRouter' not found
    'AssignedAccessManagerSvc'
    # 'CredentialEnrollmentManagerUserSvc_*' TODO not found
    'DiagTrack' 
    'DialogBlockingService' 
    'KtmRm' 
    'NetTcpPortSharing' 
    'RemoteAccess' 
    'RemoteRegistry' 
    'UevAgentService' 
    'shpamsvc' 
    'ssh-agent' 
    'tzautoupdate' 
    # 'uhssvc' not found
)

setServices -startuptype Disable -services $servicesdisabled

PrintC -c "Gray" -t "  set services to automatic startup"
$servicesautomatic = @(
    'AudioEndpointBuilder' 
    'AudioSrv' 
    'Audiosrv' 
    # 'BFE' access denied
    # 'BrokerInfrastructure' access denied
    'BthAvctpSvc'
    # 'BthHFSrv' not found
    # 'CDPUserSvc_*' TODO not found
    # 'CoreMessagingRegistrar' access denied
    'CryptSvc' 
    'DPS' 
    # 'DcomLaunch' access dnied
    'Dhcp' 
    'DispBrokerDesktopSvc' 
    # 'Dnscache' access denied
    'DusmSvc' 
    'EventLog' 
    'EventSystem' 
    'FontCache' 
    'KeyIso' 
    # 'LSM' access dnied
    'LanmanServer' 
    'LanmanWorkstation' 
    # 'MpsSvc' access dnied
    'Netlogon' 
    # 'OneSyncSvc_*' TODO not found
    'Power' 
    'ProfSvc' 
    # 'RpcEptMapper' access denied
    'RpcLocator' 
    # 'RpcSs' access denied
    'SENS' 
    'SamSs' 
    # 'Schedule' access denied
    'ShellHWDetection' 
    # 'SgrmBroker' access denied
    'Spooler' 
    'SysMain' 
    # 'SystemEventsBroker' access denied
    'TermService' 
    'Themes' 
    'TrkWks' 
    'UserManager' 
    # 'VGAuthService' not found
    # 'VMTools' not found
    'VaultSvc' 
    'Wcmsvc' 
    # 'WinDefend' access denied
    'Winmgmt' 
    'WlanSvc' 
    # 'WpnUserService_*' TODO not found
    # 'gpsvc' access denied
    'iphlpsvc' 
    # 'mpssvc' access denied
    'nsi' 
    # 'tiledatamodelsvc' not found
    # 'webthreatdefusersvc_*' TODO not found
)

setServices -startuptype Automatic -services $servicesautomatic
