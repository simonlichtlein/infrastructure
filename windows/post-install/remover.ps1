if (!$global:ltsc) {
    PrintC -c "DarkGray" -t "    Microsoft.Bing"
    Get-AppxPackage Microsoft.BingWeather | Remove-AppxPackage
    Get-AppxPackage Microsoft.BingNews | Remove-AppxPackage
    Get-AppxPackage Microsoft.BingSearch | Remove-AppxPackage
    PrintC -c "DarkGray" -t "    Microsoft.GetHelp"
    Get-AppxPackage Microsoft.GetHelp | Remove-AppxPackage
    PrintC -c "DarkGray" -t "    Microsoft.Getstarted"
    Get-AppxPackage Microsoft.Getstarted | Remove-AppxPackage
    PrintC -c "DarkGray" -t "    Microsoft.MicrosoftOfficeHub"
    Get-AppxPackage Microsoft.MicrosoftOfficeHub | Remove-AppxPackage
    PrintC -c "DarkGray" -t "    Microsoft.MicrosoftSolitaireCollection"
    Get-AppxPackage Microsoft.MicrosoftSolitaireCollection | Remove-AppxPackage
    PrintC -c "DarkGray" -t "    Microsoft.MixedReality.Portal"
    Get-AppxPackage Microsoft.MixedReality.Portal | Remove-AppxPackage
    PrintC -c "DarkGray" -t "    Microsoft.MSPaint"
    Get-AppxPackage Microsoft.MSPaint | Remove-AppxPackage
    PrintC -c "DarkGray" -t "    Microsoft.OneNote"
    Get-AppxPackage Microsoft.Office.OneNote | Remove-AppxPackage
    PrintC -c "DarkGray" -t "    Microsoft.People"
    Get-AppxPackage Microsoft.People | Remove-AppxPackage
    PrintC -c "DarkGray" -t "    Microsoft.SkypeApp"
    Get-AppxPackage Microsoft.SkypeApp | Remove-AppxPackage
    PrintC -c "DarkGray" -t "    Microsoft.Wallet"
    Get-AppxPackage Microsoft.Wallet | Remove-AppxPackage
    PrintC -c "DarkGray" -t "    Microsoft.windowscommunicationsapps"
    Get-AppxPackage microsoft.windowscommunicationsapps | Remove-AppxPackage
    PrintC -c "DarkGray" -t "    Microsoft.WindowsFeedbackHub"
    Get-AppxPackage Microsoft.WindowsFeedbackHub | Remove-AppxPackage
    PrintC -c "DarkGray" -t "    Microsoft.WindowsMaps"
    Get-AppxPackage Microsoft.WindowsMaps | Remove-AppxPackage
    PrintC -c "DarkGray" -t "    Microsoft.WindowsSoundRecorder"
    Get-AppxPackage Microsoft.WindowsSoundRecorder | Remove-AppxPackage
    PrintC -c "DarkGray" -t "    Microsoft.YourPhone"
    Get-AppxPackage Microsoft.YourPhone | Remove-AppxPackage
    PrintC -c "DarkGray" -t "    Microsoft.ZuneMusic"
    Get-AppxPackage Microsoft.ZuneMusic | Remove-AppxPackage
    PrintC -c "DarkGray" -t "    Microsoft.ZuneVideo"
    Get-AppxPackage Microsoft.ZuneVideo | Remove-AppxPackage
    PrintC -c "DarkGray" -t "    Microsoft.MicrosoftStickyNotes"
    Get-AppxPackage Microsoft.MicrosoftStickyNotes | Remove-AppxPackage
    PrintC -c "DarkGray" -t "    Microsoft.WindowsAlarms"
    Get-AppxPackage Microsoft.WindowsAlarms | Remove-AppxPackage
    PrintC -c "DarkGray" -t "    MicrosoftTeams"
    Get-AppxPackage -Name MicrosoftTeams | Remove-AppxPackage
    Get-AppxPackage MSTeams | Remove-AppxPackage
    PrintC -c "DarkGray" -t "    Clipchamp.Clipchamp"
    Get-AppxPackage Clipchamp.Clipchamp | Remove-AppxPackage
    PrintC -c "DarkGray" -t "    Microsoft.Windows.DevHome"
    Get-AppxPackage Microsoft.Windows.DevHome | Remove-AppxPackage
    PrintC -c "DarkGray" -t "    Microsoft.OutlookForWindows"
    Get-AppxPackage Microsoft.OutlookForWindows | Remove-AppxPackage
    PrintC -c "DarkGray" -t "    Microsoft.Todos"
    Get-AppxPackage Microsoft.Todos | Remove-AppxPackage



    PrintC -c "DarkGray" -t "    uninstalling Microsoft Teams Meeting Add-in for Microsoft Office"
    winget uninstall --name "Microsoft Teams Meeting Add-in for Microsoft Office" --force

    if (!$global:PIconfig.xboxfeatures) {
        PrintC -c "DarkGray" -t "    removing Xbox features"
        PrintC -c "DarkGray" -t "      Microsoft.GamingApp"
        Get-AppxPackage Microsoft.GamingApp | Remove-AppxPackage
        PrintC -c "DarkGray" -t "      Microsoft.Xbox.TCUI"
        Get-AppxPackage Microsoft.Xbox.TCUI | Remove-AppxPackage
        PrintC -c "DarkGray" -t "      Microsoft.XboxApp"
        Get-AppxPackage Microsoft.XboxApp | Remove-AppxPackage
        PrintC -c "DarkGray" -t "      Microsoft.XboxGameOverlay"
        Get-AppxPackage Microsoft.XboxGameOverlay | Remove-AppxPackage
        PrintC -c "DarkGray" -t "      Microsoft.XboxGamingOverlay"
        Get-AppxPackage Microsoft.XboxGamingOverlay | Remove-AppxPackage
        PrintC -c "DarkGray" -t "      Microsoft.XboxIdentityProvider"
        Get-AppxPackage Microsoft.XboxIdentityProvider | Remove-AppxPackage
        PrintC -c "DarkGray" -t "      Microsoft.XboxSpeechToTextOverlay"
        Get-AppxPackage Microsoft.XboxSpeechToTextOverlay | Remove-AppxPackage
    }
}

PrintC -c "DarkGray" -t "    uninstalling Microsoft OneDrive"
if (Test-Path "C:\Windows\System32\OneDriveSetup.exe") {
    Start-Process -FilePath "C:\Windows\System32\OneDriveSetup.exe" -ArgumentList "/uninstall" -Wait
}
else {
    PrintC -c "DarkGray" -t "      OneDriveSetup.exe not found - probably already uninstalled"
}

PrintC -c "DarkGray" -t "    uninstalling Microsoft Edge"
Start-Process -FilePath "$PSScriptRoot\Remove-EdgeWeb.exe" -ArgumentList "/s" -Wait

PrintC -c "DarkGray" -t "    removing Microsoft Store from taskbar"

((New-Object -Com Shell.Application).NameSpace('shell:::{4234d49b-0245-4df3-b780-3893943456e1}').Items() | Where-Object { $_.Name -eq "Microsoft Store" }).Verbs() | Where-Object { $_.Name.replace('&', '') -match 'Unpin from taskbar' } | ForEach-Object { $_.DoIt(); $exec = $true ; Write-Debug $exec }

# LTSC 
# at the moment everything seems fine with LTSC :)