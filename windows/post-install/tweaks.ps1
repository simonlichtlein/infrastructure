
#! default tweaks

# desktop add "This PC" to Desktop | instant
PrintC -c "DarkGray" -t "    add `"This PC`" to Desktop"
Merge-RegKey -path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\NewStartPanel" -name "{20D04FE0-3AEA-1069-A2D8-08002B30309D}" -value 0 -type DWORD

# desktop show windows version | instant
PrintC -c "DarkGray" -t "    show windows version on desktop"
Merge-RegKey -path "HKCU:\Control Panel\Desktop" -name "PaintDesktopVersion" -value 1 -type DWORD

# disable the anoying "your charger is weak" notification | instant
PrintC -c "DarkGray" -t "    hide slow charger notification warning"
Merge-RegKey -path "HKCU:\Software\Microsoft\Shell\USB" -name "NotifyOnWeakCharger" -value 0 -type DWORD 

# explorer compact mode | instant
PrintC -c "DarkGray" -t "    enable explorer compact mode"
Merge-RegKey -path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -name "UseCompactMode" -value 1 -type DWORD

# explorer show extensions | instant
PrintC -c "DarkGray" -t "    show extension in explorer"
Merge-RegKey -path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -name "HideFileExt" -value 0 -type DWORD

# explorer old context menu | explorer restart
PrintC -c "DarkGray" -t "    enable full 'old' context menu"
Merge-RegKey -path "HKCU:\Software\Classes\CLSID\{86ca1aa0-34aa-4e8b-a509-50c905bae2a2}\InprocServer32" -name "(Default)" -value "" -type String

# explorer show hidden files | instant
PrintC -c "DarkGray" -t "    show hidden files"
Merge-RegKey -path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -name "Hidden" -value 1 -type DWORD 

# explorer open to thisPC | instant
PrintC -c "DarkGray" -t "    explorer opens to `"This PC`" by default"
Merge-RegKey -path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -name "LaunchTo" -value 1 -type DWORD

# explorer enable clipboard history | instant
PrintC -c "DarkGray" -t "    enable clipboard history"
Merge-RegKey -path "HKCU:\SOFTWARE\Microsoft\Clipboard" -name "EnableClipboardHistory" -value 1 -type DWORD 

# explorer delete confirmation | explorer restart
PrintC -c "DarkGray" -t "    enable delete confirmation"
Merge-RegKey -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer" -name "ConfirmFileDelete" -value 1 -type DWORD 

# taskbar hide chat button | instant
PrintC -c "DarkGray" -t "    hide chat button on taskbar"
Merge-RegKey -path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -name "TaskbarMn" -value 0 -type DWORD

# unpin store from taskbar | reboot ?
PrintC -c "DarkGray" -t "    unpin Store from taskbar"
Merge-RegKey -path "HKCU\Software\Policies\Microsoft\Windows\Explorer" -name "NoPinningStoreToTaskbar" -v 1 -type DWORD

# disable windows widgets | reboot ?
PrintC -c "DarkGray" -t "    disable widgets completly"
Merge-RegKey -path "HKLM:\SOFTWARE\Microsoft\PolicyManager\default\NewsAndInterests\AllowNewsAndInterests" -name "value" -value 0 -type DWORD
Merge-RegKey -path "HKLM:\SOFTWARE\Policies\Microsoft\Dsh" -name "AllowNewsAndInterests" -value 0 -type DWORD

# disable audio ducking | instant 
PrintC -c "DarkGray" -t "    diable audio ducking"
Merge-RegKey -path "HKCU:\SOFTWARE\Microsoft\Multimedia\Audio" -name "UserDuckingPreference" -value 3 -type DWORD 

# disable sticky keys | instant
PrintC -c "DarkGray" -t "    disable sticky keys"
Merge-RegKey -path "HKCU:\Control Panel\Accessibility\StickyKeys" -name "Flags" -value "506" -type String 

# enable NumLock on Startup | reboot
PrintC -c "DarkGray" -t "    enable NumLock on Startup"
Merge-RegKey -path "HKCU:\Control Panel\Keyboard" -name "InitialKeyboardIndicators" -value 2 -type DWORD

# enable long paths | reboot
PrintC -c "DarkGray" -t "    enable long paths"
Merge-RegKey -path "HKLM:\SYSTEM\CurrentControlSet\Control\FileSystem" -name "LongPathsEnabled" -value 1 -type DWORD

# enable RDP | instant
PrintC -c "DarkGray" -t "    enable RDP"
Merge-RegKey -path "HKLM:\SYSTEM\CurrentControlSet\Control\Terminal Server" -name "fDenyTSConnections" -value 0 -type DWORD
Merge-RegKey -path "HKLM:\SYSTEM\CurrentControlSet\Control\Lsa" -name "LimitBlankPasswordUse" -value 0 -type DWORD

if (Get-NetFirewallRule -DisplayGroup "Remote Desktop" -ErrorAction SilentlyContinue) {
    Enable-NetFirewallRule -DisplayGroup "Remote Desktop"
}
elseif (Get-NetFirewallRule -DisplayGroup "RemoteDesktop" -ErrorAction SilentlyContinue) {
    Enable-NetFirewallRule -DisplayGroup "RemoteDesktop"
}
else {
    PrintC -c "Red" -t "    could not enable RDP firewall rule"
}

# classic control panel | instant
PrintC -c "DarkGray" -t "    classic control panel view"
Merge-RegKey -path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\ControlPanel" -name "AllItemsIconView" -value 1 -type DWORD
Merge-RegKey -path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\ControlPanel" -name "StartupPage" -value 1 -type DWORD

# deactivate Fast Startup | reboot
PrintC -c "DarkGray" -t "    disabled fastboot"
Merge-RegKey -path "HKLM:\SYSTEM\CurrentControlSet\Control\Session Manager\Power" -name "HiberbootEnabled" -value 0 -type DWORD

# disable auto install apps - | instant to test
PrintC -c "DarkGray" -t "    disable auto installation of apps"
Merge-RegKey -path "HKCU:\Software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager" -name "SilentInstalledAppsEnabled" -value 0 -type DWORD

# set network type private | instant 
PrintC -c "DarkGray" -t "    set networktype private on ipv4/ipv6 connected network"
Get-NetConnectionProfile | Where { $_.IPv4Connectivity -eq "Internet" } | Set-NetConnectionProfile -NetworkCategory Private
Get-NetConnectionProfile | Where { $_.IPv6Connectivity -eq "Internet" } | Set-NetConnectionProfile -NetworkCategory Private

# disable Bitlocker Auto Encryption on Windows 11 24H2 and Onwards | reboot
PrintC -c "DarkGray" -t "    disable Bitlocker Auto Encryption"
Merge-RegKey -path "HKLM:\SYSTEM\CurrentControlSet\Control\BitLocker" -name "PreventDeviceEncryption" -value 1 -type DWORD
Merge-RegKey -path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\EnhancedStorageDevices" -name "TCGSecurityActivationDisabled" -value 1 -type DWORD

#! other stuff
PrintC -c "DarkGray" -t "    remove ms-gameoverlay notification promt"
# disable "We can't open this 'ms-gamingoverlay' link" message
Merge-RegKey -path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\GameDVR" -name "AppCaptureEnabled" -value 0 -type DWORD

PrintC -c "DarkGray" -t "    GamingDVR configuratino"
# enable gamedvr (good for perfomance in games) 0 = on, 1 = off #FUCKMS
Merge-RegKey -path "HKCU:\System\GameConfigStore" -name "GameDVR_Enabled" -value 0 -type DWORD 
# 2: Limits Game DVR resource usage in full-screen games, potentially improving performance.
Merge-RegKey -path "HKCU:\System\GameConfigStore" -name "GameDVR_FSEBehavior" -value 2 -type DWORD
# 2: Limits Game DVR resource usage in DSE, potentially improving gaming performance.
Merge-RegKey -path "HKCU:\System\GameConfigStore" -name "GameDVR_DSEBehavior" -value 2 -type DWORD
# 1: Game DVR respects the FSE flag and won't record while games are in true full-screen mode.
Merge-RegKey -path "HKCU:\System\GameConfigStore" -name "GameDVR_DXGIHonorFSEWindowsCompatible" -value 1 -type DWORD
# 1: Forces application of GameDVR_FSEBehavior to all full-screen games.
Merge-RegKey -path "HKCU:\System\GameConfigStore" -name "GameDVR_HonorUserFSEBehaviorMode" -value 1 -type DWORD
# This key controls additional functionality related to Enhanced Full-screen Exclusive (EFSE), a newer version of FSE. Different bit values within the key enable or disable specific features. 0 = on, 1 = off #FUCKMS
Merge-RegKey -path "HKCU:\System\GameConfigStore" -name "GameDVR_EFSEFeatureFlags" -value 0 -type DWORD

#! custom tweaks from config
PrintC -c "Gray" -t "  tweaking more with user defined settings from config.json"

# show Desktop Folder in This PC | explorer restart
if ($global:PIconfig.tweaks.explorer.showDesktopFolder) {
    PrintC -c "DarkGray" -t "    showing Desktop folder in ThisPC View"
    Merge-RegKey -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\MyComputer\NameSpace\{B4BFCC3A-DB2C-424C-B029-7FE99A87C641}" -name "HideIfEnabled" -value 0 -type DWORD
    Merge-RegKey -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\MyComputer\NameSpace\{B4BFCC3A-DB2C-424C-B029-7FE99A87C641}" -name "HiddenByDefault" -value 0 -type DWORD
}

# show Documents Folder in This PC | explorer restart
if ($global:PIconfig.tweaks.explorer.showDocumentsFolder) {
    PrintC -c "DarkGray" -t "    showing Documents folder in ThisPC View"
    Merge-RegKey -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\MyComputer\NameSpace\{d3162b92-9365-467a-956b-92703aca08af}" -name "HideIfEnabled" -value 0 -type DWORD
    Merge-RegKey -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\MyComputer\NameSpace\{d3162b92-9365-467a-956b-92703aca08af}" -name "HiddenByDefault" -value 0 -type DWORD
}

if ($global:PIconfig.tweaks.explorer.showDownloadsFolder) {
    PrintC -c "DarkGray" -t "    showing Downloads folder in ThisPC View"
    Merge-RegKey -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\MyComputer\NameSpace\{088e3905-0323-4b02-9826-5d99428e115f}" -name "HideIfEnabled" -value 0 -type DWORD
    Merge-RegKey -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\MyComputer\NameSpace\{088e3905-0323-4b02-9826-5d99428e115f}" -name "HiddenByDefault" -value 0 -type DWORD
}

if ($global:PIconfig.tweaks.explorer.showMusicFolder) {
    PrintC -c "DarkGray" -t "    showing Music folder in ThisPC View"
    Merge-RegKey -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\MyComputer\NameSpace\{3dfdf296-dbec-4fb4-81d1-6a3438bcf4de}" -name "HideIfEnabled" -value 0 -type DWORD
    Merge-RegKey -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\MyComputer\NameSpace\{3dfdf296-dbec-4fb4-81d1-6a3438bcf4de}" -name "HiddenByDefault" -value 0 -type DWORD
    # yes this third one is required
    Merge-RegKey -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\MyComputer\NameSpace\{1CF1260C-4DD0-4ebb-811F-33C572699FDE}" -name "HiddenByDefault" -value 0 -type DWORD
}

if ($global:PIconfig.tweaks.explorer.showPicturesFolder) {
    PrintC -c "DarkGray" -t "    showing Picutures folder in ThisPC View"
    Merge-RegKey -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\MyComputer\NameSpace\{24ad3ad4-a569-4530-98e1-ab02f9417aa8}" -name "HideIfEnabled" -value 0 -type DWORD
    Merge-RegKey -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\MyComputer\NameSpace\{24ad3ad4-a569-4530-98e1-ab02f9417aa8}" -name "HiddenByDefault" -value 0 -type DWORD
}

if ($global:PIconfig.tweaks.explorer.showVideosFolder) {
    PrintC -c "DarkGray" -t "    showing Videos folder in ThisPC View"
    Merge-RegKey -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\MyComputer\NameSpace\{f86fa3ab-70d2-4fc7-9c99-fcbf05467f3a}" -name "HideIfEnabled" -value 0 -type DWORD
    Merge-RegKey -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\MyComputer\NameSpace\{f86fa3ab-70d2-4fc7-9c99-fcbf05467f3a}" -name "HiddenByDefault" -value 0 -type DWORD
}

# hide Search button | instant
if ($global:PIconfig.tweaks.taskbar.hideSearchButton) {
    PrintC -c "DarkGray" -t "    taskbar - hiding Search button"
    Merge-RegKey -path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Search" -name "SearchboxTaskbarMode" -value 0 -type DWORD
}

# hide taskview button | instant
if ($global:PIconfig.tweaks.taskbar.hideTaskViewButton) {
    PrintC -c "DarkGray" -t "    taskbar - hiding TaskView button"
    Merge-RegKey -path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -name "ShowTaskViewButton" -value 0 -type DWORD
}

# hide copilot button | instant
if ($global:PIconfig.tweaks.taskbar.hideCopilotButton) {
    PrintC -c "DarkGray" -t "    taskbar - hiding Copilot button"
    Merge-RegKey -path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -name "ShowCopilotButton" -value 0 -type DWORD
}


# TODO weird stuff happening
# hide widgets button
if ($global:PIconfig.tweaks.taskbar.hideWidgetsButton) {
    # the regkey is blocked by UPCD Service. #fuckMS
    PrintC -c "DarkGray" -t "    taskbar - hiding Widget button"
    PrintC -c "Yellow" -t   "      this will take action after an reboot"
    Merge-RegKey -path "HKLM:\SYSTEM\CurrentControlSet\Services\UCPD" -name "Start" -value 4 -type DWORD
    # TODO on recent 24H2 W11 ENT the Widget button is still there but on the right side leftside of the tray icons. 
    # ? I think it is related to the taskbar aligment.
    AddToPostreboot 'Merge-RegKey -path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -name "TaskbarDa" -value 0 -type DWORD'
}

# align taskbar to left | instant
if ($global:PIconfig.tweaks.taskbar.alignLeft) {
    PrintC -c "DarkGray" -t "    taskbar - aligning to left"
    Merge-RegKey -path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -name "TaskbarAl" -value 0 -type DWORD
}