################################################################
#                 Windows Post-Install Script                  #
# 💡 lichtlein.dev | vires in numeris | simon@lichtlein.dev 💡 #
################################################################

# self elevating script to admin (if not already)
if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) { Start-Process powershell.exe "-NoProfile -ExecutionPolicy Bypass -File `"$PSCommandPath`"" -Verb RunAs; exit }

# terminal config
$host.UI.RawUI.BackgroundColor = "Black"
$host.UI.RawUI.ForegroundColor = "White"
Clear-Host

Start-Transcript -Path $PSScriptRoot\post-install.log

# disable progressbar status because of bad handling of multiple actions #fuckMS
$ProgressPreference = "SilentlyContinue"

Set-Location $PSScriptRoot
Import-Module -Force "$PSScriptRoot\module.psm1"

PrintInfo
PrintC -c "Blue" -m $true -t "Windows Post-Install Script" 

# create post-reboot script
PrintC -c "Gray" -t "  creating the post-reboot script"
@"
Set-Location $PSScriptRoot
Start-Transcript -Path $PSScriptRoot\post-reboot.log
Import-Module -Force "$PSScriptRoot\module.psm1"
"@ | Out-File -FilePath "$PSScriptRoot\postreboot.ps1"

$configPath = "$PSScriptRoot\config.json"
if (-not (Test-Path $configPath)) {
    PrintC -c "Red" -t "Config file not found. Exiting..."
    exit
}

$global:PIconfig = Get-Content -Path $configPath | ConvertFrom-Json

$windowsEdition = (Get-WmiObject -Class Win32_OperatingSystem).Caption
if ($windowsEdition -like "*LTSC*") {
    $global:ltsc = $true
}
else {
    $global:ltsc = $false
}

PrintC -c "Gray" -t "  adding Windows Defender exclusions"
PrintC -c "DarkGray" -t "    $PSScriptRoot"
Add-MpPreference -ExclusionPath $PSScriptRoot
PrintC -c "DarkGray" -t "    $ENV:USERPROFILE\Downloads\"
Add-MpPreference -ExclusionPath "$ENV:USERPROFILE\Downloads\"

#* doing this before internet connection to avoid any weird update stuff
PrintC -c "Gray" -t "  configure WSUS Server"
Invoke-Expression "& '$PSScriptRoot\wsus.ps1'"

PrintC -c "Gray" -t "  tweaking windows"
Invoke-Expression "& '$PSScriptRoot\tweaks.ps1'"

PrintC -c "Gray" -t "  setting up post-reboot script"

AddToPostreboot "Stop-Transcript"
Merge-RegKey "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\RunOnce" "post_reboot" "powershell.exe -NoProfile -ExecutionPolicy Bypass -File `"$PSScriptRoot\postreboot.ps1`" -Verb RunAs" "String"

# check if internet is available
PrintC
while (!(Test-Connection -ComputerName "www.gitlab.com" -Count 1 -Quiet)) {
    PrintC -c "Red" -t "No internet connection. Please connect your device to the internet.."
    $retryinput = Read-Host "Press Enter to retry or 'q' to quit"
    if ($retryinput -eq "q") {
        exit
    }
}
PrintC
PrintC -c "Gray" -t "  starting activation script"
& ([ScriptBlock]::Create((Invoke-RestMethod https://get.activated.win))) /HWID
PrintC

PrintC
PrintC -c "Gray" -t "  removing bloatware"
Invoke-Expression "& '$PSScriptRoot\remover.ps1'"

PrintC
PrintC -c "Gray" -t "  adding perfomance tweaks"
Invoke-Expression "& '$PSScriptRoot\perfomance.ps1'"

PrintC
PrintC -c "Gray" -t "  installing programs"
Invoke-Expression "& '$PSScriptRoot\installer.ps1'"

PrintC
PrintC -c "Gray" -t "  finishing tasks"

PrintC -c "DarkGray" -t "    removing temporary Windows Defender exclusions"
Remove-MpPreference -ExclusionPath $PSScriptRoot

PrintC
PrintC -c "Green" -t "post-install finished"
PrintInfo

# do not restart Explorer only because the AddToPostreboot stuff will be called. force reboot instead
Write-Host "`n=== System Restart Initiated ===`n" -ForegroundColor Yellow
Stop-Transcript
$seconds = 5
for ($i = $seconds; $i -gt 0; $i--) {
    $progressBar = "[" + ("#" * ($seconds - $i + 1)).PadRight($seconds, "-") + "]"  # Progress bar

    Write-Host ("`rRestarting in $i seconds $progressBar") -ForegroundColor Cyan -NoNewline
    Start-Sleep -Seconds 1
}
Write-Host ""
Write-Host "`n🚀 Restarting now! 🚀" -ForegroundColor Red

Write-Host "Restarting now!" -ForegroundColor Red
Restart-Computer -Forc