$wuserver = "http://wsus.siro-solutions.de:8530"
Merge-RegKey -path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate" -Name "WUServer" -value $wuserver -type String
Merge-RegKey -path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate" -Name "WUStatusServer" -value $wuserver -type String
Merge-RegKey -path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate" -Name "ElevateNonAdmins" -value 1 -type DWORD

Merge-RegKey -path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" -Name "UseWUServer" -value 1 -type DWORD
Merge-RegKey -path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" -Name "AUOptions" -value 3 -type DWORD
Merge-RegKey -path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" -Name "AutoInstallMinorUpdates" -value 1 -type DWORD
Merge-RegKey -path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" -Name "DetectionFrequencyEnabled" -value 1 -type DWORD
Merge-RegKey -path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" -Name "DetectionFrequency" -value 6 -type DWORD
Merge-RegKey -path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" -Name "NoAutoRebootWithLoggedOnUsers" -value 1 -type DWORD
Merge-RegKey -path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" -Name "NoAutoUpdate" -value 0 -type DWORD

# Enables access to Windows Update (Check online for updates from Microsoft Update)
Remove-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate" -Name "DisableWindowsUpdateAccess" -ErrorAction SilentlyContinue

# restart service and start update detection
Stop-Service -Force wuauserv
Remove-Item -Force -Recurse "$ENV:WINDIR\softwaredistribution" -ErrorAction SilentlyContinue
Start-Service wuauserv
usoclient /resetauthorization /detectnow