function PrintC {
    param (
        [Alias('c')][string]$Color = "White",
        [Alias('m')][bool]$Middle = $false,
        [Alias('n')][bool]$NoNewline = $false,
        [Alias('t')][string]$Text = ""
    )

    if ($Middle) {
        $terminalWidth = $Host.UI.RawUI.WindowSize.Width
        $padding = ($terminalWidth - $Text.Length) / 2 - 1
        $Text = (" " * $padding) + $Text + (" " * $padding)
    }

    if ($NoNewline) {
        Write-Host $Text -ForegroundColor $Color -NoNewline
    }
    else {
        Write-Host $Text -ForegroundColor $Color
    }
}

function PrintL() {
    Write-Host ("-" * $Host.UI.RawUI.BufferSize.Width)
}
function PrintInfo {
    PrintC
    PrintL
    PrintC -c "Cyan" -m $true -t "lichtlein.dev | vires in numeris | simon@lichtlein.dev"
    PrintL
}

function CreateRegKeysRecursive($path) {
    Write-Verbose "checking $path"

    # split path at each \
    $split = $path -split '\\'

    $pathToCreate = ""
    for ($i = 0; $i -lt $split.Count; $i++) {

        $pathToCreate += $split[$i] + "\"

        Write-Verbose "checking split path $pathToCreate"

        if (!(Test-Path $pathToCreate)) {
            Write-Verbose "path does not exist - creating"
            New-Item -path $pathToCreate -ItemType Directory | Out-Null
        }
        else {
            Write-Verbose "path does exist - skipping"
        }
    }
}

function Merge-RegKey($path, $name, $value, $type) {
    CreateRegKeysRecursive $path
    New-ItemProperty -path $path -name $name -value $value -PropertyType $type -Force | Out-Null
}

function AddToPostreboot($command) {
    # $postrebootPath = "$PSScriptRoot\postreboot.ps1"
    # $content = Get-Content -Path $postrebootPath
    # $content += "`r`n" + $command
    # Set-Content -Path $postrebootPath -Value $content
    $content = $command + "`r`n"
    Add-Content -Path "$PSScriptRoot\postreboot.ps1" -Value $content
}