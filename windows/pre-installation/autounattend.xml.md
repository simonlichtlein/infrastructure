Installing Windows 10 is done in three phases:

Boot from install media, run Windows Setup

Configure hardware devices

Windows Welcome (OOBE)

In normal install, user interaction is required in phases 1 and 3, phase 2 being run automatically without user interaction. In phase 1 user selects language and keyboard layout settings for system accounts, enters product key for specific edition or selects edition manually if product key is not entered at this stage, and selects disk and partition to install Windows. When phase 1 is done, Windows restarts to phase 2 which is done without user interaction and when ready automatically restarts to phase 3.

In phase 3 user selects region and language settings for user accounts, creates initial admin user account, chooses OneDrive and privacy settings and finally boots to desktop.

In this tutorial we will create two answer files to automate phases 1 and 3. An answer file is set of commands and instructions in XML format to tell Windows setup what to do and how to proceed. We will need two answer files:

autounattend.xml to automate phase 1, Windows Setup

unattend.xml to automate phase 3, OOBE

https://www.tenforums.com/tutorials/96683-create-media-automated-unattended-install-windows-10-a.html

<!-- <UILanguageFallback>en-US</UILanguageFallback> -->

UILanguageFallback is the language to be used for resources, notifications and system messages that are not localized (translated) to current Windows system language.

→ no use for de-DE and en-US/UK

https://www.tenforums.com/tutorials/96683-create-media-automated-unattended-install-windows-10-a.html

Windows 11 requirement bypass
```xml
<component name="Microsoft-Windows-Setup" processorArchitecture="amd64" publicKeyToken="31bf3856ad364e35" language="neutral" versionScope="nonSxS">
      <RunSynchronous>
        <RunSynchronousCommand wcm:action="add">
          <Order>1</Order>
          <Description>BypassTPMCheck</Description>
          <Path>cmd /c reg add "HKLM\SYSTEM\Setup\LabConfig" /v "BypassTPMCheck" /t REG_DWORD /d 1</Path>
        </RunSynchronousCommand>
        <RunSynchronousCommand wcm:action="add">
          <Order>2</Order>
          <Description>BypassSecureBootCheck</Description>
          <Path>cmd /c reg add "HKLM\SYSTEM\Setup\LabConfig" /v "BypassSecureBootCheck" /t REG_DWORD /d 1</Path>
        </RunSynchronousCommand>
        <RunSynchronousCommand wcm:action="add">
          <Order>3</Order>
          <Description>BypassCPUCheck</Description>
          <Path>cmd /c reg add "HKLM\SYSTEM\Setup\LabConfig" /v "BypassCPUCheck" /t REG_DWORD /d 1</Path>
        </RunSynchronousCommand>
        <RunSynchronousCommand wcm:action="add">
          <Order>4</Order>
          <Description>BypassRAMCheck</Description>
          <Path>cmd /c reg add "HKLM\SYSTEM\Setup\LabConfig" /v "BypassRAMCheck" /t REG_DWORD /d 1</Path>
        </RunSynchronousCommand>
        <RunSynchronousCommand wcm:action="add">
          <Order>5</Order>
          <Description>BypassStorageCheck</Description>
          <Path>cmd /c reg add "HKLM\SYSTEM\Setup\LabConfig" /v "BypassStorageCheck" /t REG_DWORD /d 1</Path>
        </RunSynchronousCommand>
      </RunSynchronous>
      <DiskConfiguration>
```
Disk configuration & image install

if this is present you cant choose disk or format partition manually - be carefull with `<InstallTo><DiskID>`
```xml
      <DiskConfiguration>
        <Disk wcm:action="add">
          <DiskID>0</DiskID>
          <WillWipeDisk>true</WillWipeDisk>
          <CreatePartitions>
            <!-- System partition (ESP) -->
            <CreatePartition wcm:action="add">
              <Order>1</Order>
              <Type>EFI</Type>
              <Size>500</Size>
            </CreatePartition>
            <!-- Windows partition -->
            <CreatePartition wcm:action="add">
              <Order>2</Order>
              <Type>Primary</Type>
              <Extend>true</Extend>
            </CreatePartition>
          </CreatePartitions>
          <ModifyPartitions>
            <!-- System partition (ESP) -->
            <ModifyPartition wcm:action="add">
              <Order>1</Order>
              <PartitionID>1</PartitionID>
              <Label>System</Label>
              <Format>FAT32</Format>
            </ModifyPartition>
            <!-- Windows partition -->
            <ModifyPartition wcm:action="add">
              <Order>2</Order>
              <PartitionID>2</PartitionID>
              <Label></Label>
              <Letter>C</Letter>
              <Format>NTFS</Format>
            </ModifyPartition>
          </ModifyPartitions>
        </Disk>
      </DiskConfiguration>
       <ImageInstall>
        <OSImage>
          <InstallTo>
            <DiskID>0</DiskID>
            <PartitionID>2</PartitionID>
          </InstallTo>
          <!-- default value is false, so no entry needed - https://learn.microsoft.com/en-us/windows-hardware/customize/desktop/unattend/microsoft-windows-setup-imageinstall-osimage-installtoavailablepartition -->
          <InstallToAvailablePartition>false</InstallToAvailablePartition>
        </OSImage>
      </ImageInstall>
```

user creation, password and autologon

AutoLogon
```xml
   <component name="Microsoft-Windows-Shell-Setup" processorArchitecture="amd64" publicKeyToken="31bf3856ad364e35" language="neutral" versionScope="nonSxS">
      <AutoLogon>
        <Password>
          <Value></Value>
          <PlainText>true</PlainText>
        </Password>
        <Enabled>true</Enabled>
        <Username>tester</Username>
      </AutoLogon>
```
Setting

Description

Domain

Specifies the domain to which the computer is logging on.

Enabled

Specifies whether the automatic logon process is enabled.

LogonCount

Specifies the number of times that the account has been used. LogonCount must be specified if AutoLogon is used.

Password

Specifies the password for the account that is used for automatic logon.

Username

Specifies the user account name that is used for automatic logon.

https://learn.microsoft.com/en-us/windows-hardware/customize/desktop/unattend/microsoft-windows-shell-setup-autologon 

UserAccounts
```xml
<UserAccounts>
   <AdministratorPassword>
      <Value>cAB3AEEAZABtAGkAbgBpAHMAdAByAGEAdABvAHIAUABhAHMAcwB3AG8AcgBkAA==</Value>
      <PlainText>false</PlainText>
   </AdministratorPassword>
   <DomainAccounts>
      <DomainAccountList wcm:action="add">
         <DomainAccount wcm:action="add">
            <Name>account1</Name>
            <Group>Fabrikam\Group1</Group>
         </DomainAccount>
         <DomainAccount wcm:action="add">
            <Name>account2</Name>
            <Group>Fabrikam\Group2</Group>
         </DomainAccount wcm:action="add">
         <Domain>domain1</Domain>
      </DomainAccountList>
      <DomainAccountList wcm:action="add">
         <DomainAccount wcm:action="add">
            <Name>account3</Name>
            <Group>Fabrikam\Group2</Group>
         </DomainAccount wcm:action="add">
         <Domain>domain2</Domain>
     </DomainAccountList>
   </DomainAccounts>
   <LocalAccounts>
      <LocalAccount wcm:action="add">
         <Password>
            <Value>cAB3AFAAYQBzAHMAdwBvAHIAZAA</Value>
            <PlainText>false</PlainText>
         </Password>
         <Description>Test account</Description>
         <DisplayName>Admin/Power User Account</DisplayName>
         <Group>Administrators;Power Users</Group>
         <Name>Test1</Name>
      </LocalAccount>
      <LocalAccount wcm:action="add">
         <Password>
            <Value>cABhAHMAcwB3AG8AcgBkAFAAYQBzAHMAdwBvAHIAZAA=</Value>
            <PlainText>false</PlainText>
         </Password>
         <Description>For testing</Description>
         <DisplayName>Admin Account</DisplayName>
         <Group>Administrators</Group>
         <Name>Test2</Name>
      </LocalAccount>
   </LocalAccounts>
</UserAccounts>
```
Setting

Description

AdministratorPassword

Specifies the administrator password for the computer and whether it is hidden in the unattended installation answer file.

DomainAccounts

Specifies the details of domain accounts to be added to local security groups on the computer during installation.

LocalAccounts

Specifies the details of local accounts to be created during installation


https://learn.microsoft.com/en-us/windows-hardware/customize/desktop/unattend/microsoft-windows-shell-setup-useraccounts 

FirstLogon Commands

wsus

post installation

https://learn.microsoft.com/en-us/windows-hardware/customize/desktop/unattend/microsoft-windows-shell-setup-firstlogoncommands 
