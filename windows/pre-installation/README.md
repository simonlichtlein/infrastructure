# autounattend.xml


<hr>
<span style="color:red">This section is on `settings pass="windowsPE"`</span>
<hr>

# troubleshooting

The log file location will depend on the point of when the installed failed:

Before the installation starts in WinPE Pass – x:\windows\panther – WinPE only - Use Shift+F10 to open command window and run notepad.exe
After installation starts – c:\$Windows.~BT\Sources\panther - Use Shift+F10 to open command window and run notepad.exe
After installation is completed – c:\windows\panther
Log files to review:

Setupact.log – master log file for the whole installation process.
Setuperr.log – smaller version of Setupact.log, but only includes the errors. This should be the first log to look at.
Cbs_unattend.log - this log records the package setup and answer file processing

## language and keyboard | Microsoft-Windows-International-Core-WinPE

### language english wiht uk (us international) AND german keyboard

```xml
<SetupUILanguage>
    <UILanguage>en-US</UILanguage>
</SetupUILanguage>
<InputLocale>0409:00000809;0407:00000407</InputLocale>
<SystemLocale>en-US</SystemLocale>
<UILanguage>en-US</UILanguage>
<UserLocale>en-US</UserLocale>
```

### language german with german keyboard
```xml
<SetupUILanguage>
    <UILanguage>de-DE</UILanguage>
</SetupUILanguage>
<InputLocale>0407:00000407</InputLocale>
<SystemLocale>de-DE</SystemLocale>
<UILanguage>de-DE</UILanguage>
<UserLocale>de-DE</UserLocale>
```

## partition and installation | Microsoft-Windows-Setup

### product key

[KMS Keys](https://learn.microsoft.com/en-us/windows-server/get-started/kms-client-activation-keys?tabs=server2022%2Cwindows10ltsc%2Cversion1803%2Cwindows81)

Windows 11 Enterprise
```xml
<UserData>
	<ProductKey>
		<Key>NPPR9-FWDCX-D2C8J-H872K-2YT43</Key>
	</ProductKey>
	<AcceptEula>true</AcceptEula>
</UserData>
```

### where to install
```xml
<ImageInstall>
	<OSImage>
		<InstallTo>
			<DiskID>0</DiskID>
			<PartitionID>3</PartitionID>
		</InstallTo>
	</OSImage>
</ImageInstall>
```

### partition drive 0

<span style="color:red">ATTENTION: this deletes everything on drive 0</span>

```xml
<RunSynchronous>
	<RunSynchronousCommand wcm:action="add">
		<Order>1</Order>
		<Path>cmd.exe /c "&gt;&gt;"X:\diskpart.txt" echo SELECT DISK=0"</Path>
	</RunSynchronousCommand>
	<RunSynchronousCommand wcm:action="add">
		<Order>2</Order>
		<Path>cmd.exe /c "&gt;&gt;"X:\diskpart.txt" echo CLEAN"</Path>
	</RunSynchronousCommand>
	<RunSynchronousCommand wcm:action="add">
		<Order>3</Order>
		<Path>cmd.exe /c "&gt;&gt;"X:\diskpart.txt" echo CONVERT GPT"</Path>
	</RunSynchronousCommand>
	<RunSynchronousCommand wcm:action="add">
		<Order>4</Order>
		<Path>cmd.exe /c "&gt;&gt;"X:\diskpart.txt" echo CREATE PARTITION EFI SIZE=500"</Path>
	</RunSynchronousCommand>
	<RunSynchronousCommand wcm:action="add">
		<Order>5</Order>
		<Path>cmd.exe /c "&gt;&gt;"X:\diskpart.txt" echo FORMAT QUICK FS=FAT32"</Path>
	</RunSynchronousCommand>
	<RunSynchronousCommand wcm:action="add">
		<Order>6</Order>
		<Path>cmd.exe /c "&gt;&gt;"X:\diskpart.txt" echo CREATE PARTITION MSR SIZE=16"</Path>
	</RunSynchronousCommand>
	<RunSynchronousCommand wcm:action="add">
		<Order>7</Order>
		<Path>cmd.exe /c "&gt;&gt;"X:\diskpart.txt" echo CREATE PARTITION PRIMARY"</Path>
	</RunSynchronousCommand>
	<RunSynchronousCommand wcm:action="add">
		<Order>8</Order>
		<Path>cmd.exe /c "&gt;&gt;"X:\diskpart.txt" echo FORMAT QUICK FS=NTFS"</Path>
	</RunSynchronousCommand>
	<RunSynchronousCommand wcm:action="add">
		<Order>9</Order>
		<Path>cmd.exe /c "diskpart.exe /s "X:\diskpart.txt" &gt;&gt;"X:\diskpart.log" || ( type "X:\diskpart.log" &amp; echo diskpart encountered an error. &amp; pause &amp; exit /b 1 )"</Path>
	</RunSynchronousCommand>
```

### bypass windows 11 restrictions
```xml
	<RunSynchronousCommand wcm:action="add">
		<Order>10</Order>
		<Path>reg.exe add "HKLM\SYSTEM\Setup\LabConfig" /v BypassTPMCheck /t REG_DWORD /d 1 /f</Path>
	</RunSynchronousCommand>
	<RunSynchronousCommand wcm:action="add">
		<Order>11</Order>
		<Path>reg.exe add "HKLM\SYSTEM\Setup\LabConfig" /v BypassSecureBootCheck /t REG_DWORD /d 1 /f</Path>
	</RunSynchronousCommand>
	<RunSynchronousCommand wcm:action="add">
		<Order>12</Order>
		<Path>reg.exe add "HKLM\SYSTEM\Setup\LabConfig" /v BypassRAMCheck /t REG_DWORD /d 1 /f</Path>
	</RunSynchronousCommand>
</RunSynchronous>
```

<hr>
<span style="color:red">The next section is on 	`settings pass="specialize"`</span>
<hr>

## disable and delete windows recovery
[ReAgentc.exe /disable](https://learn.microsoft.com/de-de/windows-hardware/manufacture/desktop/reagentc-command-line-options?view=windows-11#disable)
```xml
<RunSynchronous>
    <RunSynchronousCommand wcm:action="add">
        <Order>1</Order>
        <Path>ReAgentc.exe /disable</Path>
    </RunSynchronousCommand>
    <RunSynchronousCommand wcm:action="add">
        <Order>2</Order>
        <Path>cmd.exe /c "del /a /f "C:\Windows\System32\Recovery\Winre.wim""</Path>
    </RunSynchronousCommand>
```

## skip OOBE and load defaults
```xml
    <RunSynchronousCommand wcm:action="add">
        <Order>3</Order>
        <Path>reg.exe add "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\OOBE" /v
            BypassNRO /t REG_DWORD /d 1 /f</Path>
    </RunSynchronousCommand>
    <RunSynchronousCommand wcm:action="add">
        <Order>4</Order>
        <Path>reg.exe load "HKU\DefaultUser" "C:\Users\Default\NTUSER.DAT"</Path>
    </RunSynchronousCommand>
    <RunSynchronousCommand wcm:action="add">
        <Order>5</Order>
        <Path>reg.exe unload "HKU\DefaultUser"</Path>
    </RunSynchronousCommand>
```

## set geolocation to germany
```xml
    <RunSynchronousCommand wcm:action="add">
        <Order>6</Order>
        <Path>reg.exe add "HKU\DefaultUser\Software\Microsoft\Windows\CurrentVersion\RunOnce" /v "GeoLocation" /t REG_SZ /d "powershell.exe -NoProfile -Command \"Set-WinHomeLocation -GeoId 94;\"" /f</Path>
    </RunSynchronousCommand>
```

## password doesnt expire

```xml
    <RunSynchronousCommand wcm:action="add">
        <Order>7</Order>
        <Path>net.exe accounts /maxpwage:UNLIMITED</Path>
    </RunSynchronousCommand>
</RunSynchronous>
```

