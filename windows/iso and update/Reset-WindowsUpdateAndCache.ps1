Write-Host -f blue "Reset Windows Update and Cache"
Write-Host 

Write-Host "reset startuptypes"

Write-Host .
Write-Host -f yellow "reset StartupType wuaserv"
Set-Service -Name wuauserv -StartupType Automatic

Write-Host .
Write-Host -f yellow "reset StartupType BITS"
Set-Service -Name BITS -StartupType Automatic

Write-Host .
Write-Host -f yellow "reset StartupType cryptSvc"
Set-Service -Name cryptSvc -StartupType Automatic

Write-Host

Write-Host "stopping services"

Write-Host .
Write-Host -f yellow "stop wuauserv"
Stop-Service wuauserv

Write-Host .
Write-Host -f yellow "stop BITS"
Stop-Service BITS

Write-Host .
Write-Host -f yellow "stop cryptSvc"
Stop-Service cryptSvc -Force

Write-Host .
Write-Host -f yellow "stop msiserver"
Stop-Service msiserver

Write-Host "renaming cache folder"

Write-Host .
Write-Host -f yellow "renaming SoftwareDistribution"
If (Test-Path "C:\Windows\SoftwareDistribution.bak") {
	Remove-Item "C:\Windows\SoftwareDistribution.bak" -Force -Recurse
}
Ren C:\Windows\SoftwareDistribution SoftwareDistribution.bak

Write-Host .
Write-Host -f yellow "renaming catroot2"
If (Test-Path "C:\Windows\system32\catroot2.bak") {
	Remove-Item "C:\Windows\system32\catroot2.bak" -Force -Recurse
}
Ren C:\Windows\system32\catroot2 catroot2.bak

Write-Host "start services"

Write-Host .
Write-Host -f green "starting msiserver"
Start-Service msiserver

Write-Host .
Write-Host -f green "starting cryptSvc"
Start-Service cryptSvc

Write-Host .
Write-Host -f green "starting BITS"
Start-Service BITS

Write-Host .
Write-Host -f green "starting wuauserv"
Start-Service wuauserv

Write-Host 

Get-Service wuauserv, BITS, cryptSvc, msiserver | Select-Object -Property Name, Status | FT
PAUSE