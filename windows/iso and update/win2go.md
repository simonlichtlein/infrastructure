# Make a Windows to go Stick

or for preinstalling windows on an drive

## This also works for skipping the prerequisites check in Windows 11 without having to make a registry change or to use a script

## Prerequisites

1. ready to use Windows ISO
2. connected HDD/SSD/USB on which you want to install Windows

## 1. Clean and partition the drive

All following commands needs to be executed in `diskpart`

### 1.1 select the desired disk. ATTENTION: If you select the wrong disk you wipe your data or system

```bash
DISKPART> list disk    

  Disk      Status         Size     Free     Dyn  Gpt
  --------  -------------  -------  -------  ---  ---
  Disk 0    Online         1863 GB  1024 KB        * 
  Disk 1    Online          931 GB  1024 KB        * 
  Disk 2    Online          931 GB      0 B        * 
  Disk 3    Online          465 GB  1024 KB        * 
  Disk 4    Online           14 GB      0 B

DISKPART> select disk 4

Disk 4 is now the selected disk.

DISKPART> clean

DiskPart succeeded in cleaning the disk.
```

### 1.2 option GPT (for "newer"/UEFI systems)

```bash
# initalize disk as GPT
convert gpt
# create efi partition
create partition efi size=500
format quick fs=fat32
assign letter="S"
```

### 1.2 option MBR (for "older"/BIOS systems)

```bash
# initalize disk as MBR
convert mbr
# create boot partition
create partition primary size=100
format quick fs=ntfs label="System"
assign letter="S"
active 
```

### 1.3 create primary partition (for GPT and MBR)

```bash
create partition primary
format quick fs=ntfs
assign letter="W"
```

## 2. Copy Windows

### 2.1 mount your ISO

Doubleclick the file or use powershell

```powershell
Mount-DiskImage -ImagePath "P:\OS\22000.346.Cobalt-21H2-SAC_EN.ISO"
```

In DISKPART you could `list volume` to see all volumes with drive letters. Usefull for live booting

### 2.2 find the desired version

```powershell
dism /Get-WimInfo /WimFile:D:\Sources\install.wim
  
Deployment Image Servicing and Management tool
Version: 10.0.22000.1

Details for image : E:\Sources\install.wim

Index : 1
Name : Windows 11 Home
Description : Windows 11 Home
Size : 16.626.993.875 bytes
[...]
Index : 5
Name : Windows 11 Enterprise
Description : Windows 11 Enterprise
Size : 16.784.650.067 bytes
[...]
Index : 15
Name : Windows 11 IoT Enterprise
Description : Windows 11 IoT Enterprise
Size : 16.784.650.027 bytes

The operation completed successfully.
```

Index 5 in my case is Windows 11 Enterprise. You can get more information with the following dism command

```bash
dism /Get-WimInfo /WimFile:E:\Sources\install.wim /Index:5

Deployment Image Servicing and Management tool
Version: 10.0.22000.1

Details for image : E:\Sources\install.wim

Index : 5
Name : Windows 11 Enterprise
Description : Windows 11 Enterprise
Size : 16.784.650.067 bytes
WIM Bootable : No
Architecture : x64
Hal : <undefined>
Version : 10.0.22000
ServicePack Build : 346
ServicePack Level : 0
Edition : Enterprise
Installation : Client
ProductType : WinNT
ProductSuite : Terminal Server
System Root : WINDOWS
Directories : 36816
Files : 146276
Created : 12.11.2021 - 21:14:38
Modified : 12.11.2021 - 21:27:17
Languages :
        en-US (Default)

The operation completed successfully.
```

### 2.3 apply the image to drive

```bash
dism /Apply-Image /ImageFile:E:\Sources\install.wim /Index:5 /ApplyDir:W:\
```

### 2.4 create needed boot files

If your system cannot find bcdboot it is located at `C:\Windows\System32\bcdboot.exe`

```bash
bcdboot W:\Windows /s S:\ /f ALL
```

You can also speficy if you only want BIOS or UEFI bootfiles, but it is save to use `ALL`
<hr>

## Now you have a bootable Windows on your drive
