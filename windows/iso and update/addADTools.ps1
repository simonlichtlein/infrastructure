# sometimes while using an wsus server the Windows Feature cant be added for some reasen. -> disable the use of wsus server if key is present

$useWU = $false 

if (0 -eq (Get-ItemProperty -Path HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU -Name UseWUServer)) {
    Set-ItemProperty -Path HKLM:SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU -Name UseWUServer -Value 0
    Restart-Service -Name wuauserv -Force
    $useWU = $true
}

Get-WindowsCapability -Name "Rsat.ActiveDirectory.DS-LDS.Tools~~~~0.0.1.0" -Online | Add-WindowsCapability -Online

# reenable wsus
if ($useWU) {
    Set-ItemProperty -Path HKLM:SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU -Name UseWUServer -Value 1
    Restart-Service -Name wuauserv -Force
    Restart-Computer
}