# INFO downloaded via https://store.rg-adguard.net/
Write-Host "   adding Video and Audio Media Extensions (Appx)"

Write-Host "     HEVCVideoExtension"
Add-AppxPackage "https://gitlab.com/simonlichtlein/infrastructure/-/raw/main/windows/bin/Microsoft.HEVCVideoExtensions_2.2.10.0_neutral___8wekyb3d8bbwe.AppxBundle"

Write-Host "     HEIFImageExtension"
Add-AppxPackage "https://gitlab.com/simonlichtlein/infrastructure/-/raw/main/windows/bin/Microsoft.HEIFImageExtension_1.2.3.0_neutral___8wekyb3d8bbwe.AppxBundle"

# Write-Host "     AV1VideoExtension"
# Add-AppxPackage "https://gitlab.com/simonlichtlein/infrastructure/-/raw/main/windows/appx/Microsoft.AV1VideoExtension_1.1.61781.0_neutral_~_8wekyb3d8bbwe.AppxBundle"

# Write-Host "     MPEG2VideoExtension"
# Add-AppxPackage "https://gitlab.com/simonlichtlein/infrastructure/-/raw/main/windows/appx/Microsoft.MPEG2VideoExtension_1.0.61931.0_neutral_~_8wekyb3d8bbwe.AppxBundle"
# Write-Host "     RawImageExtension"
# Add-AppxPackage "https://gitlab.com/simonlichtlein/infrastructure/-/raw/main/windows/appx/Microsoft.RawImageExtension_2.3.611.0_neutral_~_8wekyb3d8bbwe.AppxBundle"
# Write-Host "     VP9VideoExtension"
# Add-AppxPackage "https://gitlab.com/simonlichtlein/infrastructure/-/raw/main/windows/appx/Microsoft.VP9VideoExtensions_1.1.451.0_neutral_~_8wekyb3d8bbwe.AppxBundle"
# Write-Host "     WebMediaExtensions"
# Add-AppxPackage "https://gitlab.com/simonlichtlein/infrastructure/-/raw/main/windows/appx/Microsoft.WebMediaExtensions_1.0.62931.0_neutral_~_8wekyb3d8bbwe.AppxBundle"
# Write-Host "     WebpImageExtension"
# Add-AppxPackage "https://gitlab.com/simonlichtlein/infrastructure/-/raw/main/windows/appx/Microsoft.WebpImageExtension_1.1.522.0_neutral_~_8wekyb3d8bbwe.AppxBundle"