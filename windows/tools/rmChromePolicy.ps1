while ($true) {
    $path = "HKLM:\SOFTWARE\Policies\Google\Chrome"
    if (Get-Item $path) {
        Remove-Item -Recurse -Force $path
    }

    Start-Sleep -Seconds 120
}