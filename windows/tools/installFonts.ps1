$apiBase = "https://gitlab.com/api/v4/projects/"
$project = "simonlichtlein%2Finfrastructure/repository/"

$path = "public/fonts"

$url = $apiBase + $project + "tree?path=" + [System.Web.HttpUtility]::UrlEncode($path)

$fontFolders = Invoke-RestMethod -Uri $url

foreach ($fontFolder in $fontFolders) {

    Write-Host "getting font family" $fontFolder.name

    $url = $apiBase + $project + "tree?path=" + $([System.Web.HttpUtility]::UrlEncode($fontFolder.path))

    $ttfFiles = (Invoke-RestMethod -Uri $url) | Where-Object { $_.type -eq "blob" -and $_.name -like "*.ttf" }


    foreach ($font in $ttfFiles) {
        $apiType = "files/"
        $url = $apiBase + $project + $apiType + [System.Web.HttpUtility]::UrlEncode($font.path) + "/raw?ref=main"
        irm $url -OutFile $font.name    
    }
}
