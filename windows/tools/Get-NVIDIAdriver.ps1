Write-Host "attempting to detect currently installed driver version.."
try {
    $wmi_vc = Get-WmiObject -ClassName Win32_VideoController | Where-Object { $_.Name -match "NVIDIA" }
    $installedVersion = ($wmi_vc.DriverVersion.Replace('.', '')[-5..-1] -join '').insert(3, '.')
    
    Write-Host "------------------------------"
    Write-Host -F green "  found"
    Write-Host "   "$wmi_vc.Name
    Write-Host "  with driver version"
    Write-Host -F blue "  "$installedVersion
    Write-Host "-------------------------------"
}
catch {
    Write-Host -F Yellow "no installed driver and/or nvidia card detected.."
    if ( (Read-Host "continue anyway? (y/n) ") -ne "y" ) { exit }
}

Write-Host "getting newest version.."
$link = Invoke-WebRequest -Uri 'https://www.nvidia.com/Download/processFind.aspx?psid=101&pfid=816&osid=57&lid=1&whql=1&lang=en-us&ctk=0&dtcid=1' -Method GET -UseBasicParsing
$link -match '<td class="gridItem">([^<]+?)</td>' | Out-Null
$version = $matches[1]
Write-Host "------------------------------"
Write-Host -F green "  latest version"
Write-Host -F blue "   "$version

if ($installedVersion -lt $version) {
    $exe = "$version-desktop-win10-win11-64bit-international-dch-whql.exe"
    $url = "https://international.download.nvidia.com/Windows/$version/$exe"
    # $rp_url = "https://international.download.nvidia.com/Windows/$version/$version-desktop-$windowsVersion-$windowsArchitecture-international-dch-whql-rp.exe"
    $dest = "$ENV:USERPROFILE\Downloads\$exe"
    Write-Host "  downloading"
    Write-Host -F DarkGray "   "$exe
    Write-Host -F DarkGray "    ("$url" )"
    Write-Host "  to location"
    Write-Host -F DarkGray "   "$dest
    Write-Host "------------------------------"

    $dest = "$ENV:USERPROFILE\Downloads\$exe"

    Start-BitsTransfer -Source $url -Destination $dest

    Start-Process -FilePath "C:\Program Files\7-Zip\7z.exe" -ArgumentList "x -o`"$dest.out`" `"$dest`"" -Wait -NoNewWindow
    Write-Host "------------------------------"
}