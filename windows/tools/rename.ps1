$items = (Get-ChildItem)

$i = 1

foreach ($item in $items) {
    $newname = $item.Name -replace 'E\d{3}', ('E{0:D3}' -f $i)
    Rename-Item $item.FullName -NewName $newname
    $i++
}


foreach ($item in $items) {
    $newname = $item.Name -replace '\d{3}.', ('One.Piece.S04E{0:D3}.' -f $i)
    Rename-Item $item.FullName -NewName $newname
    $i++
}