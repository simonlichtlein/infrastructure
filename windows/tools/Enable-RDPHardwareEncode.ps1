function CreateRegKeysRecursive($path) {
    Write-Verbose "checking $path"

    # split path at each \
    $split = $path -split '\\'

    $pathToCreate = ""
    for ($i = 0; $i -lt $split.Count; $i++) {

        $pathToCreate += $split[$i] + "\"

        Write-Verbose "checking split path $pathToCreate"

        if (!(Test-Path $pathToCreate)) {
            Write-Verbose "path does not exist - creating"
            New-Item -path $pathToCreate -ItemType Directory | Out-Null
        }
        else {
            Write-Verbose "path does exist - skipping"
        }
    }
}

function Merge-RegKey($path, $name, $value, $type) {
    CreateRegKeysRecursive $path
    New-ItemProperty -path $path -name $name -value $value -PropertyType $type -Force | Out-Null
}


# RDP
Write-Host "   enable RDP"
Merge-RegKey -path "HKLM:\SYSTEM\CurrentControlSet\Control\Terminal Server" -name "fDenyTSConnections" -value 0 -type DWORD
Merge-RegKey -path "HKLM:\SYSTEM\CurrentControlSet\Control\Lsa" -name "LimitBlankPasswordUse" -value 0 -type DWORD
Enable-NetFirewallRule -DisplayGroup "Remote Desktop"

# Computer Configuration\Administrative Templates\Windows Components\Remote Desktop Services\Remote Desktop Session Host\Device and Resource Redirection\ -> Allow audio and video playback redirection
Merge-RegKey -path "HKLM\SOFTWARE\Policies\Microsoft\Windows NT\Terminal Services" -name "fDisableCam" -value 0 -type DWORD

# Computer Configuration\Administrative Templates\Windows Components\Remote Desktop Services\Remote Desktop Session Host\Remote Session Environment\ -> Use hardware graphics adapters for all Remote Desktop Services sessions
Merge-RegKey -path "HKLM\SOFTWARE\Policies\Microsoft\Windows NT\Terminal Services" -name "bEnumerateHWBeforeSW" -value 1 -type DWORD

# Computer Configuration\Administrative Templates\Windows Components\Remote Desktop Services\Remote Desktop Session Host\Remote Session Environment\ -> Configure H.264/AVC hardware encoding for Remote Desktop Connections
Merge-RegKey -path "HKLM\SOFTWARE\Policies\Microsoft\Windows NT\Terminal Services" -name "AVCHardwareEncodePreferred" -value 1 -type DWORD

# Computer Configuration\Administrative Templates\Windows Components\Remote Desktop Services\Remote Desktop Session Host\Remote Session Environment\ -> Prioritize H.264/AVC 444 graphics mode for Remote Desktop Connections
Merge-RegKey -path "HKLM\SOFTWARE\Policies\Microsoft\Windows NT\Terminal Services" -name "AVC444ModePreferred" -value 1 -type DWORD