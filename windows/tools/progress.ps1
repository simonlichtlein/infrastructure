$timeout = 70
$elapsed = 0
$interval = 1

while ($elapsed -le $timeout) {
    Start-Sleep -Seconds $interval
    $elapsed += $interval

    Write-Progress -Activity "waiting for completion of network identifying..." -Status "$elapsed% Complete:" -PercentComplete $elapsed -CurrentOperation "Elapsed time: $elapsed seconds"

    # $identifyingProfiles = Get-NetConnectionProfile | Where-Object { ($_.Name -match "Netzwerkidentifizierung...") -or ($_.Name -match "Identifying...") }
    $identifyingProfiles = 5

    if ($identifyingProfiles.Count -eq 0) {
        Write-Progress -Activity "waiting for completion of network identifying..." -Status "100% Complete:" -PercentComplete 100
        break
    }
}


while ((Get-NetConnectionProfile | Where-Object { ($_.Name -match "Netzwerkidentifizierung...") -or ($_.Name -match "Identifying...") }).Count -ne 0) {
    Start-Sleep -Seconds 1
    
}