# infrastructure
This repo contains many different infrastructure related files and scripts.
linux and windows post installations
many services with docker-compose.yml files and more.

# auto mirrored to [lichtlein.dev/inf](https://lichtlein.dev/inf/)

# quick access
## linux post-install
```bash
curl -sSL https://lichtlein.dev/inf/linux/post-install/install.sh | bash
```