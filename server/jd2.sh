#!/bin/bash

# switch java versions
sudo update-alternatives --config java

# https://github.com/moktavizen/material-darker-jdownloader/?tab=readme-ov-file
# Set the installation directory
INSTALL_DIR="/home/sl/tools/jd2"

cd $INSTALL_DIR

# Clone the repository
git clone https://github.com/moktavizen/material-darker-jdownloader.git

# Apply the theme
cp -r material-darker-jdownloader/images "$INSTALL_DIR/themes/standard/org/jdownloader"
cp -r material-darker-jdownloader/laf "$INSTALL_DIR/cfg"
cp material-darker-jdownloader/flatlaf.jar "$INSTALL_DIR/libs/laf"

# Clean up
rm -rf material-darker-jdownloader

echo "Done!"

echo "
[Desktop Entry]
Name=JDownloader2
Comment=Download Manager
GenericName=JDownloader2
Keywords=downloader;jd
Exec=java -jar /opt/JDownloader/JDownloader.jar
Terminal=false               
Type=Application
Icon=/opt/JDownloader/themes/standard/org/jdownloader/images/logo/logo-128x128.png
StartupNotify=true
" | sudo tee /usr/share/applications/jdownloader2.desktop >/dev/null

echo "
[Desktop Entry]
Name=Java
Comment=Java
GenericName=Java
Keywords=java
Exec=java -jar %f
Terminal=false
X-MultipleArgs=false
Type=Application
MimeType=application/x-java-archive
StartupNotify=true
" | sudo tee /usr/share/applications/java.desktop >/dev/null
