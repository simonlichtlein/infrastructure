lsblk
sda                                                                                                      
├─sda1                                                                                                   
├─sda2                  xfs                        21b2b359-5664-4fdb-b753-2dc17d703338    620.7M    35% /boot
└─sda3                  LVM2_member LVM2 001       0xUobv-dpQe-B5Nl-LMt1-4tND-qNjK-KqDrfe                
  └─fedora_slv--vault-root
                        xfs                        346ce4f8-34f9-4b24-8dab-a858e73920d0      1.3G    91% /

vgdisplay

lvextend -l 100%FREE /dev/fedora_slv-vault-root

xfs_growfs /

lsblk 

sda                                                                                                      
├─sda1                                                                                                   
├─sda2                  xfs                        21b2b359-5664-4fdb-b753-2dc17d703338    620.7M    35% /boot
└─sda3                  LVM2_member LVM2 001       0xUobv-dpQe-B5Nl-LMt1-4tND-qNjK-KqDrfe                
  └─fedora_slv--vault-root
                        xfs                        346ce4f8-34f9-4b24-8dab-a858e73920d0       17G    45% /
