#!/bin/bash

# jq needs to be installed

# Define the subdomains you want to update
DOMAINS_TO_SET=(
    "*.lan.lichtlein.dev"
)

# Get the current IP address
IP=$(curl -s https://checkip.amazonaws.com/)

# Your API keys
APIKEY="pk1"
SECRETAPIKEY="sk1"

sendMail() {
    OPTIND=1
    subject=""
    text=""
    while getopts "s:t:" flag; do
        case $flag in
        s) subject=${OPTARG} ;;
        t) text=${OPTARG} ;;
        esac
    done

    swaks --from admin@lichtlein.dev \
        --to admin@lichtlein.dev \
        --server smtp.hostinger.com \
        --port 587 \
        --auth LOGIN \
        --auth-user simon@lichtlein.dev \
        --auth-password mailpw! \
        --tls \
        --header "Subject: [lxc-auth] $subject" \
        --body "$text" >/dev/null 2>&1
}

# Function to parse the sub- and rootdomain
parse_domain() {
    local domain="$1"
    sub_domain=$(echo "$domain" | sed 's/\(.*\)\.[^\.]*\.[^\.]*$/\1/')
    root_domain=$(echo "$domain" | sed 's/.*\.\([^\.]*\.[^\.]*\)$/\1/')
    echo "$sub_domain $root_domain"
}

check_current_A_record() {
    local sub_domain="$1"
    local root_domain="$2"

    url="https://api.porkbun.com/api/json/v3/dns/retrieveByNameType/$root_domain/A/$sub_domain"
    response=$(
        curl -s -X POST "$url" \
            -H "Content-Type: application/json" \
            -d '{
                    "secretapikey":"'"$SECRETAPIKEY"'",
                    "apikey":"'"$APIKEY"'"
                }'
    )

    id=$(echo "$response" | jq -r '.records[0].id')
    id_set_ip=$(echo "$response" | jq -r '.records[0].content')
    echo "$id" "$id_set_ip"
}

# read /caddy/lastip.lock
lastip=$(cat /caddy/lastip.lock)

if [ "$IP" == "$lastip" ]; then
    echo "IP has not changed - skipping update"
    exit 0
else
    echo "IP has changed - updating records"
    echo "$IP" >/caddy/lastip.lock
fi

# Loop through each subdomain to update DNS records
for domain in "${DOMAINS_TO_SET[@]}"; do

    result=$(parse_domain "$domain")
    read sub_domain root_domain <<<"$result"
    echo "Checking DNS record for subomdain '$sub_domain' on domain '$root_domain'"

    result=$(check_current_A_record "$sub_domain" "$root_domain")
    read id id_set_ip <<<"$result"
    echo "  the id is '$id' and the current set ip is '$id_set_ip'"

    if [ "$id_set_ip" != "$IP" ]; then
        echo "  updating DNS record"
        url="https://api.porkbun.com/api/json/v3/dns/edit/$root_domain/$id"
        response=$(
            curl -s -X POST "$url" \
                -H "Content-Type: application/json" \
                -d '{
                    "secretapikey": "'"$SECRETAPIKEY"'",
                    "apikey": "'"$APIKEY"'",
                    "name": "'"$sub_domain"'",
                    "type": "A",
                    "content": "'"$IP"'",
                    "ttl": "600"
                }'
        )
        echo "  response: $response"
        sendMail -s "$sub_domain.$root_domain - Updated A-record" -t "The A-record for $sub_domain.$root_domain was updated to $IP with status: <br> $response"
    else
        echo "  no change in IP for - skipping update"
        sendMail -s "$sub_domain.$root_domain - No change in A-record" -t "The A-record for $sub_domain.$root_domain was not updated because the IP is still $IP"
    fi
    echo "  "
done
