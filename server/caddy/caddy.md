# Setup LXC container and caddy as reverse proxy and ddclient as dynamic DNS
This setup is used to create an LXC container on an Proxmox host, build an custom caddy binary for cloudflare dns, sets it up and run it.<br>
Also sets up an dynamic dns client to update the chaning public ISP IPv4 to your domain. 

Im using 
- [desec.io](https://desec.io/) as nameserver for my domain lichtlein.dev
- [caddy](https://caddyserver.com/) as reverse proxy

**Table of Contents**
- [Setup LXC container and caddy as reverse proxy and ddclient as dynamic DNS](#setup-lxc-container-and-caddy-as-reverse-proxy-and-ddclient-as-dynamic-dns)
- [LXC container installation](#lxc-container-installation)
    - [tl;dr Fedora Cloud Image](#tldr-fedora-cloud-image)
  - [Get the latest image](#get-the-latest-image)
  - [Create the LXC and boot it](#create-the-lxc-and-boot-it)
  - [Make an backup and snapshot](#make-an-backup-and-snapshot)
- [LXC setup](#lxc-setup)
  - [Caddy reverse proxy](#caddy-reverse-proxy)
    - [Add www-user for execution of caddy](#add-www-user-for-execution-of-caddy)
    - [Create /caddy directory](#create-caddy-directory)
    - [Build a custom caddy binary with cloudflare and logging](#build-a-custom-caddy-binary-with-cloudflare-and-logging)
    - [Create an systemd service for caddy](#create-an-systemd-service-for-caddy)
    - [Create the caddy file or use your existing](#create-the-caddy-file-or-use-your-existing)
    - [Now your `/caddy` directory should look like this](#now-your-caddy-directory-should-look-like-this)
    - [Add permission to bind ports](#add-permission-to-bind-ports)
    - [Enable and start systemd service](#enable-and-start-systemd-service)
- [Update the caddyfile](#update-the-caddyfile)
- [Update caddy binary](#update-caddy-binary)
- [How to read the logs](#how-to-read-the-logs)

# LXC container installation
The following happens on the Proxmox host. I am connected via `ssh root@sop-router.lan`

I'm using Feodra 40 Cloud image. More information found at [fedoraproject.org/cloud](https://fedoraproject.org/cloud/)

### tl;dr Fedora Cloud Image 
A Fedora Cloud Image is a pre-built virtual machine disk image optimized for cloud environments.

Why Use It for LXC Containers?
- Optimized for Virtualization
- Integrated into Fedora’s QA process.
- Lightweight and Flexible

Its also available as iso for normal VMs.

## Get the latest image
Check for the latest release of the os at [images.linuxcontainers.org](https://images.linuxcontainers.org/images/fedora/40/amd64/cloud/)

You need the `rootfs.tar.xz` file. For example:

`https://images.linuxcontainers.org/images/fedora/40/amd64/cloud/20240921_20%3A33/rootfs.tar.xz`

Download the image to Proxmox template directory:
```shell
TEMPLATE_NAME="fedora-40-cloud_20240921.tar.xz"
wget -O /var/lib/vz/template/cache/$TEMPLATE_NAME 'https://images.linuxcontainers.org/images/fedora/40/amd64/cloud/20240921_20%3A33/rootfs.tar.xz'
```

## Create the LXC and boot it
Get your ssh keys and set the root pw. Of course you could simply write the ssh-keys to an file manually or directly at the `pct create` command.
```shell
SSHKEYS="https://lichtlein.dev/pub/keys.pub"
curl -o /tmp/ssh-keys.txt $SSHKEYS
LXCROOTPW="verysecure"
```
Please rewiew the command and change the settings to match your system. Im using my own hostname sheme and network and storage setup.
```shell
pct create 103 /var/lib/vz/template/cache/$TEMPLATE_NAME \ 
    --hostname lxc-rproxy \
    --rootfs local-lvm:10 --memory 2048 --cores 1 \
    --net0 name=eth0,bridge=lan,ip=dhcp \
    --ssh-public-keys /tmp/ssh-keys.txt \
    --password $LXCROOTPW

pct start 103
```

##  Make an backup and snapshot
so you can revert to it if you do something wrong or want to start over. Of course you could also do the backup via the Proxmox GUI.
```shell
vzdump 103 --storage local --mode snapshot
pct snapshot 103 inital
```

# LXC setup
Switch to the LXC container with
`ssh root@lxc-rproxy.lan`

Make an system upgrade `dnf upgrade -y`

## Caddy reverse proxy
### Add www-user for execution of caddy
```shell
groupadd www-data
useradd -g www-data www-data
```

### Create /caddy directory
In this folders everything for caddy proxy will be located. 
This is my approach to keep everything in one place. Of course you could also use the default caddy directories or create your own.

```shell
mkdir /caddy/
chown www-data:www-data /caddy

mkdir /caddy/log
chown www-data:www-data /caddy/log
```

### Build a custom caddy binary with cloudflare and logging
```bash
dnf install golang -y

# this puts all go output to /caddy. so xcaddy is installed at /caddy/xcaddy
export GOBIN=/caddy
export GOPROXY=https://proxy.golang.org,direct

go install github.com/caddyserver/xcaddy/cmd/xcaddy@latest
/caddy/xcaddy build --with github.com/caddy-dns/desec --with github.com/caddyserver/caddy/v2/modules/logging


# check your built caddy
/caddy/caddy --version
    v2.8.4 h1:q3pe0wpBj1OcHFZ3n/1nl4V4bxBrYoSoab7rL9BMYNk=

# change owner and execution for the caddy binary
chown www-data:www-data /caddy/caddy
chmod +x /caddy/caddy
```

### Create an systemd service for caddy
```bash
echo "
[Unit]
Description=Caddy web server
After=network.target

[Service]
Environment="DESEC_API_TOKEN=your_desec_token CLOUDFLARE_API_TOKEN=your_cloudflare_token"
ExecStart=/caddy/caddy run --config /caddy/caddyfile
Restart=failure
User=www-data
Group=www-data

[Install]
WantedBy=multi-user.target
" > /etc/systemd/system/caddy.service

systemctl daemon-reload
# create symlink to access the service file in the caddy dir
ln -s /etc/systemd/system/caddy.service /caddy/caddy.service
```

### Create the caddy file or use your existing
```bash
vim /caddy/caddyfile
```
example caddy file. 
```json
{
	# TLS options
	email you@your.domain
	acme_ca https://acme-v02.api.letsencrypt.org/directory

	# Logging
	log {
		output file /caddy/log/default.log {
			roll_size 100MiB
			roll_keep 4
		}
	}
}

myservice.your.domain {
    log {
		output file /caddy/log/myservice.log
	}
    reverse_proxy yourlocalip:3001
    tls {
        dns cloudflare {env.CLOUDFLARE_API_TOKEN}
		resolvers 1.1.1.1
    }
}

myservice.your.domain {
    log {
		output file /caddy/log/myservice.log
	}
    reverse_proxy yourlocalip:3001
    tls {
	    dns desec { token env.DESEC_API_TOKEN }
	}
}   
```
format the json 
```bash
/caddy fmt --overwrite caddyfile
```

### Now your `/caddy` directory should look like this
![/caddy directory](dir_caddy.png)

### Add permission to bind ports
You need to do this everytime the binary changes
```bash
setcap cap_net_bind_service=+ep /caddy/caddy
```

### Enable and start systemd service
```bash
systemctl enable caddy
systemctl start caddy
systemctl status caddy
```

# Update the caddyfile
```bash
# make changes to the caddyfile and simply do
systemctl restart caddy
```

# Update caddy binary
```bash
cd /caddy/
export GOBIN=/caddy
export GOPROXY=https://proxy.golang.org,direct

/caddy/xcaddy build --output caddy_new --with github.com/caddy-dns/desec --with github.com/caddy-dns/porkbun --with github.com/caddyserver/caddy/v2/modules/logging

# now be fast or do it at night ;)
systemctl stop caddy
rm caddy
mv caddy_new caddy
chown www-data:www-data /caddy/caddy
chmod +x /caddy/caddy
setcap cap_net_bind_service=+ep /caddy/caddy
systemctl start caddy
```

# How to read the logs
Logs from systemctl
```bash
journalctl -f -u caddy
```

Analyse your custom caddy logs with jq

```bash
dnf install jq -y
cat /caddy/log/portainer.log | jq
cat /caddy/log/portainer.log | jq .request.uri
cat /caddy/log/portainer.log | jq .common_log
cat /caddy/log/portainer.log | jq '.request.uri, .request.remote_addr'
```