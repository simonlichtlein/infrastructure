# proxmox pcie passthrough for (i)gpu (Intel Integrated Graphics)¶
- [PCI(e) officiall docu](https://pve.proxmox.com/wiki/PCI(e)_Passthrough)
- [3os iGPU Passthrough](https://3os.org/infrastructure/proxmox/gpu-passthrough/igpu-passthrough-to-vm/)
- [3os iGPU split Passthrough](https://3os.org/infrastructure/proxmox/gpu-passthrough/igpu-split-passthrough/)
- [old official documentation](https://pve.proxmox.com/wiki/PCI_Passthrough)

## edit kernel boot command
```bash
vim /etc/default/grub
    GRUB_CMDLINE_LINUX_DEFAULT="quiet intel_iommu=on iommu=pt"
    GRUB_CMDLINE_LINUX_DEFAULT="quiet intel_iommu=on iommu=pt pcie_acs_override=downstream,multifunction initcall_blacklist=sysfb_init video=simplefb:off video=vesafb:off video=efifb:off video=vesa:off disable_vga=1 vfio_iommu_type1.allow_unsafe_interrupts=1 kvm.ignore_msrs=1 modprobe.blacklist=radeon,nouveau,nvidia,nvidiafb,nvidia-gpu,snd_hda_intel,snd_hda_codec_hdmi,i915"
```
### update grup
```bash
update-grub
```

## add kernerl modules
```bash
vim /etc/modules
    vfio
    vfio_iommu_type1
    vfio_pci
    vfio_virqfd

    # Intel GVT-g Split
    kvmgt
```
### generate initramfs and reboot
```bash
update-initramfs -u -k all
reboot
```

### check if iommu is enabled and groups are existent after reboot
```bash
dmesg | grep -e DMAR -e IOMMU
# you should see something like 
    [    0.047331] DMAR: IOMMU enabled
    ...
    [    0.354574] DMAR: Intel(R) Virtualization Technology for Directed I/O
```
### get info about your gpu
```bash
lspci -nnv | grep VGA
```
```txt
i5-13500T
00:02.0 VGA compatible controller [0300]: Intel Corporation AlderLake-S GT1 [8086:4680] (rev 0c) (prog-if 00 [VGA controller])

i5-12500T 
00:02.0 VGA compatible controller [0300]: Intel Corporation Alder Lake-S GT1 [UHD Graphics 770] [8086:4690] (rev 0c) (prog-if 00 [VGA controller])
```

![pcie device in ui](proxmox%20pcie%20passthrough%201.png)
