#!/bin/bash

bash -c "$(wget -qLO - https://github.com/tteck/Proxmox/raw/main/misc/post-pve-install.sh)"

bash -c "$(wget -qLO - https://github.com/tteck/Proxmox/raw/main/misc/scaling-governor.sh)"

bash -c "$(wget -qLO - https://github.com/tteck/Proxmox/raw/main/misc/microcode.sh)"

HOME="/home/$USER"

SSHKEYS="https://lichtlein.dev/pub/keys.pub"
curl -s "$SSHKEYS" >>"${HOME}/.ssh/authorized_keys"
chmod 700 "${HOME}/.ssh"
chmod 600 "${HOME}/.ssh/authorized_keys"

SUDOCONF=${SUDOCONF:-"https://lichtlein.dev/pub/linux/config/sudoconf"}
sh -c "curl -s $SUDOCONF > '/etc/sudoers.d/10-sudo'"
SUDOLECTURE=${SUDOLECTURE:-"https://lichtlein.dev/pub/linux/config/sudoers.lecture"}
sh -c "curl -s $SUDOLECTURE > '/etc/sudoers.d/sudoers.lecture'"

# TODO not working
MOTD=${MOTD:-"https://lichtlein.dev/pub/linux/config/motd/motd"}
sh -c "curl -s $MOTD > '/etc/motd'"
chmod 755 /etc/motd

# eza - check if repo is available (< debian 12 / ubuntu 24)

if ! dpkg -s eza >/dev/null 2>&1; then
    trusted_gpg="/etc/apt/trusted.gpg.d"
    sources_list="/etc/apt/sources.list.d"
    keyring="gierens.gpg"
    wget -qO- https://raw.githubusercontent.com/eza-community/eza/main/deb.asc | gpg --batch --yes --dearmor -o $trusted_gpg/$keyring
    echo "deb [signed-by=$trusted_gpg/$keyring] http://deb.gierens.de stable main" | tee $sources_list/gierens.list
    chmod 644 $trusted_gpg/$keyring $sources_list/gierens.list
fi
apt update >/dev/null 2>&1
apt install -y man-db gpg make net-tools dnsutils git curl wget eza btop bat figlet vim zsh chrony tmux ca-certificates clinfo clang make

git clone --depth=1 https://github.com/amix/vimrc.git ${HOME}/.vim_runtime
sh ${HOME}/.vim_runtime/install_awesome_vimrc.sh

# TODO not working
ZSHRC="https://lichtlein.dev/pub/linux/config/zshrc"
sh -c "$(wget -O- https://install.ohmyz.sh/)" "" --unattended
curl -s $ZSHRC >${HOME}/.zshrc

# TODO not working
BASHRC="https://lichtlein.dev/pub/linux/config/bashrc"
curl -s $BASHRC >${HOME}/.bashrc

apt upgrade -y
apt autoremove -y

reboot

journalctl -k | grep -E "microcode" | head -n 1
