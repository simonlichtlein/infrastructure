`apt install usbutils`

### identify usb device
```sh
lsusb
   Bus 001 Device 004: ID 1a86:55d4 QinHeng Electronics SONOFF Zigbee 3.0 USB Dongle Plus V2
```

### create udev rule and reload the ruleset
```sh
vim /etc/udev/rules.d/99-usb-serial.rules
   SUBSYSTEM=="tty", ATTRS{idVendor}=="1a86", ATTRS{idProduct}=="55d4" SYMLINK+="ttyZIGBEE0"
   KERNEL=="ttyACM[0-9]*",MODE="0666"

udevadm control --reload-rules
udevadm trigger
```

### modify lxc container
```sh
vim /etc/pve/lxc/103.conf
   lxc.mount.entry: /dev/ttyZIGBEE0 dev/ttyACM0 none bind,optional,create=file
```

restart container with `pct reboot 103`
