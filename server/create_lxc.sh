#!/bin/bash

function printc() {
    PARAM_NAME=""
    PARAM_CORES=2

    # TODO optional cores defaults to 1 and memory two 512
    while getopts "name:cores:memory:network:rootpw" flag; do
        case $flag in
        name) PARAM_NAME=${OPTARG} ;;
        cores) PARAM_LXCNAME=${OPTARG} ;;
        esac
    done

}

TEMPLATE_NAME="fedora-41-default-20241118.tar.xz"
wget -O /var/lib/vz/template/cache/$TEMPLATE_NAME 'https://fra1lxdmirror01.do.letsbuildthe.cloud/images/fedora/41/amd64/default/20241118_20%3A33/rootfs.tar.xz'

SSHKEYS="https://lichtlein.dev/pub/keys.pub"
curl -o /tmp/ssh-keys.txt $SSHKEYS
LXCROOTPW="verysecure"

pct create 103 /var/lib/vz/template/cache/$TEMPLATE_NAME \ 
--hostname $PARAM_NAME \
    --rootfs local-lvm:10 --memory 2048 --cores 1 \
    --net0 name=eth0,bridge=lan,ip=dhcp \
    --ssh-public-keys /tmp/ssh-keys.txt \
    --password $LXCROOTPW
