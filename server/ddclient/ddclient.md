# NOTE
i dont use ddclient anymore, i use an custom script because i only use the porkbun api
see server/caddy/updateArecords.sh

# adding dynamic dns

`sudo dnf install ddclient -y`

### create dir for ddclient files
```shell
sudo mkdir /ddclient
sudo chown ddclient:ddclient /ddclient
sudo ln /usr/lib/systemd/system/ddclient.service /ddclient/ddclient.service
```

`sudo vim /ddclient/ddclient.conf`
```bash
protocol=dyndns2
daemon=300
use=cmd, cmd='curl https://checkipv4.dedyn.io/'
ssl=yes
server=update.dedyn.io
login='*.lan.lichtlein.dev'
password='[token secret]'
*.lan.lichtlein.dev
```

### testing the command
```shell
sudo ddclient -daemon=0 -debug -verbose -noquiet -file /ddclient/ddclient.conf
```

### changing service
`sudo vim /ddclient/ddclient.service`
```shell
[Unit]
Description=Dynamic DNS Update Client
After=syslog.target network-online.target nss-lookup.target

[Service]
User=ddclient
Group=ddclient
Type=forking
PIDFile=/ddclient/ddclient.pid
ExecStartPre=/bin/touch /ddclient/ddclient.cache
ExecStart=/usr/sbin/ddclient -verbose -debug -file /ddclient/ddclient.conf -cache /ddclient/ddclient.cache -pid /ddclient/ddclient.pid

[Install]
WantedBy=multi-user.target
```

### start service
```shell
sudo systemctl daemon-reload
sudo systemctl enable ddclient.service
sudo systemctl start ddclient.service
```