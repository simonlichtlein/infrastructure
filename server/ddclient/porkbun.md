o 'porkbun'

The 'porkbun' protocol is used for porkbun (https://porkbun.com/).
The API is documented here: https://porkbun.com/api/json/v3/documentation

Before setting up, it is necessary to create your API Key by referring to the following page.

https://kb.porkbun.com/article/190-getting-started-with-the-porkbun-api

Available configuration variables:
  * apikey (required): API Key of Porkbun API
  * secretapikey (required): Secret API Key of Porkbun API
  * root-domain: The root domain of the specified domain name.
  * on-root-domain=yes or no (default: no): Indicates whether the specified domain name (FQDN) is
    an unnamed record (Zone APEX) in a zone.
    It is useful to specify it as a local variable as shown in the example.
    This configuration value is deprecated, use root-domain instead!
  * server: API endpoint to use, defaults to api.porkbun.com
  * usev4, usev6 : These configuration variables can be specified as local variables to override
    the global settings. It is useful to finely control IPv4 or IPv6 as shown in the example.
  * use (deprecated) : This parameter is deprecated but can be overridden like the above parameters.

Limitations:
  * Multiple same name records (for round robin) are not supported.
    The same IP address is set for all, creating meaningless extra records.
  * If neither root-domain nor on-root-domain are specified, ddclient-4.0.0 will split the given
    hostname into subdomain and domain on the first dot.
    For example:
      * sub.example.com -> Subdomain "sub", root domain "example.com"
      * sub.foo.example.com -> Subdomain "sub", root domain "foo.example.com"
    If both root-domain and on-root-domain are specified, root-domain takes precedence.

Example ddclient-4.0.0.conf file entry:
  protocol=porkbun
  apikey=APIKey
  secretapikey=SecretAPIKey
  root-domain=example.com
  example.com,host.example.com,host2.sub.example.com

Additional example to finely control IPv4 or IPv6 :
  # Example 01 : Global enable both IPv4 and IPv6, and update both records.
  usev4=webv4
  usev6=ifv6, ifv6=enp1s0

  protocol=porkbun
  apikey=APIKey
  secretapikey=SecretAPIKey
  root-domain=example.com
  host.example.com,host2.sub.example.com

  # Example 02 : Global enable only IPv4, and update only IPv6 record.
  usev4=webv4

  protocol=porkbun
  apikey=APIKey
  secretapikey=SecretAPIKey
  root-domain=example.com
  usev6=ifv6, ifv6=enp1s0, usev4=disabled ipv6.example.com

  # Example 03: Update just a root domain
  protocol=porkbun
  apikey=APIKey
  secretapikey=SecretAPIKey
  root-domain=host.example.com
  host.example.com