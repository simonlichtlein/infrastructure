if [ -f /etc/debian_version ]; then
    # Add the repository to Apt sources:
    echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/debian $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | sudo tee /etc/apt/sources.list.d/docker.list >/dev/null

    sudo apt update
    sudo apt install -y ca-certificates curl
    sudo install -m 0755 -d /etc/apt/keyrings
    sudo curl -fsSL https://download.docker.com/linux/debian/gpg -o /etc/apt/keyrings/docker.asc
    sudo chmod a+r /etc/apt/keyrings/docker.asc
    sudo apt install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
    sudo usermod -aG docker $USER
    newgrp docker
    sudo systemctl enable docker
    sudo systemctl start docker

elif [ -f /etc/redhat-release ]; then

    sudo rpm --import https://download.docker.com/linux/fedora/gpg

    # dnf3
    # sudo dnf config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo
    #dnf5
    sudo dnf config-manager addrepo --from-repofile=https://download.docker.com/linux/fedora/docker-ce.repo

    sudo dnf install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

    # change to btrfs storage driver
    echo '{"storage-driver": "btrfs"}' | sudo tee /etc/docker/daemon.json >/dev/null

    # enable bridge iptables
    echo 'net.bridge.bridge-nf-call-iptables=1' | sudo tee /etc/sysctl.d/99-docker-bridge.conf >/dev/null
    echo 'net.bridge.bridge-nf-call-ip6tables=1' | sudo tee /etc/sysctl.d/99-docker-bridge.conf >/dev/null
    echo br_netfilter | sudo tee /etc/modules-load.d/br_netfilter.conf
    sudo sysctl --system

    sudo usermod -aG docker $USER
    newgrp docker
    sudo systemctl start docker
    sudo systemctl enable docker
    sudo reboot # needs to enable br_netfilter else docker info prints an warning
    docker info
fi
# install portainer
# note that the persistent data is stored in /data/container/portainer because i use what on my slv-horde server for default container storage
# docker run -d -p 8000:8000 -p 9443:9443 --name portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v /data/container/portainer:/data portainer/portainer-ce:latest
