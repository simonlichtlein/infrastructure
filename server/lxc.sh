# LXC Fedora setup

# install needed packages and enable sshd
sudo dnf install -y curl openssh-server firewalld
sudo systemctl enable --now sshd
sudo systemctl enable --now firewalld

sudo firewall-cmd --add-service=ssh --permanent
sudo firewall-cmd --reload

# add admin user
adduser admin
usermod -aG wheel admin
passwd admin

reboot

# login as admin or switch to admin user
su admin
# add ssh keys to admin user
mkdir -p "/home/admin/.ssh"
SSHKEYS="https://lichtlein.dev/pub/keys.pub"
curl -s "$SSHKEYS" >"/home/admin/.ssh/authorized_keys"
chmod 700 "/home/admin/.ssh"
chmod 600 "/home/admin/.ssh/authorized_keys"
