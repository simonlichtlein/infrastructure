# this script automatically sets up podman with an extra user for running the pods, creates a directory to store the container data
# **** PRE **** #

curl -s "https://lichtlein.dev/pub/linux/post-install/module.sh" >module.sh
chmod +x module.sh
source module.sh

printinfo
println

printc -t "installing podman"
if command -v apt &>/dev/null; then
    sudo apt install -y podman
elif command -v dnf &>/dev/null; then
    sudo dnf install -y podman
else
    printc -c $red$bold -t "your OS is not supported"
    exit 3
fi
# TODO

rm module.sh

### add docker hub to reposearchlist
echo "[registries.search] \
registries = ['docker.io', 'quay.io', 'registry.fedoraproject.org']" | sudo tee /etc/containers/registries.conf

### add ipv4 forward capability```
echo "net.ipv4.ip_forward=1" | sudo tee /etc/sysctl.d/99-ipforward.conf
sudo sysctl --system

systemctl --user enable podman.socket
systemctl --user start podman.socket

echo $XDG_RUNTIME_DIR/podman/podman.sock

- $XDG_RUNTIME_DIR/podman/podman.sock:/var/run/podman.sock:ro
