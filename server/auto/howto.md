
Mount the Fedora ISO and copy the kickstart file to the ISO.
```shell
mkdir /mnt/iso
mount -o loop /home/sl/VMs/iso/Fedora-Everything-netinst-x86_64-41-1.4.iso /mnt/iso
mkdir /mnt/iso/ks
cp /home/sl/wks/infrastructure/server/auto/ks.cfg /mnt/iso/ks
```

Modify the ISO to use the kickstart file.
```shell
cd /mnt/iso/EFI/BOOT


```

Recreate the ISO
```shell
cd /mnt/iso

mkisofs -o /home/sl/VMs/iso/custom-fedora-netinstall.iso \
-b isolinux/isolinux.bin \
-c isolinux/boot.cat \
-no-emul-boot \
-boot-load-size 4 \
-boot-info-table \
-R -J -v -T .
```