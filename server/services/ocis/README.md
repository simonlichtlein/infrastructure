Please note what i dont use the secrets defined in the yaml files or environment variables - so you dont need to try to hack me <3

# OCIS with Collabora on existing reverse proxy
My goal was to use ocis as replacement for Microsoft OneDrive. I tried Nextcloud but it was "too much" for my needs. I wanted to have an easy to use online file storage, with an sharing service, online document editor and a desktop client. 

I stumble across owncloud and later their in GOlang rewritten owncloud infinite scale (ocis). They also support OnlyOffice and Collabora Online. I decided to use Collabora Online because ¯\_(ツ)_/¯

## Prerequisites
These are really simple:
- A server (or client if your brave enough)
- Docker and docker compose installed
- A reverse proxy (I use caddy)

## setup folders on your server
Im using /opt/ocis as my base folder for ocis. You can use any other folder you like. 
Note that you need to change the paths in the docker-compose.yml to your needs.
```bash
sudo mkdir -p /opt/ocis/config
sudo mkdir -p /opt/ocis/data
# i assume you know that what you need to use the uid and gid of your user ;)
sudo chown -R 1000: /opt/ocis
```

## setup your reverse proxy
I use caddy as reverse proxy. You can use any other reverse proxy you like. 

Please note that you of course need to change the ip addresses and domain names to your needs. Also the log files are optional and in my setup at an location what is not default.

```json
{
	# TLS options
	email admin@lichtlein.dev
	acme_ca https://acme-v02.api.letsencrypt.org/directory
	acme_dns desec {
		token {env.DESEC_DNS_TOKEN}
	}
    [...]
}

wopiserver.lan.lichtlein.dev {
	log {
		output file /caddy/log/ocis-wopi.log
	}
	tls {
		resolvers ns1.desec.io
	}
	reverse_proxy 10.0.0.4:9300
}

ocis.lan.lichtlein.dev {
	log {
		output file /caddy/log/ocis.log
	}
	tls {
		resolvers ns1.desec.io
	}
	reverse_proxy 10.0.0.4:9200
}

collabora.lan.lichtlein.dev {
	log {
		output file /caddy/log/ocis-collabora.log
	}
	tls {
		resolvers ns1.desec.io
	}
	encode gzip
	reverse_proxy 10.0.0.4:9980
}
```

## folder structure
```bash
/opt/ocis/
├── config/
│   ├── app-registry.yaml
│   ├── apps.yaml
│   ├── csp.yaml
│   ├── ocis.yaml
├── data/ <- this is where the ocis data is stored
```

## inital start ocis to generate an ocis.yaml
You first need to start the ocis container with an `init` command to generate an `ocis.yaml` file what includes some secrets and the default admin password. 
```bash
docker run --rm -it -v /opt/ocis/config:/etc/ocis -v /opt/ocis/data:/var/lib/ocis owncloud/ocis init
# you need to type in yes and safe the inital admin password for later
```

## config yaml files
Note that i think you can simply put all yaml files in one and it would work but i like to have them separated. Also you can simple create an new yaml config for new functions or apps and dont need to touch your docker-compose.yml because i mount the whole config folder to the ocis container.

### extend ocis.yaml
The only thing you need to do is to add the collabora wopi server to the ocis.yaml.
This connects the ocis with the collabora server.
```yaml
extensions:
  wopi:
    enabled: true
    apps:
      collabora:
        wopi_url: https://collabora.lan.lichtlein.dev
        enabled: true
[...] # keep the rest of the auto generated ocis.yaml
```

### apps.yaml
In this file you enable the collabora app.
```yaml
importer:
  config:
    companionUrl: https://collabora.lan.lichtlein.dev
```

### app-registry.yaml
This is for registering the mime types and the default app for them.
```yaml
app_registry:
  mimetypes:
  - mime_type: application/pdf
    extension: pdf
    name: PDF
    description: PDF document
    icon: ''
    default_app: ''
    allow_creation: false
  - mime_type: application/vnd.oasis.opendocument.text
    extension: odt
    name: OpenDocument
    description: OpenDocument text document
    icon: ''
    default_app: Collabora
    allow_creation: true
  - mime_type: application/vnd.oasis.opendocument.spreadsheet
    extension: ods
    name: OpenSpreadsheet
    description: OpenDocument spreadsheet document
    icon: ''
    default_app: Collabora
    allow_creation: true
  - mime_type: application/vnd.oasis.opendocument.presentation
    extension: odp
    name: OpenPresentation
    description: OpenDocument presentation document
    icon: ''
    default_app: Collabora
    allow_creation: true
  - mime_type: application/vnd.jupyter
    extension: ipynb
    name: Jupyter Notebook
    description: Jupyter Notebook
    icon: ''
    default_app: ''
    allow_creation: true
```

### csp.yaml
This is for the content security policy. You need this so the ocis can embed the collabora server. (CORS policy)
```yaml
directives:
  child-src:
    - '''self'''
  connect-src:
    - '''self'''
    - 'blob:'
    - 'https://collabora.lan.lichtlein.dev/'
    - 'wss://collabora.lan.lichtlein.dev/'
    - 'https://raw.githubusercontent.com/owncloud/awesome-ocis/'
  default-src:
    - '''none'''
  font-src:
    - '''self'''
  frame-ancestors:
    - '''self'''
  frame-src:
    - '''self'''
    - 'blob:'
    - 'https://embed.diagrams.net/'
    # In contrary to bash and docker the default is given after the | character
    - 'https://collabora.lan.lichtlein.dev/'
    # This is needed for the external-sites web extension when embedding sites
    - 'https://owncloud.dev'
  img-src:
    - '''self'''
    - 'data:'
    - 'blob:'
    - 'https://raw.githubusercontent.com/owncloud/awesome-ocis/'
    # In contrary to bash and docker the default is given after the | character
    - 'https://collabora.lan.lichtlein.dev/'
  manifest-src:
    - '''self'''
  media-src:
    - '''self'''
  object-src:
    - '''self'''
    - 'blob:'
  script-src:
    - '''self'''
    - '''unsafe-inline'''
  style-src:
    - '''self'''
    - '''unsafe-inline'''
```

## setup docker-compose.yml
And now the bring the whole magic together via docker-compose.yml.
```yaml
services:
  ocis:
    container_name: owncloud-ocis
    image: owncloud/ocis:latest
    ports:
      - 9200:9200
    expose:
      - 9233
      - 9191
      - 9142
    environment:
      OCIS_LOG_LEVEL: info
      OCIS_URL: https://ocis.lan.lichtlein.dev
      OCIS_INSECURE: true
      PROXY_TLS: false
      FRONTEND_APP_HANDLER_SECURE_VIEW_APP_ADDR: com.owncloud.api.collaboration.Collabora
      PROXY_CSP_CONFIG_FILE_LOCATION: /etc/ocis/csp.yaml
      NATS_NATS_PORT: 9233
      NATS_NATS_HOST: 0.0.0.0
      SETTINGS_GRPC_ADDR: 0.0.0.0:9191 # make settings service available to oCIS Hello
      GATEWAY_GRPC_ADDR: 0.0.0.0:9142 # make the REVA gateway accessible to the app drivers
    volumes:
      - /opt/ocis/config:/etc/ocis
      - /opt/ocis/data:/var/lib/ocis
    restart: unless-stopped

  collaboration:
    container_name: owncloud-collaboration
    image: owncloud/ocis:latest
    restart: unless-stopped
    ports:
      - 9300:9300 #woopi server port
    depends_on:
      ocis:
        condition: service_started
      collabora:
        condition: service_healthy
    entrypoint:
      - /bin/sh
    command: [ "-c", "ocis collaboration server" ]
    environment:
      COLLABORATION_GRPC_ADDR: 0.0.0.0:9301
      COLLABORATION_HTTP_ADDR: 0.0.0.0:9300
      MICRO_REGISTRY: "nats-js-kv"
      MICRO_REGISTRY_ADDRESS: "ocis:9233"
      COLLABORATION_WOPI_SRC: https://wopiserver.lan.lichtlein.dev
      COLLABORATION_APP_NAME: "Collabora"
      COLLABORATION_APP_ADDR: https://collabora.lan.lichtlein.dev
      COLLABORATION_APP_ICON: https://collabora.lan.lichtlein.dev/favicon.ico
      COLLABORATION_APP_INSECURE: true
      COLLABORATION_CS3API_DATAGATEWAY_INSECURE: true
      COLLABORATION_LOG_LEVEL: info
    volumes:
      - /opt/ocis/config:/etc/ocis

  collabora:
    container_name: owncloud-collabora
    image: collabora/code:latest
    restart: unless-stopped
    ports:
      - 9980:9980
    environment:
      aliasgroup1: https://wopiserver.lan.lichtlein.dev:443
      dictionaries: de_DE en_GB en_US
      DONT_GEN_SSL_CERT: true
      extra_params: --o:ssl.enable=false --o:ssl.termination=true --o:welcome.enable=false --o:net.frame_ancestors=ocis.lan.lichtlein.dev
      username: admin
      password: admin
    command: [ "bash", "-c", "coolconfig generate-proof-key ; /start-collabora-online.sh" ]
    healthcheck:
      test: [ "CMD", "curl", "-f", "https://collabora.lan.lichtlein.dev/hosting/discovery" ]
      interval: 2s
      timeout: 10s
      retries: 10
```

## start the services
```bash
docker-compose up -d
```