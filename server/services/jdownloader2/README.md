# jdownloader2 headless but with VNC setup on Alpine Linux

## mount cifs share via service
`/usr/local/bin/mount-cifs.sh`
```sh
#!/bin/sh
mount -t cifs //10.0.0.2/media /opt/mntmedia -o user,uid=1000,gid=1000,file_mode=0777,dir_mode=0777,credentials=/opt/.cifs
```

`/etc/init.d/mount-cifs`
```sh
#!/sbin/openrc-run

description="Mount CIFS Share"

depend() {
    need net
}

start() {
    ebegin "Mounting CIFS share"
    /usr/local/bin/mount-cifs.sh
    eend $?
}
```

```sh
chmod +x /usr/local/bin/mount-cifs.sh
chmod +x /etc/init.d/mount-cifs
rc-update add mount-cifs default
```

## install jdownloader2
```sh
# install java
apk add openjdk21-jre
# install lightweight desktop environment
apk add xfce4 xfce4-terminal dbus-x11 tigervnc openbox
# install vnc server
apk add noVNC websockify

```


## service for jdownloader2
`/usr/local/bin/jd2.sh`
```sh
#!/bin/sh
DISPLAY=:0 /usr/bin/java -jar /opt/jd2/JDownloader.jar
```

`/etc/init.d/jd2`
```sh
#!/sbin/openrc-run

description="JDownlaoder service"

depend() {
    need mount-cifs
}

start() {
    ebegin "Starting JDownloder2"
    /usr/local/bin/jd2.sh
    eend $?
}
```