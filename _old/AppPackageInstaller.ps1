﻿<# 

NAME    : Appx-Package Installer
Version : 0.1
Usage   : ./AppPackageInstaller.ps1 -LogFolder "Folder-Path" -AppName "Appx-Name" -MainAppx "Complete path of Main Appx-Package" -AppxLicense "Complete path of Appx-Package License File" -DependencyAppx "Complete path of Dependency Appx-Package, pass multiple Dependency Appx-Package Separated by comma (,)."   
    
    Appx-Package Installer Script accepts parameters,
                   
                   Mandatory Arguments : LogFolder-path, AppName, Main Appx-Package
                   
                   Optional Argument   : Appx-Package license, Dependency Appx-Packages


#>

[CmdletBinding()]
Param(
    
   [Parameter(Position=1)]
    $LogFolder,

   [Parameter(Position=2)]
    $AppName,

   [Parameter(Position=3)]
    $MainAppx,

   [Parameter(Position=4)]
    $AppxLicense,

   [Parameter(Position=5)]
    $DependencyAppx

)


# Exit Codes for Appx-Installer: Global Variable. 

[int]$Global:ExitCodeSuccess = 0    # Success.
[int]$Global:ExitCodeError   = 1    # Error with error message.
[int]$Global:ExitLogFolderError   = 2    # Error Log-Folder Path not found or invalid path passed.


# Logging for Appx-Installer.

$LogFileName = "\Appx_Installer.txt"                                 # Log-File Name                                   
$Global:LogFilePath = -join($LogFolder,$LogFileName)                 # Log-File Path: Global Variable.


if ((-not [string]::IsNullOrEmpty($LogFolder)))
{
    If(!(Test-Path -Path $LogFolder))
    {
        Write-Host "LogFolder location is invalid." `n -BackgroundColor Red
        exit $ExitLogFolderError
    }
    else
    {
        try {
                New-Item -ItemType File -Path $LogFilePath -Force
        }
        catch {
                Write-Host "Error Creating Log-File." `n -BackgroundColor Red
                exit $ExitLogFolderError
        }
    }
}
else
{
    Write-Host "Mandatory Argument: Log-Folder path not passed." `n -BackgroundColor Red
    exit $ExitLogFolderError
}


# Function to get time-stamp for Logging.

function Get-TimeStamp {
    
    return "[{0:MM/dd/yy} {0:HH:mm:ss}]" -f (Get-Date)
    
}


# Starting Appx-Logging.

Add-Content -Path $LogFilePath -Value "   ***   Appx-Package Installation Log   ***   "


# Function to Validate Parameters Passed.

function ValidParameter($Parameter,$ParameterName) {
    
    if (([string]::IsNullOrEmpty($Parameter)))
    {
        Write-Host "Manadatory Argument: $ParameterName not passed. " -BackgroundColor Red `n
        Add-Content -Path $LogFilePath -Value "$(Get-TimeStamp)   Manadatory Argument: $ParameterName not passed."
        exit $ExitCodeError
    }

}

# Function to Test File for it's Existence. 

function ValidFile($TestFile,$FileName) {
    
    If(!(Test-Path -Path $TestFile))
    {
        Write-Host "$FileName NOT FOUND."  -BackgroundColor Red `n
        Add-Content -Path $LogFilePath -Value "$(Get-TimeStamp)   $FileName NOT FOUND."
        exit $ExitCodeError
    }
}


# Function to Install Appx-Package (Provisionig Appx-Package: By default available for all the users.)

    # 1. Installation of Appx-Package with License-File and Dependency Appx-Packages.

function InstallAppx($Appx,$License,$AppxDependency) {
    
        Add-AppxProvisionedPackage -Online -PackagePath $Appx -DependencyPackagePath $AppxDependency -LicensePath $License

        Add-Content -Path $LogFilePath -Value "$(Get-TimeStamp)   PowerShell Command to Install/Provision Appx-Package: Add-AppxProvisionedPackage -Online -PackagePath $Appx -DependencyPackagePath $AppxDependency -LicensePath $License"

        if ( !$? )
        {

            Write-Host "Error Installing Appx-Package:" + $Error[0].Exception.Message -BackgroundColor Red `n
            Add-Content -Path $LogFilePath -Value "$(Get-TimeStamp)   Error Installing Appx-Package: $Error[0].Exception.Message "
            exit $ExitCodeError
    
        }
        else
        {
    
            Write-Host "Appx-Package Installed Successfully." -BackgroundColor Green `n
            Add-Content -Path $LogFilePath -Value "$(Get-TimeStamp)   Appx-Package Installed Successfully."
            exit $ExitCodeSuccess
        }


}
    

    # 2. Installation of Appx-Package with only License-File.

function InstallAppxWithLicense($Appx,$License) {
    
        Add-AppxProvisionedPackage -Online -PackagePath $Appx -LicensePath $License

        Add-Content -Path $LogFilePath -Value "$(Get-TimeStamp)   PowerShell Command to Install/Provision Appx-Package: Add-AppxProvisionedPackage -Online -PackagePath $Appx -LicensePath $License"

        if ( !$? )
        {

            Write-Host "Error Installing Appx-Package:" + $Error[0].Exception.Message -BackgroundColor Red `n
            Add-Content -Path $LogFilePath -Value "$(Get-TimeStamp)   Error Installing Appx-Package: $Error[0].Exception.Message "
            exit $ExitCodeError
    
        }
        else
        {
    
            Write-Host "Appx-Package Installed Successfully." -BackgroundColor Green `n
            Add-Content -Path $LogFilePath -Value "$(Get-TimeStamp)   Appx-Package Installed Successfully."
            exit $ExitCodeSuccess
        }


}


    # 3. Installation of Appx-Package with only Dependency Appx-Packages.

function InstallAppxWithDependencyAppx($Appx,$AppxDependency) {
    
        Add-AppxProvisionedPackage -Online -PackagePath $Appx -DependencyPackagePath $AppxDependency -SkipLicense

        Add-Content -Path $LogFilePath -Value "$(Get-TimeStamp)   PowerShell Command to Install/Provision Appx-Package: Add-AppxProvisionedPackage -Online -PackagePath $Appx -DependencyPackagePath $AppxDependency -SkipLicense"

        if ( !$? )
        {

            Write-Host "Error Installing Appx-Package:" + $Error[0].Exception.Message -BackgroundColor Red `n
            Add-Content -Path $LogFilePath -Value "$(Get-TimeStamp)   Error Installing Appx-Package: $Error[0].Exception.Message "
            exit $ExitCodeError
    
        }
        else
        {
    
            Write-Host "Appx-Package Installed Successfully." -BackgroundColor Green `n
            Add-Content -Path $LogFilePath -Value "$(Get-TimeStamp)   Appx-Package Installed Successfully."
            exit $ExitCodeSuccess
        }


}


    # 4. Installation of Appx-Package only.

function InstallAppxDefault($Appx) {
    
        Add-AppxProvisionedPackage -Online -PackagePath $Appx -SkipLicense

        Add-Content -Path $LogFilePath -Value "$(Get-TimeStamp)   PowerShell Command to Install/Provision Appx-Package: Add-AppxProvisionedPackage -Online -PackagePath $Appx -SkipLicense"

        if ( !$? )
        {

            Write-Host "Error Installing Appx-Package:" + $Error[0].Exception.Message -BackgroundColor Red `n
            Add-Content -Path $LogFilePath -Value "$(Get-TimeStamp)   Error Installing Appx-Package: $Error[0].Exception.Message "
            exit $ExitCodeError
    
        }
        else
        {
    
            Write-Host "Appx-Package Installed Successfully." -BackgroundColor Green `n
            Add-Content -Path $LogFilePath -Value "$(Get-TimeStamp)   Appx-Package Installed Successfully."
            exit $ExitCodeSuccess
        }


}


<# 
    Function to check if the Appx-Package already installed on the system, 
                if installed the Appx-Package is De-Provisioned before Installing the New Package.
#>


function CheckAppx_Unistall($AppxPackageName) {
    
    $Appx_provisioned_Package = (Get-AppxProvisionedPackage -Online | where DisplayName –eq $AppxPackageName).PackageName

    Add-Content -Path $LogFilePath -Value "$(Get-TimeStamp)   PowerShell Command to Fetch the Appx-Provisioned Package Name: (Get-AppxProvisionedPackage -Online | where DisplayName –eq $AppxPackageName).PackageName"

    if ((-not [string]::IsNullOrEmpty($Appx_provisioned_Package)))
    {
        
        Remove-AppxProvisionedPackage -Online -PackageName $Appx_provisioned_Package

        Add-Content -Path $LogFilePath -Value "$(Get-TimeStamp)   PowerShell Command to De-Provision Appx-Package: Remove-AppxProvisionedPackage -Online -PackageName $Appx_provisioned_Package"

        if ( !$? )
        {

            Write-Host "Error De-Provisioning existing Appx-Package:" + $Error[0].Exception.Message -BackgroundColor Red `n
            Add-Content -Path $LogFilePath -Value "$(Get-TimeStamp)   Error De-Provisioning existing Appx-Package: $Error[0].Exception.Message"
            exit $ExitCodeError
    
        }
        else
        {
    
            Write-Host "Existing Appx-Package De-Provisioned Successfully." -BackgroundColor Green `n
            Add-Content -Path $LogFilePath -Value "$(Get-TimeStamp)   Existing Appx-Package De-Provisioned Successfully." 
            
        }

    }

    else
    {

        Write-Host "Appx-Package not Installed/Provisioned, Proceeding with Appx-Package Installation/Provisioning." `n
        Add-Content -Path $LogFilePath -Value "$(Get-TimeStamp)   Appx-Package not Installed/Provisioned, Proceeding with Appx-Package Installation/Provisioning." 
    }

    
}

# Function to split Dependency Appx-Packages path and check if each path is valid or not.

function SplitDependencyAppx($DSplit) {
    
    $DependencyPackageArray = $DSplit.Split(',') 
    
    foreach ($D_Array_Ele in $DependencyPackageArray) 
    { 
     
      ValidFile $D_Array_Ele 'Dependency Appx-Package'

    }

}


# Main Script Block.


if( ( -not [string]::IsNullOrEmpty( $DependencyAppx ) ) -and ( -not [string]::IsNullOrEmpty( $AppxLicense ) ) )   # Check if License and Dependency Appx-Packages are passed.
{

   # Validate if all mandatory parameters are passed.
   
   ValidParameter $AppName 'Appx-Package Name'     
   ValidParameter $MainAppx 'Main Appx-Package'
   
   # Validate if the file is present.

   ValidFile $MainAppx 'Main Appx-Package'
   ValidFile $AppxLicense 'Appx License'

   SplitDependencyAppx $DependencyAppx  # Split the Dependency Appx-Packages passed and check if all the file is present.

   CheckAppx_Unistall $AppName          # Check if Appx-Package is already present, if present Remove-AppxProvisionedPackage.

   InstallAppx $MainAppx $AppxLicense $DependencyAppx  # Install or Add-AppxProvisionedPackage.

}
elseif ( ( [string]::IsNullOrEmpty( $DependencyAppx ) ) -and ( -not [string]::IsNullOrEmpty( $AppxLicense ) ) )
{
    
   # Validate if all mandatory parameters are passed.
   
   ValidParameter $AppName 'Appx-Package Name'     
   ValidParameter $MainAppx 'Main Appx-Package'
   
   # Validate if the file is present.

   ValidFile $MainAppx 'Main Appx-Package'
   ValidFile $AppxLicense 'Appx License'

   CheckAppx_Unistall $AppName                     # Check if Appx-Package is already present, if present Remove-AppxProvisionedPackage.
   
   InstallAppxWithLicense $MainAppx $AppxLicense   # Install or Add-AppxProvisionedPackage. 

}
elseif ( ( -not [string]::IsNullOrEmpty( $DependencyAppx ) ) -and ( [string]::IsNullOrEmpty( $AppxLicense ) ) )
{

   # Validate if all mandatory parameters are passed.
   
   ValidParameter $AppName 'Appx-Package Name'     
   ValidParameter $MainAppx 'Main Appx-Package'
   
   # Validate if the file is present.

   ValidFile $MainAppx 'Main Appx-Package'
   
   SplitDependencyAppx $DependencyAppx  # Split the Dependency Appx-Packages passed and check if all the file is present.

   CheckAppx_Unistall $AppName          # Check if Appx-Package is already present, if present Remove-AppxProvisionedPackage.

   InstallAppxWithDependencyAppx $MainAppx $DependencyAppx  # Install or Add-AppxProvisionedPackage.    

}
else
{

   # Validate if all the parameters are passed.
   
   ValidParameter $AppName 'Appx-Package Name'
   ValidParameter $MainAppx 'Main Appx-Package'

   # Validate if the file is present.

   ValidFile $MainAppx 'Main Appx-Package'
   
   CheckAppx_Unistall $AppName          # Check if Appx-Package is already present, if present Remove-AppxProvisionedPackage.

   InstallAppxDefault $MainAppx         # Install or Add-AppxProvisionedPackage.       

}











