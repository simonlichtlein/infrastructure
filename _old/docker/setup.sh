#!/bin/sh

sudo apt install podman podman-compose

sudo systemctl enable --now podman.socket
sudo mkdir -P /data/portainer
cd /data || exit

# portainer
sudo podman pull docker.io/portainer/portainer-ee
sudo podman run \
  --name portainer \
  --privileged \
  --detach \
  --tz=local \
  --network=bridge \
  -p 9443:9443 \
  -v /run/podman/podman.sock:/var/run/docker.sock:Z \
  -v /data/portainer:/data:Z \
  docker.io/portainer/portainer-ee
#  docker.io/portainer/portainer-ce

sudo podman generate systemd -n portainer --restart-policy always -f 
sudo podman stop portainer

sudo chmod +x container-portainer.service

sudo ln -s /data/container-portainer.service /etc/systemd/system/container-portainer.service
sudo systemctl daemon-reload
sudo systemctl enable container-portainer.service
sudo systemctl start container-portainer.service
sudo systemctl status container-portainer.service

