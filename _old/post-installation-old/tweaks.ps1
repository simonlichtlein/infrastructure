function CreateRegKeysRecursive($path) {
    Write-Verbose "checking $path"

    # split path at each \
    $split = $path -split '\\'

    $pathToCreate = ""
    for ($i = 0; $i -lt $split.Count; $i++) {

        $pathToCreate += $split[$i] + "\"

        Write-Verbose "checking split path $pathToCreate"

        if (!(Test-Path $pathToCreate)) {
            Write-Verbose "path does not exist - creating"
            New-Item -path $pathToCreate -ItemType Directory | Out-Null
        }
        else {
            Write-Verbose "path does exist - skipping"
        }
    }
}

function Merge-RegKey($path, $name, $value, $type) {
    CreateRegKeysRecursive $path
    New-ItemProperty -path $path -name $name -value $value -PropertyType $type -Force | Out-Null
}

# desktop disable edge auto desktop shortcut | LTSC unused
Write-Host "   disable auto creation of edge desktop icon"
Merge-RegKey -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer" -name "DisableEdgeDesktopShortcutCreation" -value 1 -type DWORD 
Merge-RegKey -path "HKLM:\SOFTWARE\Policies\Microsoft\EdgeUpdate" -name "CreateDesktopShortcutDefault" -value 0 -type DWORD 

# desktop add "This PC" to Desktop | instant
Write-Host "   add `"This PC`" to Desktop"
Merge-RegKey -path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\NewStartPanel" -name "{20D04FE0-3AEA-1069-A2D8-08002B30309D}" -value 0 -type DWORD

# desktop show windows version | instant
Write-Host "   show windows version on desktop"
Merge-RegKey -path "HKCU:\Control Panel\Desktop" -name "PaintDesktopVersion" -value 1 -type DWORD

# explorer compact mode | instant
Write-Host "   enable explorer compact mode"
Merge-RegKey -path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -name "UseCompactMode" -value 1 -type DWORD

# explorer show extensions | instant
Write-Host "   show extension in explorer"
Merge-RegKey -path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -name "HideFileExt" -value 0 -type DWORD

# explorer show hidden files | instant
Write-Host "   show hidden files"
Merge-RegKey -path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -name "Hidden" -value 1 -type DWORD 

# explorer open to thisPC | instant
Write-Host "   explorer open to `"This PC`" by default"
Merge-RegKey -path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -name "LaunchTo" -value 1 -type DWORD

# explorer enable clipboard history | instant
Write-Host "   enable clipboard history"
Merge-RegKey -path "HKCU:\SOFTWARE\Microsoft\Clipboard" -name "EnableClipboardHistory" -value 1 -type DWORD 

# explorer disable onlinetips | LTSC unused gererally not with Enterprise Edition
Write-Host "   disable online tips"
Merge-RegKey -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer" -name "AllowOnlineTips" -value 0 -type DWORD 

# explorer delete confirmation | instant (after explorer restart)
Write-Host "   enable delete confirmation"
Merge-RegKey -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer" -name "ConfirmFileDelete" -value 1 -type DWORD 

# explorer add folders to this pc file explorer | instant
Write-Host "   add folders to this pc in file explorer"
# Desktop
Remove-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\MyComputer\NameSpace\{B4BFCC3A-DB2C-424C-B029-7FE99A87C641}" -Name "HideIfEnabled" -Force
# Documents
Remove-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\MyComputer\NameSpace\{d3162b92-9365-467a-956b-92703aca08af}" -Name "HideIfEnabled" -Force
# Downloads
Remove-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\MyComputer\NameSpace\{088e3905-0323-4b02-9826-5d99428e115f}" -Name "HideIfEnabled" -Force
# Pictures
Remove-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\MyComputer\NameSpace\{24ad3ad4-a569-4530-98e1-ab02f9417aa8}" -Name "HideIfEnabled" -Force
# Music
# Remove-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\MyComputer\NameSpace\{3dfdf296-dbec-4fb4-81d1-6a3438bcf4de}" -Name "HideIfEnabled" -Force
# Videos
# Remove-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\MyComputer\NameSpace\{f86fa3ab-70d2-4fc7-9c99-fcbf05467f3a}" -Name "HideIfEnabled" -Force

# explorer faster (source: https://github.com/ChrisTitusTech/winutil/issues/1679) | instant
Write-Host "   set foldertype for notspecified for faster explorer perfomance"
Merge-RegKey -path "HKCU:\Software\Classes\Local Settings\Software\Microsoft\Windows\Shell\Bags\AllFolders\Shell" -Name "FolderType" -value "NotSpecified" -type String

#! NOT WORKING W11 TODO
# taskbar always show all tray icons
# Write-Host "   always show all tray icons"
# Merge-RegKey -path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer" -name EnableAutoTray -value 0 -type DWORD

# tasbar unpin edge | LTSC unused
$appname = "Microsoft Edge"
((New-Object -Com Shell.Application).NameSpace('shell:::{4234d49b-0245-4df3-b780-3893943456e1}').Items() | ? { $_.Name -eq $appname }).Verbs() | ? { $_.Name.replace('&', '') -match 'Unpin from taskbar' } | % { $_.DoIt(); $exec = $true }
# tasbar unpin store | LTSC unused
$appname = "Microsoft Store"
((New-Object -Com Shell.Application).NameSpace('shell:::{4234d49b-0245-4df3-b780-3893943456e1}').Items() | ? { $_.Name -eq $appname }).Verbs() | ? { $_.Name.replace('&', '') -match 'Unpin from taskbar' } | % { $_.DoIt(); $exec = $true }

# taskbar left allign | instant
Write-Host "   allign taskbar on left side"
Merge-RegKey -path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -name "TaskbarAl" -value 0 -type DWORD 

# desktop disable widgets (Win+W) | LTSC unused
Write-Host "   disable widgets"
Merge-RegKey -path "HKLM:\SOFTWARE\Policies\Microsoft\Dsh" -Name "AllowNewsAndInterests" -value 0 -type DWORD

# taskbar hide widget icon | LTSC unused
Write-Host "   hide `"Widget`" Taskicon"
Merge-RegKey -path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -name "TaskbarDa" -value 0 -type DWORD 

# taskbar hide search icon | instant
Write-Host "   hide `"Search`" Taskicon"
Merge-RegKey -path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Search" -name "SearchboxTaskbarMode" -value 0 -type DWORD

# deprecated with 24H2
# Merge-RegKey -path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -name "ShowCortanaButton" -value 0 -type DWORD

# taskbar hide chat icon | LTSC unused, Chat is now Teams in non-LTSC Versions
# Write-Host "   hide `"Chat`" Taskicon"
# Merge-RegKey -path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" Name "TaskbarMn" -value 0 -type  DWORD

# taskbar hide default volume icon (for eartrumpet) | instant
Write-Host "   hide sytem volume icon for eartrumpet"
Merge-RegKey -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer" -name "HideSCAVolume" -value 1 -type DWORD 

# taskbar hide weather and news | LTSC unused
Write-Host "   hide news and weather taskbar icon"
Merge-RegKey -path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Feeds" -name "ShellFeedsTaskbarViewMode" -value 2 -type DWORD

# OneDrive AD in explorer | LTSC unused gererally not with Enterprise Edition
# Write-Host "   disable onedrive ad in explorer"
# Merge-RegKey -path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -name "ShowSyncProviderNotifications" -value 0 -type DWORD 

# disable feedback | LTSC unused unused gererally not with Enterprise Edition
Write-Host "   disable feedback popup"
Merge-RegKey -path "HKCU:\SOFTWARE\Microsoft\Siuf\Rules" -name "NumberOfSIUFInPeriod" -value 0 -type DWORD
Remove-ItemProperty -Path "HKCU:\SOFTWARE\Microsoft\Siuf\Rules" -Name "PeriodInNanoSeconds" -Force -ErrorAction SilentlyContinue

# disable audio ducking | instant 
Write-Host "   diable audio ducking"
Merge-RegKey -path "HKCU:\SOFTWARE\Microsoft\Multimedia\Audio" -name "UserDuckingPreference" -value 3 -type DWORD 

# disable sticky keys | instant
Write-Host "   disable sticky keys"
Merge-RegKey -path "HKCU:\Control Panel\Accessibility\StickyKeys" -name "Flags" -value "506" -type String 

# disable auto install apps - | instant to test
Write-Host "   disable auto installation of apps"
Merge-RegKey -path "HKCU:\Software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager" -name "SilentInstalledAppsEnabled" -value 0 -type DWORD

# disable driver update | LTSC unused (only 4 not siro-wsus connected clients)
Write-Host "   disable driver auto install"
Merge-RegKey -path "HKLM:\SYSTEM\Policies\Microsoft\Windows" -name "ExcludeWUDriversInQualityUpdate" -value 1 -type DWORD
Merge-RegKey -Path "HKLM:SOFTWARE\Microsoft\Windows\CurrentVersion\DriverSearching\" -Name 'SearchOrderConfig' -Value 0 -type DWORD

# set network type private
Write-Host "   set networktype private on ipv4/ipv6 connected network"
Get-NetConnectionProfile | Where { $_.IPv4Connectivity -eq "Internet" } | Set-NetConnectionProfile -NetworkCategory Private
Get-NetConnectionProfile | Where { $_.IPv6Connectivity -eq "Internet" } | Set-NetConnectionProfile -NetworkCategory Private

# RDP
Write-Host "   enable RDP"
Merge-RegKey -path "HKLM:\SYSTEM\CurrentControlSet\Control\Terminal Server" -name "fDenyTSConnections" -value 0 -type DWORD
Merge-RegKey -path "HKLM:\SYSTEM\CurrentControlSet\Control\Lsa" -name "LimitBlankPasswordUse" -value 0 -type DWORD
Enable-NetFirewallRule -DisplayGroup "Remote Desktop"
# Todo for german systems: Enable-NetFirewallRule -DisplayGroup "Remotedesktop"

# Network File/Folder Sharing
# enable network discovery
Set-NetFirewallRule -DisplayGroup 'Network Discovery' -Profile Private -Enabled true
# turn off password protected sharing 
Merge-RegKey -path "HKLM:\SYSTEM\CurrentControlSet\Control\Lsa" -name "everyoneincludeanonymous" -value 1 -type DWORD
Merge-RegKey -path "HKLM:\SYSTEM\CurrentControlSet\Services\LanmanServer\Parameters" -name restrictnullsessaccess -value 0 -type DWORD
# Turn on file and printer sharing
Set-NetFirewallRule -DisplayGroup "File And Printer Sharing" -Profile Private -Enabled True

# defender warnings
# device security - Memory integrity | LTSC unused, default
Merge-RegKey -path "HKLM:\SYSTEM\CurrentControlSet\Control\DeviceGuard\Scenarios\HypervisorEnforcedCodeIntegrity" -name "Enabled" -value 1 -type DWORD
# account protection - microsoft account | instant (reopen security center)
Merge-RegKey -path "HKCU:\SOFTWARE\Microsoft\Windows Security Health\State" -name "AccountProtection_MicrosoftAccount_Disconnected" -value 0 -type DWORD
# App & Browser control - Reputation-based protection - Potentially unwanted app blocking | LTSC unused, default
Set-MpPreference -PUAProtection 1

# deactivate Fast Startup | reboot
Merge-RegKey -path "HKLM:\SYSTEM\CurrentControlSet\Control\Session Manager\Power" -name "HiberbootEnabled" -value 0 -type DWORD

# disable auto reconnect network drives (diable could not connect to all network drives)
Merge-RegKey -path "HKLM:\SYSTEM\CurrentControlSet\Control\NetworkProvider" -name "NetworkProvider" -value 0 -type DWORD

# enable unauthorized guest access to file shares 
Merge-RegKey -path "HKLM:\SYSTEM\CurrentControlSet\Services\LanmanWorkstation\Parameters" -name "AllowInsecureGuestAuth" -value 0 -type DWORD

# classic control panel & small icons | instant
Merge-RegKey -path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\ControlPanel" -name "AllItemsIconView" -value 1 -type DWORD
Merge-RegKey -path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\ControlPanel" -name "StartupPage" -value 1 -type DWORD

# hide taskview from taskbar | instant
Merge-RegKey -path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -name "ShowTaskViewButton" -value 0 -type DWORD

# disable UAC | instant
Merge-RegKey -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System" -name "ConsentPromptBehaviorAdmin" -value 0 -type DWORD

# hide learn about this picture from desktop | instant
Merge-RegKey -path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\NewStartPanel" -name "{2cc5ca98-6485-489a-920e-b3e88a6ccce3}" -value 1 -type DWORD

# show all tray icons (once)
Merge-RegKey -Path "HKCU:\Control Panel\NotifyIconSettings\*" -Name 'IsPromoted' -Value 1 -type DWORD