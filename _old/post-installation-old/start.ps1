# self elevating script to admin (if not already)
if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) { Start-Process powershell.exe "-NoProfile -ExecutionPolicy Bypass -File `"$PSCommandPath`"" -Verb RunAs; exit }

# start log
Start-Transcript -Path $PSScriptRoot\post-installation.log

# disable progressbar status because of bad handling of multiple actions
$ProgressPreference = "SilentlyContinue"

${ESC} = [char]27
Clear-Host
Write-Host "|==============================================|"
Write-Host "|    Windows 10/11 Post-Installation Tasks     |"
Write-Host "|" -NoNewLine 
Write-Host "----------------------------------------------" -NoNewLine -F DarkGray
Write-Host "|"
Write-Host "| " -NoNewline
Write-Host "simon@lichtlein.dev" -NoNewline -F Red 
Write-Host "    -    " -NoNewline
Write-Host "gitlab.com/simonlichtlein" -NoNewline -F Red
Write-Host " |"
Write-Host "|==============================================|"
Write-Host

# killing explorer
taskkill /f /im explorer.exe | Out-Null

# adding defender exclusion
Write-Host " adding Defender exclusion for $PSScriptRoot"
Add-MpPreference -ExclusionPath $PSScriptRoot
Write-Host " adding Defender exclusion for $ENV:USERPROFILE\Downloads\"
Add-MpPreference -ExclusionPath "$ENV:USERPROFILE\Downloads\"

# Activation
Write-Host " 1. starting activation script"
& ([ScriptBlock]::Create((irm https://get.activated.win/get))) /HWID
Write-Host

# remover
Write-Host " 2. starting removal of useless programs and bloatware"
Invoke-Expression "& '$PSScriptRoot\remover.ps1'"
Write-Host
Write-Host "   finished removing useless programs and bloatware"
Write-Host

# tweaks
Write-Host " 3. starting tweaks script"
Invoke-Expression "& '$PSScriptRoot\tweaks.ps1'"
Write-Host
Write-Host "   finished tweaking"
Write-Host

# winget programs
Write-Host " 4. starting winget program installation"
Invoke-Expression "& '$PSScriptRoot\installer.ps1'"
Write-Host
Write-Host "   finished installing programs via winget"
Write-Host

# removing defender exclusion
Write-Host " removing Defender exclusion for $PSScriptRoot"
Remove-MpPreference -ExclusionPath $PSScriptRoot

# renaming computer
$input_pcname = Read-Host " pc name? (default: $($ENV:USERNAME)-PC) (<name> / n )"

if ($input_pcname -ne 'n') {
    if (!$input_pcname) {
        $pcname = "$($ENV:USERNAME)-PC"
    }
    else {
        $pcname = $input_pcname
    }
    Rename-Computer -NewName $pcname -Force
}

Write-Host " set reboot alias on powershell"
Set-Alias -Name "reboot" -Value "shutdown /r /f /t 2" -Scope Global

# restarting explorer
Write-Host " restarting explorer"
Start-Process explorer.exe
Write-Host

Start-Sleep 3
# start app because of startup
$et_app = Get-AppxPackage -Name 40459File-New-Project.EarTrumpet
& explorer "shell:appsFolder\$($et_app.PackageFamilyName)!$((Get-AppxPackageManifest -Package $et_app.PackageFullName).Package.Applications.Application.Id)"
Start-Sleep 3
Get-Process -Name "EarTrumpet" | Stop-Process -Force

& "C:\Users\$($ENV:USERNAME)\Desktop\Twinkle Tray.lnk" | Out-Null
Start-Sleep 3
Get-Process -Name "Twinkle Tray" | Stop-Process -Force
Remove-Item "C:\Users\$($ENV:USERNAME)\Desktop\Twinkle Tray.lnk" -Force

# finished 
Write-Host -F DarkGray "|==============================================|"
Write-Host -F DarkGray "|               vires in numeris               |"
Write-Host -F DarkGray "|==============================================|"
Write-Host

# stopping loggging
Stop-Transcript

# reboot request
Write-Host " requesting reboot"
if ((Read-Host " Do you want to restart the pc now? (y/n)") -eq "y") {
    shutdown /f /r /t 2
}
else {
    PAUSE
}