Write-Host "   removing msstore source from winget sources"
winget source remove msstore
Write-Host

Write-Host "   installing Microsoft VCRedist 2015+ x86 / x64"
winget install --id Microsoft.VCRedist.2015+.x86 --silent | Out-Null
winget install --id Microsoft.VCRedist.2015+.x64 --silent | Out-Null

Write-Host "   installing Microsoft .NET Runtimes"
winget install --id Microsoft.DotNet.Runtime.3_1 --silent | Out-Null
winget install --id Microsoft.DotNet.Runtime.5 --silent | Out-Null
winget install --id Microsoft.DotNet.Runtime.6 --silent | Out-Null
winget install --id Microsoft.DotNet.Runtime.7 --silent | Out-Null
winget install --id Microsoft.DotNet.Runtime.8 --silent | Out-Null

Write-Host "   installing Microsoft .NET Desktop Runtimes"
winget install --id Microsoft.DotNet.DesktopRuntime.3_1 --silent | Out-Null
winget install --id Microsoft.DotNet.DesktopRuntime.5 --silent | Out-Null
winget install --id Microsoft.DotNet.DesktopRuntime.6 --silent | Out-Null
winget install --id Microsoft.DotNet.DesktopRuntime.7 --silent | Out-Null
winget install --id Microsoft.DotNet.DesktopRuntime.8 --silent | Out-Null

Write-Host "   installing 7zip"
winget install --id 7zip.7zip --silent | Out-Null

Write-Host "   installing EarTrumpet (better volume control)"
winget install --id File-New-Project.EarTrumpet --silent | Out-Null

Write-Host "   installing twinkletray (monitor brightness control)"
winget install --id xanderfrangos.twinkletray --silent | Out-Null

Write-Host "   installing K-Lite CodecPack with Player"
winget install --id CodecGuide.K-LiteCodecPack.Standard --silent | Out-Null

Write-Host "   installing Notepad++"
winget install --id Notepad++.Notepad++ --silent | Out-Null

Write-Host "   installing Windows Terminal"
winget install --id Microsoft.WindowsTerminal --silent | Out-Null

Write-Host "   installing PowerShell 7"
winget install --id Microsoft.PowerShell --silent | Out-Null

Write-Host "   installing PowerToys"
winget install -e --id Microsoft.PowerToys --silent | Out-Null

Write-Host "   installing only office"
winget install -e --id ONLYOFFICE.DesktopEditors --silent | Out-Null

Write-Host "   installing bitwarden"
winget install -e --id Bitwarden.Bitwarden --silent | Out-Null

Write-Host "   installing citrix workspace app"
winget install -e --id Citrix.Workspace --silent | Out-Null

Write-Host "   installing citrix virtual box"
winget install -e --id Oracle.VirtualBox --silent | Out-Null

Write-Host "   installing citrix rustdesk"
winget install -e --id RustDesk.RustDesk --silent | Out-Null

# install optional windows feature
# nfs features
Enable-WindowsOptionalFeature -FeatureName ServicesForNFS-ClientOnly, ClientForNFS-Infrastructure -Online -NoRestart

# update store apps 
Get-CimInstance -Namespace "Root\cimv2\mdm\dmmap" -ClassName "MDM_EnterpriseModernAppManagement_AppManagement01" | Invoke-CimMethod -MethodName UpdateScanMethod

# HEVC Codec see other repo/file

