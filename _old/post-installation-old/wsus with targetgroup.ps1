$filepath = "$env:TEMP\wsus.ps1"
If (! (Test-Path $filepath) ) {
    $ipv4 = $null
    $ipv4 = (Resolve-DnsName wsus.siro-solutions.de -Server 1.1.1.1 -Type A -NoHostsFile).IP4Address

    if ($ipv4) {
        if ((Test-NetConnection -ComputerName $ipv4 -Port 8530).TcpTestSucceeded) {
            Write-Host -F "Green" "Connection successfull"
        }
        else {
            Write-Host -F "Red" "Connection unsuccessfull!"
            PAUSE
            exit
        }
    }
    else {
        Write-Host -F "Red" "Connection unsuccessfull!"
        PAUSE
        exit
    }

    Invoke-WebRequest -Uri "https://public.siro-solutions.de/wsus.ps1" -OutFile "$env:TEMP\wsus.ps1"
    Start-Process powershell.exe "-NoProfile -ExecutionPolicy Bypass -File `"$env:TEMP\wsus.ps1`"" -Verb RunAs -Wait

    Remove-Item "$env:TEMP\wsus.ps1"

    $wuserver = "http://wsus.siro-solutions.de:8530"
    $wuservermachine = (Get-ItemPropertyValue -Path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate" -Name "WUServer")
    if ($wuserver -eq $wuservermachine ) {
        Write-Host ""
        Write-Host " using the following as WSUS-Server"
        Write-Host -F "Blue" "  $wuservermachine"
        Write-Host ""
        Write-Host -F "Green" "done!"
    }
    else {
        Write-Host ""
        Write-Host -F "Red" "error! please try again - maybe with admin?"
        Write-Host ""
    }
    PAUSE
}
else {
    function CreateRegKeysRecursive($path) {
        Write-Verbose "checking $path"

        # split path at each \
        $split = $path -split '\\'

        $pathToCreate = ""
        for ($i = 0; $i -lt $split.Count; $i++) {

            $pathToCreate += $split[$i] + "\"

            Write-Verbose "checking split path $pathToCreate"

            if (!(Test-Path $pathToCreate)) {
                Write-Verbose "path does not exist - creating"
                New-Item -path $pathToCreate -ItemType Directory | Out-Null
            }
            else {
                Write-Verbose "path does exist - skipping"
            }
        }
    }

    function Merge-RegKey($path, $name, $value, $type) {
        CreateRegKeysRecursive $path
        New-ItemProperty -path $path -name $name -value $value -PropertyType $type -Force | Out-Null
    }

    Merge-RegKey -path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate" -Name "ElevateNonAdmins" -value 1 -type DWORD
    Merge-RegKey -path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate" -Name "WUServer" -value "http://wsus.siro-solutions.de:8530" -type String
    Merge-RegKey -path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate" -Name "WUStatusServer" -value "http://wsus.siro-solutions.de:8530" -type String
    
    Merge-RegKey -path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate" -Name "TargetGroupEnabled" -value 1 -type DWORD
    $os = (Get-WmiObject -class Win32_OperatingSystem).Caption

    if ($os -like "*11*") {
        Merge-RegKey -path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate" -Name "TargetGroup" -value "Client" -type String
    }
    elseif ($os -like "*10*") {
        Merge-RegKey -path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate" -Name "TargetGroup" -value "Windows 10" -type String
    }
    elseif ($os -like "*Server*") {
        Merge-RegKey -path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate" -Name "TargetGroup" -value "Server" -type String
    }
    else {
        Merge-RegKey -path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate" -Name "TargetGroup" -value "other" -type String
    }

    # Enables access to Windows Update (Check online for updates from Microsoft Update)
    Remove-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate" -Name "DisableWindowsUpdateAccess" -ErrorAction SilentlyContinue
    
    Merge-RegKey -path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" -Name "UseWUServer" -value 1 -type DWORD
    Merge-RegKey -path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" -Name "AUOptions" -value 3 -type DWORD
    Merge-RegKey -path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" -Name "AutoInstallMinorUpdates" -value 1 -type DWORD
    Merge-RegKey -path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" -Name "DetectionFrequencyEnabled" -value 1 -type DWORD
    Merge-RegKey -path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" -Name "DetectionFrequency" -value 6 -type DWORD
    Merge-RegKey -path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" -Name "NoAutoRebootWithLoggedOnUsers" -value 1 -type DWORD
    Merge-RegKey -path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" -Name "NoAutoUpdate" -value 0 -type DWORD

    # restart service and start update detection
    Stop-Service -Force wuauserv
    Remove-Item -Force -Recurse "$ENV:WINDIR\softwaredistribution"
    Start-Service -Force wuauserv
    usoclient /resetauthorization /detectnow
}