# script for getting and saving metadata of client

# GUID
$siro_path = "HKCU:\SOFTWARE\siro"

if (! (Get-Item -Path $siro_path -ErrorAction SilentlyContinue) ) {
    New-Item -Path $siro_path | Out-Null
} 

if (! (Get-ItemProperty -Path $siro_path -Name guid -ErrorAction SilentlyContinue) ) {
    $siro_guid_new = "siro_" + (New-Guid).Guid.toString()
    New-ItemProperty -Path $siro_path -Name guid -Value $siro_guid_new | Out-Null
}

$siro_guid = Get-ItemPropertyValue -Path $siro_path -Name guid

# mail
if (! (Get-ItemProperty -Path $siro_path -Name mail -ErrorAction SilentlyContinue) ) {
    $siro_mail_new = Read-Host "Please type in an email where we can contact you "
    New-ItemProperty -Path $siro_path -Name mail -Value $siro_mail_new | Out-Null
}

$siro_mail = Get-ItemPropertyValue -Path $siro_path -Name mail

# gather data
$appwiz = @(@{})
foreach ($UKey in 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\*',
    'HKLM:\SOFTWARE\Wow6432node\Microsoft\Windows\CurrentVersion\Uninstall\*',
    'HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\*', 
    'HKCU:\SOFTWARE\Wow6432node\Microsoft\Windows\CurrentVersion\Uninstall\*') { 
    foreach ($Product in (Get-ItemProperty $UKey -ErrorAction SilentlyContinue)) { 
        if ($Product.DisplayName -and $Product.SystemComponent -ne 1) {
            $appwiz += @{
                "name"    = $Product.DisplayName
                "version" = $Product.DisplayVersion
            }
        }
    } 
}

$data = @{
    "siro_mail"     = $siro_mail
    "siro_guid"     = $siro_guid
    "hostname"      = HOSTNAME.EXE
    # "build"         = [System.Environment]::OSVersion.Version.Build
    "OSinfo"        = Get-ComputerInfo
    "OSinstalldate" = $data.OSinfo.WindowsInstallDateFromRegistry
    "OSname"        = $data.OSinfo.OsName
    "OSversion"     = $data.OSinfo.OsVersion + " patch " + $data.OSinfo.WindowsUBR
    "OSlastboot"    = $data.OSinfo.OsLastBootUpTime
    "BIOSversion"   = $data.OSinfo.BiosSMBIOSBIOSVersion
    "BIOSdate"      = $data.OSinfo.BiosReleaseDate
    "manufacturer"  = $data.OSinfo.CsManufacturer
    "model"         = $data.OSinfo.CsModel
    "processor"     = $data.OSinfo.CsProcessors
    "memory"        = $data.OSinfo.CsPhysicallyInstalledMemory
    "username"      = $env:UserName
    "lastreport"    = Get-Date -F "yyyy.MM.dd HH:mm"
    "appwiz"        = $appwiz
    "HWReadiness"   = (irm https://aka.ms/HWReadinessScript | iex)
}

# TODO 
# send data 
<#
        where to safe data? directly on wsus or on different db?
        what to do with metadata?
            use the HWreadinessscript for dynamicly add the needed regsitry files for "inplace" upgrade?
        inform user about metadata
        possibility to get all the data from db via user 
    #>