# the only ones which are present in LTSC, too
Write-Host "   removing Microsoft.Windows.DevHome"
Get-AppxPackage Microsoft.Windows.DevHome | Remove-AppxPackage
Write-Host "   removing Microsoft.OutlookForWindows"
Get-AppxPackage Microsoft.OutlookForWindows | Remove-AppxPackage

# LTSC unused
Write-Host "   removing Microsoft.BingWeather"
Get-AppxPackage Microsoft.BingWeather | Remove-AppxPackage
Write-Host "   removing Microsoft.GetHelp"
Get-AppxPackage Microsoft.GetHelp | Remove-AppxPackage
Write-Host "   removing Microsoft.Getstarted"
Get-AppxPackage Microsoft.Getstarted | Remove-AppxPackage
Write-Host "   removing Microsoft.MicrosoftOfficeHub"
Get-AppxPackage Microsoft.MicrosoftOfficeHub | Remove-AppxPackage
Write-Host "   removing Microsoft.MicrosoftSolitaireCollection"
Get-AppxPackage Microsoft.MicrosoftSolitaireCollection | Remove-AppxPackage
Write-Host "   removing Microsoft.MixedReality.Portal"
Get-AppxPackage Microsoft.MixedReality.Portal | Remove-AppxPackage
Write-Host "   removing Microsoft.MSPaint"
Get-AppxPackage Microsoft.MSPaint | Remove-AppxPackage
Write-Host "   removing Microsoft.OneNote"
Get-AppxPackage Microsoft.Office.OneNote | Remove-AppxPackage
Write-Host "   removing Microsoft.People"
Get-AppxPackage Microsoft.People | Remove-AppxPackage
Write-Host "   removing Microsoft.SkypeApp"
Get-AppxPackage Microsoft.SkypeApp | Remove-AppxPackage
Write-Host "   removing Microsoft.Wallet"
Get-AppxPackage Microsoft.Wallet | Remove-AppxPackage
Write-Host "   removing Microsoft.windowscommunicationsapps"
Get-AppxPackage microsoft.windowscommunicationsapps | Remove-AppxPackage
Write-Host "   removing Microsoft.WindowsFeedbackHub"
Get-AppxPackage Microsoft.WindowsFeedbackHub | Remove-AppxPackage
Write-Host "   removing Microsoft.WindowsMaps"
Get-AppxPackage Microsoft.WindowsMaps | Remove-AppxPackage
Write-Host "   removing Microsoft.WindowsSoundRecorder"
Get-AppxPackage Microsoft.WindowsSoundRecorder | Remove-AppxPackage
Write-Host "   removing Microsoft.YourPhone"
Get-AppxPackage Microsoft.YourPhone | Remove-AppxPackage
Write-Host "   removing Microsoft.ZuneMusic"
Get-AppxPackage Microsoft.ZuneMusic | Remove-AppxPackage
Write-Host "   removing Microsoft.ZuneVideo"
Get-AppxPackage Microsoft.ZuneVideo | Remove-AppxPackage
Write-Host "   removing Microsoft.MicrosoftStickyNotes"
Get-AppxPackage Microsoft.MicrosoftStickyNotes | Remove-AppxPackage
Write-Host "   removing Microsoft.WindowsAlarms"
Get-AppxPackage Microsoft.WindowsAlarms | Remove-AppxPackage
Write-Host "   removing MicrosoftTeams"
Get-AppxPackage -Name MicrosoftTeams | Remove-AppxPackage
Write-Host "   removing Clipchamp.Clipchamp"
Get-AppxPackage Clipchamp.Clipchamp | Remove-AppxPackage

# xbox features | LTSC unusd
Write-Host "   removing Microsoft.Xbox.TCUI"
Get-AppxPackage Microsoft.Xbox.TCUI | Remove-AppxPackage
Write-Host "   removing Microsoft.XboxApp"
Get-AppxPackage Microsoft.XboxApp | Remove-AppxPackage
Write-Host "   removing Microsoft.XboxGameOverlay"
Get-AppxPackage Microsoft.XboxGameOverlay | Remove-AppxPackage
Write-Host "   removing Microsoft.XboxGamingOverlay"
Get-AppxPackage Microsoft.XboxGamingOverlay | Remove-AppxPackage
Write-Host "   removing Microsoft.XboxIdentityProvider"
Get-AppxPackage Microsoft.XboxIdentityProvider | Remove-AppxPackage
Write-Host "   removing Microsoft.XboxSpeechToTextOverlay"
Get-AppxPackage Microsoft.XboxSpeechToTextOverlay | Remove-AppxPackage
reg add HKCR\ms-gamebar /f /ve /d URL:ms-gamebar 2>&1 >''
reg add HKCR\ms-gamebar /f /v "URL Protocol" /d "" 2>&1 >''
reg add HKCR\ms-gamebar /f /v "NoOpenWith" /d "" 2>&1 >''
reg add HKCR\ms-gamebar\shell\open\command /f /ve /d "\`"$env:SystemRoot\System32\systray.exe\`"" 2>&1 >''
reg add HKCR\ms-gamebarservices /f /ve /d URL:ms-gamebarservices 2>&1 >''
reg add HKCR\ms-gamebarservices /f /v "URL Protocol" /d "" 2>&1 >''
reg add HKCR\ms-gamebarservices /f /v "NoOpenWith" /d "" 2>&1 >''
reg add HKCR\ms-gamebarservices\shell\open\command /f /ve /d "\`"$env:SystemRoot\System32\systray.exe\`"" 2>&1 >''
