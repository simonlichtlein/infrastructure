function CreateRegKeysRecursive($path) {
    Write-Verbose "checking $path"

    # split path at each \
    $split = $path -split '\\'

    $pathToCreate = ""
    for ($i = 0; $i -lt $split.Count; $i++) {

        $pathToCreate += $split[$i] + "\"

        Write-Verbose "checking split path $pathToCreate"

        if (!(Test-Path $pathToCreate)) {
            Write-Verbose "path does not exist - creating"
            New-Item -path $pathToCreate -ItemType Directory | Out-Null
        }
        else {
            Write-Verbose "path does exist - skipping"
        }
    }
}

function Merge-RegKey($path, $name, $value, $type) {
    CreateRegKeysRecursive $path
    New-ItemProperty -path $path -name $name -value $value -PropertyType $type -Force | Out-Null
}

# desktop add "This PC" to Desktop | instant
Write-Host "   add `"This PC`" to Desktop"
Merge-RegKey -path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\NewStartPanel" -name "{20D04FE0-3AEA-1069-A2D8-08002B30309D}" -value 0 -type DWORD

# desktop show windows version | instant
Write-Host "   show windows version on desktop"
Merge-RegKey -path "HKCU:\Control Panel\Desktop" -name "PaintDesktopVersion" -value 1 -type DWORD

# explorer compact mode | instant
Write-Host "   enable explorer compact mode"
Merge-RegKey -path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -name "UseCompactMode" -value 1 -type DWORD

# explorer show extensions | instant
Write-Host "   show extension in explorer"
Merge-RegKey -path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -name "HideFileExt" -value 0 -type DWORD

# explorer show hidden files | instant
Write-Host "   show hidden files"
Merge-RegKey -path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -name "Hidden" -value 1 -type DWORD 

# explorer open to thisPC | instant
Write-Host "   explorer open to `"This PC`" by default"
Merge-RegKey -path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -name "LaunchTo" -value 1 -type DWORD

# explorer enable clipboard history | instant
Write-Host "   enable clipboard history"
Merge-RegKey -path "HKCU:\SOFTWARE\Microsoft\Clipboard" -name "EnableClipboardHistory" -value 1 -type DWORD 

# explorer delete confirmation | instant (after explorer restart)
Write-Host "   enable delete confirmation"
Merge-RegKey -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer" -name "ConfirmFileDelete" -value 1 -type DWORD 

# explorer add folders to this pc file explorer | instant
Write-Host "   add folders to this pc in file explorer"
# Desktop
Remove-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\MyComputer\NameSpace\{B4BFCC3A-DB2C-424C-B029-7FE99A87C641}" -Name "HideIfEnabled" -Force
# Documents
Remove-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\MyComputer\NameSpace\{d3162b92-9365-467a-956b-92703aca08af}" -Name "HideIfEnabled" -Force
# Downloads
Remove-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\MyComputer\NameSpace\{088e3905-0323-4b02-9826-5d99428e115f}" -Name "HideIfEnabled" -Force
# Pictures
Remove-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\MyComputer\NameSpace\{24ad3ad4-a569-4530-98e1-ab02f9417aa8}" -Name "HideIfEnabled" -Force
# Music
# Remove-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\MyComputer\NameSpace\{3dfdf296-dbec-4fb4-81d1-6a3438bcf4de}" -Name "HideIfEnabled" -Force
# Videos
# Remove-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\MyComputer\NameSpace\{f86fa3ab-70d2-4fc7-9c99-fcbf05467f3a}" -Name "HideIfEnabled" -Force

# explorer faster (source: https://github.com/ChrisTitusTech/winutil/issues/1679) | instant
Write-Host "   set foldertype for notspecified for faster explorer perfomance"
Merge-RegKey -path "HKCU:\Software\Classes\Local Settings\Software\Microsoft\Windows\Shell\Bags\AllFolders\Shell" -Name "FolderType" -value "NotSpecified" -type String

#! NOT WORKING W11 TODO
# taskbar always show all tray icons
# Write-Host "   always show all tray icons"
# Merge-RegKey -path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer" -name EnableAutoTray -value 0 -type DWORD

# taskbar left allign
Write-Host "   allign taskbar on left side"
Merge-RegKey -path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -name "TaskbarAl" -value 0 -type DWORD 

# taskbar hide search icon | instant
Write-Host "   hide `"Search`" Taskicon"
Merge-RegKey -path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Search" -name "SearchboxTaskbarMode" -value 0 -type DWORD

# taskbar hide default volume icon (for eartrumpet)
Write-Host "   hide sytem volume icon for eartrumpet"
Merge-RegKey -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer" -name "HideSCAVolume" -value 1 -type DWORD 

# disable audio ducking | instant 
Write-Host "   diable audio ducking"
Merge-RegKey -path "HKCU:\SOFTWARE\Microsoft\Multimedia\Audio" -name "UserDuckingPreference" -value 3 -type DWORD 

# disable sticky keys | instant
Write-Host "   disable sticky keys"
Merge-RegKey -path "HKCU:\Control Panel\Accessibility\StickyKeys" -name "Flags" -value "506" -type String 

# disable auto install apps - | instant to test
Write-Host "   disable auto installation of apps"
Merge-RegKey -path "HKCU:\Software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager" -name "SilentInstalledAppsEnabled" -value 0 -type DWORD
# set network type private
Write-Host "   set networktype private on ipv4/ipv6 connected network"
Get-NetConnectionProfile | Where { $_.IPv4Connectivity -eq "Internet" } | Set-NetConnectionProfile -NetworkCategory Private
Get-NetConnectionProfile | Where { $_.IPv6Connectivity -eq "Internet" } | Set-NetConnectionProfile -NetworkCategory Private

# RDP
Write-Host "   enable RDP"
Merge-RegKey -path "HKLM:\SYSTEM\CurrentControlSet\Control\Terminal Server" -name "fDenyTSConnections" -value 0 -type DWORD
Merge-RegKey -path "HKLM:\SYSTEM\CurrentControlSet\Control\Lsa" -name "LimitBlankPasswordUse" -value 0 -type DWORD
Enable-NetFirewallRule -DisplayGroup "Remote Desktop"
# Todo for german systems: Enable-NetFirewallRule -DisplayGroup "Remotedesktop"

# Network File/Folder Sharing
# enable network discovery
Set-NetFirewallRule -DisplayGroup 'Network Discovery' -Profile Private -Enabled true
# turn off password protected sharing 
Merge-RegKey -path "HKLM:\SYSTEM\CurrentControlSet\Control\Lsa" -name "everyoneincludeanonymous" -value 1 -type DWORD
Merge-RegKey -path "HKLM:\SYSTEM\CurrentControlSet\Services\LanmanServer\Parameters" -name restrictnullsessaccess -value 0 -type DWORD
# Turn on file and printer sharing
Set-NetFirewallRule -DisplayGroup "File And Printer Sharing" -Profile Private -Enabled True

# defender warnings
# account protection - microsoft account | instant (reopen security center)
Merge-RegKey -path "HKCU:\SOFTWARE\Microsoft\Windows Security Health\State" -name "AccountProtection_MicrosoftAccount_Disconnected" -value 0 -type DWORD

# disable auto reconnect network drives (diable could not connect to all network drives)
Merge-RegKey -path "HKLM:\SYSTEM\CurrentControlSet\Control\NetworkProvider" -name "NetworkProvider" -value 0 -type DWORD

# enable unauthorized guest access to file shares 
Merge-RegKey -path "HKLM:\SYSTEM\CurrentControlSet\Services\LanmanWorkstation\Parameters" -name "AllowInsecureGuestAuth" -value 0 -type DWORD

# classic control panel & small icons | instant
Merge-RegKey -path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\ControlPanel" -name "AllItemsIconView" -value 1 -type DWORD
Merge-RegKey -path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\ControlPanel" -name "StartupPage" -value 1 -type DWORD

# hide taskview from taskbar | instant
Merge-RegKey -path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -name "ShowTaskViewButton" -value 0 -type DWORD

# disable UAC | instant
Merge-RegKey -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System" -name "ConsentPromptBehaviorAdmin" -value 0 -type DWORD

# hide learn about this picture from desktop | instant
Merge-RegKey -path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\NewStartPanel" -name "{2cc5ca98-6485-489a-920e-b3e88a6ccce3}" -value 1 -type DWORD

# show all tray icons (once)
Merge-RegKey -Path "HKCU:\Control Panel\NotifyIconSettings\*" -Name 'IsPromoted' -Value 1 -type DWORD

# deactivate Fast Startup | reboot
Merge-RegKey -path "HKLM:\SYSTEM\CurrentControlSet\Control\Session Manager\Power" -name "HiberbootEnabled" -value 0 -type DWORD