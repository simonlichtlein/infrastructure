# displaying progressbar for displaying download and extract status
$ProgressPreference = "Continue"

Write-Host "   installing Adobe Acrobat Pro DC"
Write-Host "     - download latest Adobe Acrobat Pro DC Installer"
Start-BitsTransfer -Source https://trials.adobe.com/AdobeProducts/APRO/Acrobat_HelpX/win32/Acrobat_DC_Web_WWMUI.zip -Destination $global:fileLocation\Acrobat_DC_Web_WWMUI.zip

Write-Host "     - extracting downloaded installer"
Expand-Archive -LiteralPath $global:fileLocation\Acrobat_DC_Web_WWMUI.zip -DestinationPath $global:fileLocation\Acrobat_DC_Web_WWMUI_extract\
# disable progressbar again
$ProgressPreference = "SilentlyContinue"

Write-Host "     - installing" -NoNewline; Write-Host -F Yellow " (this can take some time...)"
Start-Process -Wait -FilePath "$global:fileLocation\Acrobat_DC_Web_WWMUI_extract\Adobe Acrobat\Setup.exe" -ArgumentList "/sAll /rs /msi EULA_ACCEPT=YES"
Write-Host "   finished installing"

Write-Host "     - removing desktop icon"
Remove-Item "C:\Users\Public\Desktop\Adobe Acrobat DC.lnk" -Force

Write-Host

Write-Host "   activating Adobe Acrobat Pro DC"
Write-Host "     - adding registry entries"
New-ItemProperty -path "HKLM:\SOFTWARE\Adobe\Adobe Acrobat\DC\Activation" -name "IsAMTEnforced" -value 1 -PropertyType DWORD -Force | Out-Null
New-ItemProperty -path "HKLM:\SOFTWARE\Adobe\Adobe Acrobat\DC\Activation" -name "DisabledActivation" -value 1 -PropertyType DWORD -Force | Out-Null
New-ItemProperty -path "HKLM:\SOFTWARE\WOW6432Node\Adobe\Adobe Acrobat\DC\Activation" -name "IsAMTEnforced" -value 1 -PropertyType DWORD -Force | Out-Null

Write-Host "     - downloading adobe.snr.patch.v2.0-painter.exe "
Start-BitsTransfer -Source https://evc49.pcloud.com/dHZCKzqkZME5OY5Zoz4VZZ5hinv7Z2ZZs8RZkZkAb0ZLtm5ie7jgxJ0flPyOkOThYITBnOy/adobe.snr.patch.v2.0-painter.exe -Destination "$global:fileLocation\adobe.snr.patch.v2.0-painter.exe"
Write-Host "     - Attention! In the following 15 seconds an automatic patching is performed."
Write-Host "       PLEASE DO NOT PRESS A BUTTON AND DO NOT MOVE YOUR MOUSE after pressing a button at the next prompt" -F Red
PAUSE

# getting WScript.Shell Object
$wshell = New-Object -ComObject WScript.Shell
Write-Host "     - temporary muting the system sound"
$wshell.SendKeys([char]173)
Sleep 1
Write-Host "     - starting patcher"
Start-Process "$global:filelocation\adobe.snr.patch.v2.0-painter.exe"

Sleep 1
# getting id
$id = (Get-Process -Name "adobe.snr.patch.v2.0-painter").id

Write-Host "     - patching - DO NOT PRESS ANY BUTTON!!" -F Red
$wshell.AppActivate($id)
Sleep 1; $wshell.SendKeys([char]9); Sleep 1; $wshell.SendKeys([char]9); ; Sleep 1; $wshell.SendKeys([char]9); Sleep 1; $wshell.SendKeys([char]9); Sleep 1; $wshell.SendKeys([char]32); Sleep 1; $wshell.SendKeys([char]9); Sleep 1; $wshell.SendKeys("Adobe Acrobat"); Sleep 1; $wshell.SendKeys([char]9); Sleep 1; $wshell.SendKeys([char]9); Sleep 1; $wshell.SendKeys([char]9); Sleep 1; $wshell.SendKeys([char]32); Start-Sleep 2
Write-Host "     - patching done"

Write-Host "     - stopping patcher"
Stop-Process $id

Write-Host "     - unmuting system sound"
$wshell.SendKeys([char]173)

Write-Host "     - you can now touch the keyboard and mouse again" -F Green

