### This will help discover other Devices or PC's and especially if PC ICONS are Missing in the Network Folder
```powershell
Get-Service "Function Discovery Provider Host" | Set-Service -StartupType Automatic
Get-Service "Function Discovery Provider Host" | Start-Service
```
### Also make sure these `SSDP Discovery` and `UPnP Device Host` are already be running as they should be
```powershell
Get-Service "SSDP Discovery" | Start-Service
Get-Service "UPnP Device Host" | Start-Service
```