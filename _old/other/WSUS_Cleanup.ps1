<#
    WSUS Database and ServerCleanup

    Simon Lichtlein - IB54SL - ap7r4

    SQL Scripts will be executed against the internals WSUS SUSDB with the following command
        "C:\Program Files\Microsoft SQL Server\Client SDK\ODBC\170\Tools\Binn\sqlcmd.exe" -S \\.\pipe\MICROSOFT`#`#WID\tsql\query -i query.sql
    "Microsoft Command Line Utils for SQL Server"

    v0.4 - 21.10.2021
        distinction between internal and external database. If External use the same Registry Value. If Internal build String with "\\.\.pipe\.."
    v0.3 - 21.10.2021
        Read SQLServerName from Registry and run only if WID (internals)
    v0.2 - 20.10.2021
        check if synchronization is in progress
        temporaly disable automatic synchronization
    v0.1 - 15.10.2021
        initial creation
        SQL Script from https://docs.microsoft.com/en-us/troubleshoot/mem/configmgr/wsus-maintenance-guide and https://docs.microsoft.com/en-us/troubleshoot/mem/configmgr/reindex-the-wsus-database
#>

<# BEGIN OF SQL SCRIPTS #>
$SQL_1_create_custom_index = "-- Create custom index in tbLocalizedPropertyForRevision
USE [SUSDB]

CREATE NONCLUSTERED INDEX [nclLocalizedPropertyID] ON [dbo].[tbLocalizedPropertyForRevision]
(
     [LocalizedPropertyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

-- Create custom index in tbRevisionSupersedesUpdate
CREATE NONCLUSTERED INDEX [nclSupercededUpdateID] ON [dbo].[tbRevisionSupersedesUpdate]
(
     [SupersededUpdateID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]"

$SQL_2_reindex = "USE SUSDB; 
GO 
SET NOCOUNT ON; 
 
-- Rebuild or reorganize indexes based on their fragmentation levels 
DECLARE @work_to_do TABLE ( 
    objectid int 
    , indexid int 
    , pagedensity float 
    , fragmentation float 
    , numrows int 
) 
 
DECLARE @objectid int; 
DECLARE @indexid int; 
DECLARE @schemaname nvarchar(130);  
DECLARE @objectname nvarchar(130);  
DECLARE @indexname nvarchar(130);  
DECLARE @numrows int 
DECLARE @density float; 
DECLARE @fragmentation float; 
DECLARE @command nvarchar(4000);  
DECLARE @fillfactorset bit 
DECLARE @numpages int 
 
-- Select indexes that need to be defragmented based on the following 
-- * Page density is low 
-- * External fragmentation is high in relation to index size 
PRINT 'Estimating fragmentation: Begin. ' + convert(nvarchar, getdate(), 121)  
INSERT @work_to_do 
SELECT 
    f.object_id 
    , index_id 
    , avg_page_space_used_in_percent 
    , avg_fragmentation_in_percent 
    , record_count 
FROM  
    sys.dm_db_index_physical_stats (DB_ID(), NULL, NULL , NULL, 'SAMPLED') AS f 
WHERE 
    (f.avg_page_space_used_in_percent < 85.0 and f.avg_page_space_used_in_percent/100.0 * page_count < page_count - 1) 
    or (f.page_count > 50 and f.avg_fragmentation_in_percent > 15.0) 
    or (f.page_count > 10 and f.avg_fragmentation_in_percent > 80.0) 
 
PRINT 'Number of indexes to rebuild: ' + cast(@@ROWCOUNT as nvarchar(20)) 
 
PRINT 'Estimating fragmentation: End. ' + convert(nvarchar, getdate(), 121) 
 
SELECT @numpages = sum(ps.used_page_count) 
FROM 
    @work_to_do AS fi 
    INNER JOIN sys.indexes AS i ON fi.objectid = i.object_id and fi.indexid = i.index_id 
    INNER JOIN sys.dm_db_partition_stats AS ps on i.object_id = ps.object_id and i.index_id = ps.index_id 
 
-- Declare the cursor for the list of indexes to be processed. 
DECLARE curIndexes CURSOR FOR SELECT * FROM @work_to_do 
 
-- Open the cursor. 
OPEN curIndexes 
 
-- Loop through the indexes 
WHILE (1=1) 
BEGIN 
    FETCH NEXT FROM curIndexes 
    INTO @objectid, @indexid, @density, @fragmentation, @numrows; 
    IF @@FETCH_STATUS < 0 BREAK; 
 
    SELECT  
        @objectname = QUOTENAME(o.name) 
        , @schemaname = QUOTENAME(s.name) 
    FROM  
        sys.objects AS o 
        INNER JOIN sys.schemas as s ON s.schema_id = o.schema_id 
    WHERE  
        o.object_id = @objectid; 
 
    SELECT  
        @indexname = QUOTENAME(name) 
        , @fillfactorset = CASE fill_factor WHEN 0 THEN 0 ELSE 1 END 
    FROM  
        sys.indexes 
    WHERE 
        object_id = @objectid AND index_id = @indexid; 
 
    IF ((@density BETWEEN 75.0 AND 85.0) AND @fillfactorset = 1) OR (@fragmentation < 30.0) 
        SET @command = N'ALTER INDEX ' + @indexname + N' ON ' + @schemaname + N'.' + @objectname + N' REORGANIZE'; 
    ELSE IF @numrows >= 5000 AND @fillfactorset = 0 
        SET @command = N'ALTER INDEX ' + @indexname + N' ON ' + @schemaname + N'.' + @objectname + N' REBUILD WITH (FILLFACTOR = 90)'; 
    ELSE 
        SET @command = N'ALTER INDEX ' + @indexname + N' ON ' + @schemaname + N'.' + @objectname + N' REBUILD'; 
    PRINT convert(nvarchar, getdate(), 121) + N' Executing: ' + @command; 
    EXEC (@command); 
    PRINT convert(nvarchar, getdate(), 121) + N' Done.'; 
END 
 
-- Close and deallocate the cursor. 
CLOSE curIndexes; 
DEALLOCATE curIndexes; 
 
 
IF EXISTS (SELECT * FROM @work_to_do) 
BEGIN 
    PRINT 'Estimated number of pages in fragmented indexes: ' + cast(@numpages as nvarchar(20)) 
    SELECT @numpages = @numpages - sum(ps.used_page_count) 
    FROM 
        @work_to_do AS fi 
        INNER JOIN sys.indexes AS i ON fi.objectid = i.object_id and fi.indexid = i.index_id 
        INNER JOIN sys.dm_db_partition_stats AS ps on i.object_id = ps.object_id and i.index_id = ps.index_id 
 
    PRINT 'Estimated number of pages freed: ' + cast(@numpages as nvarchar(20)) 
END 
GO 
 
 
--Update all statistics 
PRINT 'Updating all statistics.' + convert(nvarchar, getdate(), 121)  
EXEC sp_updatestats 
PRINT 'Done updating statistics.' + convert(nvarchar, getdate(), 121)  
GO"

$SQL_3_set_PK_on_revisionList_table = "USE [SUSDB]
GO

/****** Object:  StoredProcedure [dbo].[spDeleteUpdate]    Script Date: 11/2/2020 8:55:02 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 
ALTER PROCEDURE [dbo].[spDeleteUpdate]
    @localUpdateID int
AS
SET NOCOUNT ON
BEGIN TRANSACTION
SAVE TRANSACTION DeleteUpdate
DECLARE @retcode INT
DECLARE @revisionID INT
DECLARE @revisionList TABLE(RevisionID INT PRIMARY KEY)
INSERT INTO @revisionList (RevisionID)
    SELECT r.RevisionID FROM dbo.tbRevision r
        WHERE r.LocalUpdateID = @localUpdateID
IF EXISTS (SELECT b.RevisionID FROM dbo.tbBundleDependency b WHERE b.BundledRevisionID IN (SELECT RevisionID FROM @revisionList))
   OR EXISTS (SELECT p.RevisionID FROM dbo.tbPrerequisiteDependency p WHERE p.PrerequisiteRevisionID IN (SELECT RevisionID FROM @revisionList))
BEGIN
    RAISERROR('spDeleteUpdate got error: cannot delete update as it is still referenced by other update(s)', 16, -1)
    ROLLBACK TRANSACTION DeleteUpdate
    COMMIT TRANSACTION
    RETURN(1)
END
INSERT INTO @revisionList (RevisionID)
    SELECT DISTINCT b.BundledRevisionID FROM dbo.tbBundleDependency b
        INNER JOIN dbo.tbRevision r ON r.RevisionID = b.RevisionID
        INNER JOIN dbo.tbProperty p ON p.RevisionID = b.BundledRevisionID
        WHERE r.LocalUpdateID = @localUpdateID
            AND p.ExplicitlyDeployable = 0
IF EXISTS (SELECT IsLocallyPublished FROM dbo.tbUpdate WHERE LocalUpdateID = @localUpdateID AND IsLocallyPublished = 1)
BEGIN
    INSERT INTO @revisionList (RevisionID)
        SELECT DISTINCT pd.PrerequisiteRevisionID FROM dbo.tbPrerequisiteDependency pd
            INNER JOIN dbo.tbUpdate u ON pd.PrerequisiteLocalUpdateID = u.LocalUpdateID
            INNER JOIN dbo.tbProperty p ON pd.PrerequisiteRevisionID = p.RevisionID
            WHERE u.IsLocallyPublished = 1 AND p.UpdateType = 'Category'
END
DECLARE #cur CURSOR LOCAL FAST_FORWARD FOR
    SELECT t.RevisionID FROM @revisionList t ORDER BY t.RevisionID DESC
OPEN #cur
FETCH #cur INTO @revisionID
WHILE (@@ERROR=0 AND @@FETCH_STATUS=0)
BEGIN
    IF EXISTS (SELECT b.RevisionID FROM dbo.tbBundleDependency b WHERE b.BundledRevisionID = @revisionID
                   AND b.RevisionID NOT IN (SELECT RevisionID FROM @revisionList))
       OR EXISTS (SELECT p.RevisionID FROM dbo.tbPrerequisiteDependency p WHERE p.PrerequisiteRevisionID = @revisionID
                      AND p.RevisionID NOT IN (SELECT RevisionID FROM @revisionList))
    BEGIN
        DELETE FROM @revisionList WHERE RevisionID = @revisionID
        IF (@@ERROR <> 0)
        BEGIN
            RAISERROR('Deleting disqualified revision from temp table failed', 16, -1)
            GOTO Error
        END
    END
    FETCH NEXT FROM #cur INTO @revisionID
END
IF (@@ERROR <> 0)
BEGIN
    RAISERROR('Fetching a cursor to value a revision', 16, -1)
    GOTO Error
END
CLOSE #cur
DEALLOCATE #cur
DECLARE #cur CURSOR LOCAL FAST_FORWARD FOR
    SELECT t.RevisionID FROM @revisionList t ORDER BY t.RevisionID DESC
OPEN #cur
FETCH #cur INTO @revisionID
WHILE (@@ERROR=0 AND @@FETCH_STATUS=0)
BEGIN
    EXEC @retcode = dbo.spDeleteRevision @revisionID
    IF @@ERROR <> 0 OR @retcode <> 0
    BEGIN
        RAISERROR('spDeleteUpdate got error from spDeleteRevision', 16, -1)
        GOTO Error
    END
    FETCH NEXT FROM #cur INTO @revisionID
END
IF (@@ERROR <> 0)
BEGIN
    RAISERROR('Fetching a cursor to delete a revision', 16, -1)
    GOTO Error
END
CLOSE #cur
DEALLOCATE #cur
COMMIT TRANSACTION
RETURN(0)
Error:
    CLOSE #cur
    DEALLOCATE #cur
    IF (@@TRANCOUNT > 0)
    BEGIN
        ROLLBACK TRANSACTION DeleteUpdate
        COMMIT TRANSACTION
    END
    RETURN(1)
GO"
<# END OF SQL SCRIPTS #>

$currentPath = (Get-Location).Path

# read SQLServerName
if (Test-Path "HKLM:\SOFTWARE\Microsoft\Update Services\Server\Setup") {
    $db_registry = Get-ItemPropertyValue -Path "HKLM:\SOFTWARE\Microsoft\Update Services\Server\Setup" -Name "SQLServerName"

    <#
    Beispiel:
        Internal:
        Registry: MICROSOFT##WID
        -S \\.\pipe\MICROSOFT##WID\tsql\query

        External:
        Registry: VDB0547.dmz.huk.de\MSSQL37,2137 
        -S VDB0537.dmz.huk.de\MSSQL37,2137
    #>

    if ($db_registry -match "##WID") {
        $db = "\\.\pipe\$db_registry\tsql\query"
    }
    else {
        $db = $db_registry
    }
    Write-Host "Using the following DB: $db"
}
else {
    Write-Host -F Red "SQLServerName not found!"
    Exit
}

# check if sync is running
if ((Get-WsusServer).GetSubscription().GetSynchronizationStatus() -ne "NotProcessing") {
    Write-Host -F Red "WSUS Synchronization in progress"
    Write-Host -F DarkGray ("SyncStatus: " + (Get-WsusServer).GetSubscription().GetSynchronizationStatus())
    Write-Host -F DarkGray ("SyncPhase:  " + (Get-WsusServer).GetSubscription().GetSynchronizationStatus())
    Exit
}

Write-Host
# get current sync settings
$sync = (Get-WSUSServer).GetSubscription()
Write-Host "SynchronizeAutomatically = $((Get-WSUSServer).GetSubscription().SynchronizeAutomatically)"
$wasSyncActivated = $True

# chek if automatic sync is already deactivated
if ($sync.SynchronizeAutomatically) {
    Write-Host "Temporarily deactivating the automatic synchronization"
    Write-Host -F DarkGray "  backup current sync settings to '$currentPath\sync.xml'"

    $sync | Export-Clixml $currentPath\sync.xml
    $sync.SynchronizeAutomatically = $False
    $sync.Save()

    Write-Host "  SynchronizeAutomatically = $((Get-WSUSServer).GetSubscription().SynchronizeAutomatically)"
}
else {
    Write-Host "Sync is already deactivated - keeping sync settings"
    $wasSyncActivated = $False
}

function Invoke-SQLCleanup($type, $sql) {
    Write-Host -F DarkGray "  $type"

    $sql | Out-File "$currentPath\execute.sql" -Force

    try {
        $stopwatch = [System.Diagnostics.stopwatch]::StartNew()
        & "C:\Program Files\Microsoft SQL Server\Client SDK\ODBC\170\Tools\Binn\sqlcmd.exe" -S $db -i "$currentPath\execute.sql" | 
        Out-File ($currentPath + "\" + (Get-Date -Format "yyyy.MM.dd") + "-$type.log") -Append
        Write-Host -F green "    $type finished in $($stopwatch.Elapsed)"
    }
    catch {
        Write-Host -F red "    ERROR on $type"
    }
    finally {
        $stopwatch.Stop()
        Remove-Item "$currentPath\execute.sql" -Force
    }
}

function Invoke-Cleanup($type) {
    Write-Host -F DarkGray "  $type"
    try {
        $stopwatch = [System.Diagnostics.stopwatch]::StartNew()
        Invoke-Expression "Invoke-WsusServerCleanup -$type"
        Write-Host "    $type finished in $($stopwatch.Elapsed)"
    }
    catch {
        Write-Host "    ERROR on $type"
    }
    finally {
        $stopwatch.Stop()
    }
}

Write-Host
Write-Host "starting SQLCleanup"
Invoke-SQLCleanup "1_create_custom_index" $SQL_1_create_custom_index
Invoke-SQLCleanup "2_reindex" $SQL_2_reindex
Invoke-SQLCleanup "3_set_PK_on_revisionList_table" $SQL_3_set_PK_on_revisionList_table

Write-Host
Write-Host "starting WSUSServerCleanup"
Invoke-Cleanup "DeclineExpiredUpdates" # Update older than 30 days
Invoke-Cleanup "CleanupObsoleteUpdates"
Invoke-Cleanup "CleanupUnneededContentFiles"
Invoke-Cleanup "CompressUpdates"
# Invoke-Cleanup "CleanupObsoleteComputers"


Write-Host
if ($wasSyncActivated) {
    Write-Host "Resetting automatic synchronization"
    $sync = (Get-WSUSServer).GetSubscription()
    $sync.SynchronizeAutomatically = $True
    $sync.Save()
    Write-Host "  SynchronizeAutomatically = $((Get-WSUSServer).GetSubscription().SynchronizeAutomatically)"
}
