Clear-Host
Write-Host "|==============================================|"
Write-Host "|        winget automated installation         |"
Write-Host "|" -NoNewLine 
Write-Host "----------------------------------------------" -NoNewLine -F DarkGray
Write-Host "|"
Write-Host "| " -NoNewline
Write-Host "simon@lichtlein.one" -NoNewline -F Red 
Write-Host "    -    " -NoNewline
Write-Host "gitlab.com/lichs" -NoNewline -F Red
Write-Host " |"
Write-Host "|==============================================|"
Write-Host

$fileLocation = $ENV:TEMP + '\lichs'
mkdir $fileLocation -ErrorAction SilentlyContinue | Out-Null

Write-Host "   installing dependencys"
Write-Host "     - downloading Microsoft.VCLibs.x64.14.00.Desktop" -F DarkGray
Start-BitsTransfer -Source https://aka.ms/Microsoft.VCLibs.x64.14.00.Desktop.appx -Destination $fileLocation\Microsoft.VCLibs.x64.14.00.Desktop.appx
Write-Host "     - installing Microsoft.VCLibs.x64.14.00.Desktop" -F DarkGray
Add-AppxPackage -Path $fileLocation\Microsoft.VCLibs.x64.14.00.Desktop.appx -ErrorAction SilentlyContinue
Remove-Item -Path $fileLocation\Microsoft.VCLibs.x64.14.00.Desktop.appx -Force

Write-Host

Write-Host "     - downloading Microsoft.VCLibs.x86.14.00.Desktop" -F DarkGray
Start-BitsTransfer -Source https://aka.ms/Microsoft.VCLibs.x86.14.00.Desktop.appx -Destination $fileLocation\Microsoft.VCLibs.x86.14.00.Desktop.appx
Write-Host "     - installing Microsoft.VCLibs.x86.14.00.Desktop" -F DarkGray
Add-AppxPackage -Path $fileLocation\Microsoft.VCLibs.x86.14.00.Desktop.appx -ErrorAction SilentlyContinue
Remove-Item -Path $fileLocation\Microsoft.VCLibs.x86.14.00.Desktop.appx -Force

Write-Host "   dependencys installed"
Write-Host

if (!(Get-Command winget -ErrorAction SilentlyContinue)) {
    Write-Host "   winget is not installed" -F Red
    Write-Host "     - downloading latest winget-cli" -F DarkGray
    Start-BitsTransfer -Source https://github.com/microsoft/winget-cli/releases/latest/download/Microsoft.DesktopAppInstaller_8wekyb3d8bbwe.msixbundle -Destination $fileLocation\Microsoft.DesktopAppInstaller_8wekyb3d8bbwe.msixbundle
    Write-Host "     - installing latest winget-cli" -F DarkGray
    Add-AppxPackage -Path $fileLocation\Microsoft.DesktopAppInstaller_8wekyb3d8bbwe.msixbundle
    Remove-Item -Path $fileLocation\Microsoft.DesktopAppInstaller_8wekyb3d8bbwe.msixbundle -Force
}

if (Get-Command winget -ErrorAction SilentlyContinue) {
    $version = winget -v
    Write-Host "   winget installed with version $version" -F Green
}
else {
    Write-Host "   error while testing winget! please try again" -F Red
    PAUSE
    Exit 1
}

PAUSE