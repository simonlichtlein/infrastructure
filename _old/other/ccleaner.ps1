Clear-Host
Write-Host "|==============================================|"
Write-Host "|           CCleaner portable update           |"
Write-Host "|" -NoNewLine 
Write-Host "----------------------------------------------" -NoNewLine -F DarkGray
Write-Host "|"
Write-Host "| " -NoNewline
Write-Host "simon@lichtlein.one" -NoNewline -F Red 
Write-Host "    -    " -NoNewline
Write-Host "gitlab.com/lichs" -NoNewline -F Red
Write-Host " |"
Write-Host "|==============================================|"
Write-Host

param ($keepZip, $keepLangs, $keep32Bit)

$release_notes = "https://www.ccleaner.com/ccleaner/release-notes"
$builds        = "https://www.ccleaner.com/de-de/ccleaner/builds"

# check if installed
if (Test-Path .\CCleaner64.exe) {
	# get new version number
	$webpage = ((Invoke-WebRequest -UseBasicParsing -Uri $release_notes).RawContent).toString()
	
	# parse html code to version
	$vers_new = $webpage.Split("<h4 class=`"mb--xs`">Release Notes</h4>")[1].Split("</h6>")[0].Replace("<h6>v","").Split("(")[0].Trim()
	
	# get installed version and replace 0 in version to match webpage version style (WEB: 5.67.7763 EXE:5.67.0.7763)
	$vers_old = [System.Diagnostics.FileVersionInfo]::GetVersionInfo("$(Get-Location)\CCleaner64.exe").FileVersion
	$vers_old = $vers_old.Replace(".0","")
} else {
	# always update if not installed
	$vers_new = "1.1.1"
	$vers_old = "0.0.0"
}

# parse string version into version object and compare them
if ([version]("{0}.{1}.{2}" -f $vers_new.split(".")) -gt [version]("{0}.{1}.{2}" -f $vers_old.split("."))) {
	Write-Host "Newer Version Found!" -F Blue
	Write-Host "Old: $vers_old" -F Red
	Write-Host "New: $vers_new" -F Green
	
	# get new version download link
	$webrequest = Invoke-WebRequest -UseBasicParsing -Uri $builds
	$href = ( $webrequest.Links | Where { $_.href -match "https://download.ccleaner.com/portable" } ).href

	# download new version
	Invoke-WebRequest $href -OutFile .\ccleaner.portable.zip

	# extract new version
	Expand-Archive -Force ".\ccleaner.portable.zip" .\

	If (!($keep32Bit)) {
		Remove-Item .\CCleaner.exe
	}
	
	If (!($keepLangs)) {
		# Remove-Item .\lang\* -Exclude lang-1031.dll
		Remove-Item .\lang -Recurse
	}

	If (!($keepZip)) {
		Remove-Item .\ccleaner.portable.zip
	}

} else {
	Write-Host "No new version found" -F Blue
	Write-Host "Old = $vers_old" -F DarkGray
	Write-Host "New = $vers_new" -F DarkGray 
}

$path_hklm = "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\CCleaner"
If (!(Test-Path $path_hklm)) {
	New-Item -Path $path_hklm -ItemType Directory
}

New-ItemProperty -Path $path -Name "DisplayName" -Value "CCleaner" -Type String -Force
New-ItemProperty -Path $path -Name "DisplayVersion" -Value "$vers_new" -Type String -Force
New-ItemProperty -Path $path -Name "DisplayIcon" -Value "$(Get-Location)\CCleaner64.exe" -Type String -Force
New-ItemProperty -Path $path -Name "InstallLocation" -Value "$(Get-Location)\" -Type String -Force
New-ItemProperty -Path $path -Name "InstallDate" -Value "$(Get-Date -Format yyyyMMdd)" -Type String -Force

PAUSE