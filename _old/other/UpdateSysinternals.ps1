Invoke-RestMethod -Uri "https://download.sysinternals.com/files/SysinternalsSuite.zip" -OutFile .\Suite.zip

& "C:\Program Files\7-Zip\7z.exe" x Suite.zip -o"P:\sysinternals\Test"

Remove-Item .\Suite.zip
Get-ChildItem | % { ($_.Name).Split('.')[0] }