Clear-Host
Write-Host "|==============================================|"
Write-Host "|      auto-install VCLibs Appx Packages       |"
Write-Host "|" -NoNewLine 
Write-Host "----------------------------------------------" -NoNewLine -F DarkGray
Write-Host "|"
Write-Host "| " -NoNewline
Write-Host "simon@lichtlein.one" -NoNewline -F Red 
Write-Host "    -    " -NoNewline
Write-Host "gitlab.com/lichs" -NoNewline -F Red
Write-Host " |"
Write-Host "|==============================================|"
Write-Host

Write-Host "downloading Microsoft.VCLibs.140.00_14.0.30035.0_x64"
Start-BitsTransfer -Source https://gitlab.com/lichs/windowstools/-/raw/main/bin/Microsoft.VCLibs.140.00_14.0.30035.0_x64__8wekyb3d8bbwe.appx?inline=false -Destination $ENV:TEMP\Microsoft.VCLibs.140.00_14.0.30035.0_x64.appx

Write-Host "installing Microsoft.VCLibs.140.00_14.0.30035.0_x64"
Add-AppxPackage -Path $ENV:TEMP\Microsoft.VCLibs.140.00_14.0.30035.0_x64.appx -ErrorAction SilentlyContinue

Write-Host "removing installed Microsoft.VCLibs.140.00_14.0.30035.0_x64 file"
Remove-Item -Path $ENV:TEMP\Microsoft.VCLibs.140.00_14.0.30035.0_x64.appx -Force


Write-Host


Write-Host "downloading Microsoft.VCLibs.140.00_14.0.30035.0_x86"
Start-BitsTransfer -Source https://gitlab.com/lichs/windowstools/-/raw/main/bin/Microsoft.VCLibs.140.00_14.0.30035.0_x86__8wekyb3d8bbwe.appx?inline=false -Destination $ENV:TEMP\Microsoft.VCLibs.140.00_14.0.30035.0_x86.appx

Write-Host "installing Microsoft.VCLibs.140.00_14.0.30035.0_x86"
Add-AppxPackage -Path $ENV:TEMP\Microsoft.VCLibs.140.00_14.0.30035.0_x86.appx

Write-Host "removing installed Microsoft.VCLibs.140.00_14.0.30035.0_x86 file"
Remove-Item -Path $ENV:TEMP\Microsoft.VCLibs.140.00_14.0.30035.0_x86.appx -Force


Write-Host


Write-Host "downloading Microsoft.VCLibs.140.00.UWPDesktop_14.0.30035.0_x64"
Start-BitsTransfer -Source https://gitlab.com/lichs/windowstools/-/raw/main/bin/Microsoft.VCLibs.140.00.UWPDesktop_14.0.30035.0_x64__8wekyb3d8bbwe.Appx?inline=false -Destination $ENV:TEMP\Microsoft.VCLibs.140.00.UWPDesktop_14.0.30035.0_x64.appx

Write-Host "installing Microsoft.VCLibs.140.00.UWPDesktop_14.0.30035.0_x64"
Add-AppxPackage -Path $ENV:TEMP\Microsoft.VCLibs.140.00.UWPDesktop_14.0.30035.0_x64.appx

Write-Host "removing installed Microsoft.VCLibs.140.00.UWPDesktop_14.0.30035.0_x64 file"
Remove-Item -Path $ENV:TEMP\Microsoft.VCLibs.140.00.UWPDesktop_14.0.30035.0_x64.appx


Write-Host


Write-Host "downloading Microsoft.VCLibs.140.00.UWPDesktop_14.0.30035.0_x86"
Start-BitsTransfer -Source https://gitlab.com/lichs/windowstools/-/raw/main/bin/Microsoft.VCLibs.140.00.UWPDesktop_14.0.30035.0_x86__8wekyb3d8bbwe.Appx?inline=false -Destination $ENV:TEMP\Microsoft.VCLibs.140.00.UWPDesktop_14.0.30035.0_x86.appx

Write-Host "nstalling Microsoft.VCLibs.140.00.UWPDesktop_14.0.30035.0_x86"
Add-AppxPackage -Path $ENV:TEMP\Microsoft.VCLibs.140.00.UWPDesktop_14.0.30035.0_x86.appx

Write-Host "removing installed Microsoft.VCLibs.140.00.UWPDesktop_14.0.30035.0_x86 file"
Remove-Item -Path $ENV:TEMP\Microsoft.VCLibs.140.00.UWPDesktop_14.0.30035.0_x86.appx

Write-Host
PAUSE