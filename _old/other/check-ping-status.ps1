[CmdletBinding()]
param (
    [Parameter()] [String] $IP = "192.168.0.9",
    [Parameter()] [String] $tries = "-1",
    [Parameter()] [String] $sleeptime = "25000"
)

<#
Ping statistics for 192.168.0.9:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 0ms, Average = 0ms
#>

$tries_done = 0 
$starttime = Get-Date
$starttime_string = $starttime.ToString("yyyy.MM.dd HH:mm:ss.ffff")
$results = @()

do {

    $result = Test-NetConnection $IP 
    $results += [PSCustomObject]@{
        DateTime      = Get-Date
        Time          = (Get-Date).ToString("yyyy.MM.dd HH:mm:ss.ffff")
        RemoteAddress = $result.RemoteAddress
        SourceAddress = $result.SourceAddress
        PingSucceeded = $result.PingSucceeded
    }

    Clear-Host
    Write-Host ""
    Write-Host "Started at $starttime_string"
    Write-Host ""
    Write-Host -F Green "Time                        Connection"
    Write-Host -F Green "----                        ----------"
    foreach ($r in $results) {

        Write-Host $($r.Time) -NoNewline

        if ($result.PingSucceeded) {
            Write-Host "    Succees"
        }
        else {
            Write-Host "    Failed" -F Red
        }
    }

    Write-Host ""
    Write-Host -F DarkGray " waiting $sleeptime ms.."

    Start-Sleep -Milliseconds $sleeptime

    $tries_done++
    

} while ($tries_done -ne $tries)

Write-Host ""
Write-Host "Ping statistics for $IP"
