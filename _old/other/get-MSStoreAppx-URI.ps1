[CmdletBinding()]
param (
    [Parameter(Mandatory)]
    [String]
    $PackageFamilyName,
    
    [Parameter()]
    [ValidateSet('x64', 'x86', 'arm')]
    [String]
    $arch = "x64"
)


$InvokeWebRequestSplat = @{
    Uri             = 'https://store.rg-adguard.net/api/GetFiles'
    Method          = 'POST'
    ContentType     = 'application/x-www-form-urlencoded'
    Body            = "type=PackageFamilyName&url=$PackageFamilyName&ring=RP&lang=en-US"
    UseBasicParsing = $True
}

$links = (Invoke-WebRequest @InvokeWebRequestSplat).Links

if ($links) {
    $href = $links | Where { $_.OuterHTML -match ".appx" -and $_.OuterHTML -match $arch }

    if ($href.href) {
        return $href.href
    }
    else {
        Write-Error "No href found from PackageFamilyName $PackageFamilyName"
    }
}
else {
    Write-Error "No packages found with PackageFamilyName $PackageFamilyName"
}
return $null