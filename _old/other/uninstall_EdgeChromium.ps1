Clear-Host
Write-Host "|==============================================|"
Write-Host "|          Edge Chromium uninstaller           |"
Write-Host "|" -NoNewLine 
Write-Host "----------------------------------------------" -NoNewLine -F DarkGray
Write-Host "|"
Write-Host "| " -NoNewline
Write-Host "simon@lichtlein.one" -NoNewline -F Red 
Write-Host "    -    " -NoNewline
Write-Host "gitlab.com/lichs" -NoNewline -F Red
Write-Host " |"
Write-Host "|==============================================|"
Write-Host

Write-Host Getting msedge.exe path
Write-Host .
$msedgePath = (Get-ItemProperty 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\msedge.exe').'(Default)';
Write-Host $msedgePath

Write-Host Getting msedge.exe version
Write-Host .
$msedgeVersion = (Get-Item $msedgePath).VersionInfo.ProductVersion
Write-Host $msedgeVersion

$setupPath = (Split-Path -Path $msedgePath) + "\" + $msedgeVersion +  "\Installer\setup.exe"
If (Test-Path $setupPath) {
	Write-Host Uninstalling msedge
	Write-Host .
	Start-Process -FilePath $setupPath -ArgumentList "--uninstall --system-level --verbose-logging --force-uninstall" -Wait
	Write-Host -F Green "Successfully uninstalled msedge"
} Else {
	Write-Host -F Red "setup.exe not found"
	Exit 1
}


PAUSE