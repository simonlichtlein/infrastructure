Clear-Host
Write-Host "|==============================================|"
Write-Host "|             OneDrive uninstaller             |"
Write-Host "|" -NoNewLine 
Write-Host "----------------------------------------------" -NoNewLine -F DarkGray
Write-Host "|"
Write-Host "| " -NoNewline
Write-Host "simon@lichtlein.one" -NoNewline -F Red 
Write-Host "    -    " -NoNewline
Write-Host "gitlab.com/lichs" -NoNewline -F Red
Write-Host " |"
Write-Host "|==============================================|"
Write-Host

Write-Host "killing OneDrive process"
if (Get-Process OneDrive -ErrorAction SilentlyContinue) {
    Get-Process OneDrive | Stop-Process -Force
}
Write-Host "."

Write-Host "uninstalling"
Start-Process -Wait -FilePath "$ENV:SYSTEMROOT\SysWOW64\OneDriveSetup.exe" -ArgumentList "/uninstall"
Write-Host "."

Write-Host "removing OneDrive leftovers"
Write-Host "  deleting $ENV:USERPROFILE\OneDrive" -F DarkGray
if (Test-Path "$ENV:USERPROFILE\OneDrive") {
    Remove-Item "$ENV:USERPROFILE\OneDrive" -Recurse -Force 
}

Write-Host "  deleting C:\OneDriveTemp" -F DarkGray
if (Test-Path "C:\OneDriveTemp" ) {
    Remove-Item "C:\OneDriveTemp" -Recurse -Force
}

Write-Host "  deleting $ENV:LOCALAPPDATA\Microsoft\OneDrive" -F DarkGray
if (Test-Path "$ENV:LOCALAPPDATA\Microsoft\OneDrive" ) {
    Remove-Item "$ENV:LOCALAPPDATA\Microsoft\OneDrive" -Recurse -Force
}

Write-Host "."

Write-Host "removing onedrive in explorer side panel"
Write-Host "  deleting HKEY_CLASSES_ROOT\CLSID\{018D5C66-4533-4307-9B53-224DE2ED1FE6}" -F DarkGray
if (Test-Path "Registry::HKEY_CLASSES_ROOT\CLSID\{018D5C66-4533-4307-9B53-224DE2ED1FE6}") {
    Remove-Item -Path "Registry::HKEY_CLASSES_ROOT\CLSID\{018D5C66-4533-4307-9B53-224DE2ED1FE6}" -Recurse -Force
}

Write-Host "  deleting HKEY_CLASSES_ROOT\CLSID\{018D5C66-4533-4307-9B53-224DE2ED1FE6}" -F DarkGray
if (Test-Path "Registry::HKEY_CLASSES_ROOT\Wow6432Node\CLSID\{018D5C66-4533-4307-9B53-224DE2ED1FE6}") {
    Remove-Item -Path "Registry::HKEY_CLASSES_ROOT\Wow6432Node\CLSID\{018D5C66-4533-4307-9B53-224DE2ED1FE6}" -Recurse -Force
}

Write-Host "."

Write-Host "disable filesyncngsc"

if (!(Test-Path "Registry::HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\OneDrive")) {
    New-Item -path "Registry::HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\OneDrive" -ItemType Directory
}
New-ItemProperty -Path "Registry::HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\OneDrive" -Name "DisableFileSyncNGSC" -Value 1 -PropertyType DWORD -Force

Write-Host "."
Write-Host "uninstall complete"
PAUSE