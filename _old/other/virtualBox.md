.\VBoxManage.exe showhdinfo "P:\VM\Windows Insider\Windows Insider.vdi"
    UUID:           116384eb-dee0-4ef4-a07d-3332942d0584
    Parent UUID:    base
    State:          created
    Type:           normal (base)
    Location:       P:\VM\Windows Insider\Windows Insider.vdi
    Storage format: VDI
    Format variant: dynamic default
    Capacity:       51200 MBytes
    Size on disk:   49090 MBytes
    Encryption:     disabled
    Property:       AllocationBlockSize=1048576
    In use by VMs:  Windows Insider (UUID: cd362096-a3d1-43c6-96ba-b11a7648a8ed)

.\VBoxManage.exe modifyvm cd362096-a3d1-43c6-96ba-b11a7648a8ed --iconfile "P:\OneDrive\Icons\windows-insider.png"