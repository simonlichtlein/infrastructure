if ( (Get-NetAdapter -Name "Ethernet").Status -eq "Disabled" ) {
    Get-NetAdapter -Name "Ethernet" | Enable-NetAdapter
    Get-NetAdapter -Name "Wi-Fi" | Disable-NetAdapter
} else {
    Get-NetAdapter -Name "Ethernet" | Disable-NetAdapter
    Get-NetAdapter -Name "Wi-Fi" | Enable-NetAdapter
}