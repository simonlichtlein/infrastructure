## List all editions with index from install.wim
```
dism /Get-WimInfo /WimFile:D:\ISO_FILES\sources\install.wim
```
<details>
    <summary>Example Output</summary>
<p>

</p>
</details>

<hr><br>

## Export one windows edition from an mulit-edition install.wim
```
dism /Export-Image /SourceImageFile:install.wim /SourceIndex:5 /DestinationImageFile:install.index5.wim /DestinationName:"lichs Windows"
```
With this command you can also add other editions to the same install.wim.

<hr><br>

### Example
```
dism /Export-Image /SourceImageFile:F:\sources\install.wim /SourceIndex:5 /DestinationImageFile:install.wim /DestinationName:"My Windows 10 Enterprise"

dism /Export-Image /SourceImageFile:F:\sources\install.wim /SourceIndex:3 /DestinationImageFile:install.wim /DestinationName:"My Windows 10 Pro"

dism /Get-WimInfo /WimFile:install.wim

    Deployment Image Servicing and Management tool
    Version: 10.0.19041.844

    Details for image : install.wim

    Index : 1
    Name : My Windows 10 Enterprise
    Description : Windows 10 Enterprise
    Size : 15.501.145.240 bytes

    Index : 2
    Name : My Windows 10 Pro
    Description : Windows 10 Pro
    Size : 15.501.120.805 bytes

    The operation completed successfully.
```

<hr><br>

## create iso from sources
oscdimg.exe under C:\Program Files (x86)\Windows Kits\10\Assessment and Deployment Kit\Deployment Tools\amd64\Oscdimg after installing windows adk

winget install Microsoft.WindowsADK
```
oscdimg.exe -m -o -u2 -udfver102 -bootdata:2#p0,e,bP:\OS\ISO\boot\etfsboot.com#pEF,e,bP:\OS\ISO\efi\microsoft\boot\efisys.bin P:\OS\ISO P:\OS\ISO\21H1_19043.1081_EN-MULTI_unattend.ISO
```
# PowerShell DISM Commands

## list all windows versions from install.wim
```powershell       
Get-WindowsImage -ImagePath "P:\ISO\sources\install.wim"

ImageIndex       : 1
ImageName        : Windows 10 Pro
ImageDescription : Windows 10 Pro
ImageSize        : 17.836.320.394 bytes

ImageIndex       : 2
ImageName        : Windows 10 Enterprise
ImageDescription : Windows 10 Enterprise
ImageSize        : 17.836.332.628 bytes
[...]
```

## information of a specific windows version in install.wim
```powershell
Get-WindowsImage -ImagePath "P:\ISO\sources\install.wim" -Index 2

ImageIndex       : 2
ImageName        : Windows 10 Enterprise
ImageDescription : Windows 10 Enterprise
ImageSize        : 17.836.332.628 bytes
WIMBoot          : False
Architecture     : Unknown
Hal              :
Version          : 10.0.21390.1
SPBuild          : 1
SPLevel          : 0
EditionId        : Enterprise
InstallationType : Client
ProductType      : WinNT
ProductSuite     : Terminal Server
SystemRoot       : WINDOWS
DirectoryCount   : 19553
FileCount        : 94773
CreatedTime      : 23.05.2021 01:36:14
ModifiedTime     : 23.07.2021 21:18:18
Languages        : en-US (Default)
```

## extract specific windows version from install.wim to another (with max compression)
```powershell
Export-WindowsImage -CheckIntegrity -CompressionType "max" -SourceImagePath "P:\ISO\sources\install.wim" -SourceIndex 1 -DestinationImagePath "P:\ISO\sources\install2.wim"
```

## remove windows version from install.wim
```powershell
Remove-WindowsImage -ImagePath "P:\ISO\sources\install.wim" -Index 1 -CheckIntegrity
```

cd "P:\OS\WindowsADK\Assessment and Deployment Kit\Deployment Tools\amd64\Oscdimg"

Oscdimg -m -o -u2 -udfver102 -bootdata:2#p0,e,bP:\OS\ISO\boot\etfsboot.com#pEF,e,bP:\OS\ISO\efi\microsoft\boot\efisys.bin P:\OS\ISO P:\OS\finished.iso