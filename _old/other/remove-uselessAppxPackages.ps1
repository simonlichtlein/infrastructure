Clear-Host
Write-Host "|==============================================|"
Write-Host "|           AppxPackage uninstaller            |"
Write-Host "|" -NoNewLine 
Write-Host "----------------------------------------------" -NoNewLine -F DarkGray
Write-Host "|"
Write-Host "| " -NoNewline
Write-Host "simon@lichtlein.one" -NoNewline -F Red 
Write-Host "    -    " -NoNewline
Write-Host "gitlab.com/lichs" -NoNewline -F Red
Write-Host " |"
Write-Host "|==============================================|"
Write-Host

Get-AppxPackage Microsoft.BingWeather | Remove-AppxPackage
Get-AppxPackage Microsoft.GetHelp | Remove-AppxPackage
Get-AppxPackage Microsoft.Getstarted | Remove-AppxPackage
Get-AppxPackage Microsoft.MicrosoftOfficeHub | Remove-AppxPackage
Get-AppxPackage Microsoft.MicrosoftSolitaireCollection | Remove-AppxPackage
# Get-AppxPackage Microsoft.MicrosoftStickyNotes | Remove-AppxPackage
Get-AppxPackage Microsoft.MixedReality.Portal | Remove-AppxPackage
Get-AppxPackage Microsoft.MSPaint | Remove-AppxPackage
Get-AppxPackage Microsoft.Office.OneNote | Remove-AppxPackage
Get-AppxPackage Microsoft.People | Remove-AppxPackage
Get-AppxPackage Microsoft.SkypeApp | Remove-AppxPackage
Get-AppxPackage Microsoft.Wallet | Remove-AppxPackage
# Get-AppxPackage Microsoft.WindowsAlarms | Remove-AppxPackage
# Get-AppxPackage Microsoft.WindowsCamera | Remove-AppxPackage
Get-AppxPackage microsoft.windowscommunicationsapps | Remove-AppxPackage
Get-AppxPackage Microsoft.WindowsFeedbackHub | Remove-AppxPackage
Get-AppxPackage Microsoft.WindowsMaps | Remove-AppxPackage
Get-AppxPackage Microsoft.WindowsSoundRecorder | Remove-AppxPackage
# Get-AppxPackage Microsoft.Xbox.TCUI | Remove-AppxPackage
# Get-AppxPackage Microsoft.XboxApp | Remove-AppxPackage
# Get-AppxPackage Microsoft.XboxGameOverlay | Remove-AppxPackage
# Get-AppxPackage Microsoft.XboxGamingOverlay | Remove-AppxPackage
# Get-AppxPackage Microsoft.XboxIdentityProvider | Remove-AppxPackage
Get-AppxPackage Microsoft.XboxSpeechToTextOverlay | Remove-AppxPackage
Get-AppxPackage Microsoft.YourPhone | Remove-AppxPackage
Get-AppxPackage Microsoft.ZuneMusic | Remove-AppxPackage
Get-AppxPackage Microsoft.ZuneVideo | Remove-AppxPackage

PAUSE