$ISO = ""
$wim = ""
$esd = ""

<#
    ISO extrat with 7zip
    esd to wim
    compress
    add language
    export

    #>

$path = "P:\OS\ISO\sources"
$path = "F:\sources"

dism /Get-ImageInfo /ImageFile:"$path\install.wim"
Get-WindowsImage -ImagePath "$path\install.wim"
Get-WindowsImage -ImagePath "$path\install.wim" -Index 5

dism /Get-ImageInfo /ImageFile:"$path\install.wim" /Index:5

dism /Export-Image /SourceImageFile:"P:\OS\ISO\sources\install.esd" /sourceIndex:5 /DestinationImageFile:"P:\OS\ISO\sources\install.wim" /Compress:MAX /checkIntegrity

dism /Get-ImageInfo /ImageFile:"P:\OS\ISO\sources\install.wim" /Index:1

Dism /Mount-Image /ImageFile:"P:\OS\ISO\sources\install.wim" /Index:1 /MountDir:"P:\OS\mount"

Dism /Image:"P:\OS\mount" /Add-Package /PackagePath="E:\LanguagesAndOptionalFeatures\Microsoft-Windows-Client-Language-Pack_x64_en-gb.cab"
Dism /Image:"P:\OS\mount" /Add-Package /PackagePath="P:\OS\22621.1_Microsoft-Windows-Client-LanguagePack-Package_en-us-amd64-en-us.esd"


Dism /Image:"P:\OS\mount" /Get-Packages /format:table

Dism /Commit-Image /MountDir:"P:\OS\mount"
