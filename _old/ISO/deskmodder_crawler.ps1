function Parse-Items($items) {
    ForEach ($item in $items.Matches) {
        Write-Host "."
        Write-Host "Descr: "$item.Groups[1].Value $item.Groups[2].Value
        Write-Host "Link: "$item.Groups[3].Value.Split("rel")[0]
    }
}

$html = Invoke-WebRequest 'https://www.deskmodder.de/blog/' -Headers @{"Cache-Control" = "no-cache" }

$w11_links = ($html.Links | Where { ($_.href -like '*/blog/*/*windows-11*iso-esd*') -and ($_.href -notlike "*#comments*" ) } | Select href -Unique)

$w11_insider = $w11_links[0].href
$w11_sac = $w11_links[1].href

Write-Host $w11_insider
Write-Host $w11_sac

$w11_sac_html = (Invoke-WebRequest "https://www.deskmodder.de/$w11_sac").Content.replace("`n", "").replace("`r", "").Replace("`t", "").Trim()

Write-Host ""
Write-Host "weiterex64"
Write-Host "..........."

$w11_sac_weitere_items = (($w11_sac_html | Select-String -Pattern '<h3><a id="weiterex64"></a>(.*?)<h3>').Matches.Groups[0].Value | Select-String -Pattern '<li><strong>(.*?)<\/strong>(.*?)<a(.*?)<\/li>' -AllMatches)

Parse-Items $w11_sac_weitere_items


Write-Host ""
Write-Host "ISOeng"
Write-Host "..........."

$w11_sac_eng_items = (($w11_sac_html | Select-String -Pattern '<h3><a id="ISOeng"></a>(.*?)<h3>').Matches.Groups[0].Value | Select-String -Pattern '<li><strong>(.*?)<\/strong>(.*?)<a(.*?)<\/li>' -AllMatches)

Parse-Items $w11_sac_eng_items
Exit





$w10_links = ($html.Links | Where { $_.href -like '/blog/*/windows-10-*-iso-esd-*' } | Select href)

$w10_sac = $w10_links[0].href

Write-Host "W11 Insider $w11_insider"
Write-Host "W11 SAC     $w11_sac"
Write-Host "W10 SAC     $w10_sac"

Write-Host "..........."










# $w11_sac_html = $w11_sac_html -Split "`n"




# $selection_array = @()

# $w11_sac_html | Select-String -Pattern "<li><strong>(.*?)<\/strong>(.*?)<a(.*?)<\/li>" | ForEach { 
#     Write-Host $_.Matches.Groups[1].Value 
#     Write-Host "-"
# }

$html = Invoke-WebRequest 'https://www.deskmodder.de/blog/'

$w11_links = ($html.Links | Where { $_.href -like '/blog/*/windows-11-*-iso-esd-*' } | Select href)

$w11_insider = $w11_links[0].href
$w11_sac = $w11_links[1].href

$w11_sac_html = (Invoke-WebRequest "https://www.deskmodder.de/$w11_sac").Content
$w11_sac_html = $w11_sac_html.replace("`n", "").replace("`r", "").Replace("`t", "").Trim()

function Parse-Items($items) {
    ForEach ($item in $items.Matches) {
        Write-Host ""
        Write-Host "Descr: "$item.Groups[1].Value $item.Groups[2].Value
        Write-Host "Link: "$item.Groups[3].Value.Split("rel")[0]
    }
}

<#

HPx64 - Windows 11 22000 ISO x64 deutsch inkl. Updates
weiterex64 - Windows 11 22000 ISO x64 deutsch weitere inkl. Updates
ARM - Windows 11 22000 ISO ARM deutsch inkl. Updates
ISOeng - Windows 11 22000 ISO english inkl. Updates
MVS - Windows 11 22000 MVS (MSDN) Version
Ur - Windows 11 22000.1 als „Urversion“
ESD - Windows 11 ESD 22000.318 (vollständig)


#>

# $w11_sac_html = $w11_sac_html -Split "`n"
Write-Host ""
Write-Host "weiterex64"

$weitere = ($w11_sac_html | Select-String -Pattern '<h3><a id="weiterex64"></a>(.*?)<h3>').Matches.Groups[0].Value
$weitere_name = ($weitere | Select-String -Pattern '</a>(.*?)</h3>').Matches.Groups[0].Value
$weitere_items = ($weitere | Select-String -Pattern '<li><strong>(.*?)<\/strong>(.*?)<a(.*?)<\/li>' -AllMatches)
Write-Host $weitere_name
Write-Host "..........."

Parse-Items $weitere_items

Write-Host ""
Write-Host "ISOeng"
$weitere = ($w11_sac_html | Select-String -Pattern '<h3><a id="ISOeng"></a>(.*?)<h3>').Matches.Groups[0].Value
$weitere_name = ($weitere | Select-String -Pattern '</a>(.*?)</h3>').Matches.Groups[0].Value
$weitere_items = ($weitere | Select-String -Pattern '<li><strong>(.*?)<\/strong>(.*?)<a(.*?)<\/li>' -AllMatches)

Write-Host $weitere_name
Write-Host "..........."
Parse-Items $weitere_items