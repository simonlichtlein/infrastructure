[CmdletBinding()]
param (
    # 1 = server 2 = windows 11 3 = windows 10
    [Parameter()]
    [ValidateSet(1, 2, 3)]
    [Int16]
    $type = 2,

    [Parameter()]
    [Boolean]
    $iso = $true,

    [Parameter()]
    [ValidateSet("en-US", "de-DE")]
    [String]
    $lang = "en-US"
)

$downloadLocation = "D:\OS\"

switch ($type) {
    1 { $page = "https://www.microsoft.com/de-de/evalcenter/download-windows-server-2022" }
    2 { $page = "https://www.microsoft.com/de-de/evalcenter/download-windows-11-enterprise" }
    3 { $page = "https://www.microsoft.com/de-de/evalcenter/download-windows-10-enterprise" }
    Default { Exit }
}

switch ($iso) {
    $true { $iso_s = "ISO" }
    $false { $iso_S = "VHD" }
    Default { Exit }
}

$baseurl = ((Invoke-WebRequest $page).Links | where aria-label -clike "*$iso_S*($lang)*").href

$curloutput = "curl -s -i -D -X POST $baseurl"

$filename = $False

foreach ($line in $curloutput) {
    if ($line -match "Location:*") {
        $filename = $line.Split("Location: ")[1].Split("/")[-1]
        break;
    }
}

if ($filename) {
    Write-Host -F Green $filename
    Start-BitsTransfer -Source $baseurl -Destination "$downloadLocation\$filename"
}
else {
    Write-Host -F Red "Location not found"
}

Remove-Item "-X" -Force