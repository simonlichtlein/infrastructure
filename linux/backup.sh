USER="$(whoami)"
HOME="/home/$USER"

${HOME}/.ssh/
${HOME}/.thunderbird
${HOME}/.config/dolphinrc
${HOME}/.local/share/user-places.xbel
${HOME}/.alacritty.toml
