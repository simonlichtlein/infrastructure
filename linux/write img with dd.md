decompress an image
```bash
xz --decompress v2-hdmi-rpi4-latest.img.xz
```

delete all partitions of a target disk
```bash
sudo sfdisk --delete /dev/sda
```

```bash
$ lsblk
NAME FSTYPE FSVER LABEL UUID FSAVAIL FSUSE% MOUNTPOINTS
sda
(...)
```

check what image to copy
```bash
$ dir *img
.rw-r--r--@ 897Mi sl : 2024-10-06 16:19  DietPi_RPi-ARMv6-Bookworm.img
.rw-r--r--@ 852Mi sl : 2024-10-06 16:19  DietPi_RPi-ARMv8-Bookworm.img
.rw-r--r--@ 8.5Gi sl : 2024-10-06 16:20  v1-hdmiusb-rpi3-latest.img
.rw-r--r--@ 8.5Gi sl : 2024-10-06 16:20  v2-hdmiusb-rpi4-latest.img
```

copy the image to the disk
```bash
sudo dd if=v2-hdmiusb-rpi4-latest.img of=/dev/sda bs=4M status=progress
```
