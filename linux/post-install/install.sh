#!/bin/bash

# **** PATHS FOR FILES TO INTERGRATE **** #
TIMEZONE="Europe/Berlin"
SSHKEYS="https://lichtlein.dev/inf/keys.pub"

# file and savename of the certs will be $(basename $CRT)
CACRT="https://lichtlein.dev/inf/lichtlein.dev-CA.crt"
CAINTERCRT="https://lichtlein.dev/inf/lichtlein.dev-interCA.crt"

SUDOCONF="https://lichtlein.dev/inf/linux/config/sudoconf"
SUDOLECTURE="https://lichtlein.dev/inf/linux/config/sudoers.lecture"
MOTD="https://lichtlein.dev/inf/linux/config/motd"

ZSHRC="https://lichtlein.dev/inf/linux/config/zshrc"
BASHRC="https://lichtlein.dev/inf/linux/config/bashrc"

# **** TOOLS TO BE INSTALLED **** #
TOOLS=(openssl man-db gpg make net-tools dnsutils git curl wget eza btop bat figlet vim zsh chrony tmux ca-certificates clinfo clang pwgen ncdu rsync unzip jq)
TOOLS_FEDORA=(dnf-plugins-core dnf-plugin-system-upgrade java-latest-openjdk-headless nc fastfetch)
TOOLS_DEBIAN=(default-jre-headless)

# ! **** BEGIN **** ! #
if ! command -v curl &>/dev/null; then
  # wget -qO- "https://lichtlein.dev/inf/linux/post-install/module.sh" >module.sh
  echo "curl is not installed - please install curl and run this script again"
  exit 3
else
  curl -s "https://lichtlein.dev/inf/linux/post-install/module.sh" >module.sh
fi
chmod +x module.sh
source ./module.sh
pre

# **** START **** #
mkdir -p "${HOME}/.ssh"
if [ -n "$SSHKEYS" ]; then
  printc -c $darkgray -t "  adding ssh keys"
  if curl --output /dev/null --silent --head --fail "$SSHKEYS"; then
    curl -s "$SSHKEYS" >"${HOME}/.ssh/authorized_keys"
    chmod 700 "${HOME}/.ssh"
    chmod 600 "${HOME}/.ssh/authorized_keys"
  else
    printc -c $red -t "    SSH keys URL is not reachable"
    printc -C $lightred -t "  continue anyway"
  fi
fi

# add CA and inter CA cert
printc -c $darkgray -t "  adding custom CA and inter-CA certs"
CACRTNAME=$(basename $CACRT)
CAINTERCRTNAME=$(basename $CAINTERCRT)

if $FEDORA; then
  sudo sh -c "curl -s $CACRT > '/etc/pki/ca-trust/source/anchors/$CACRTNAME'"
  printc -c $darkgray -t "    added  $CACRT as $CACRTNAME"
  sudo sh -c "curl -s $CAINTERCRT > '/etc/pki/ca-trust/source/anchors/$CAINTERCRTNAME'"
  printc -c $darkgray -t "    added  $CAINTERCRT as $CAINTERCRTNAME"
  sudo update-ca-trust >/dev/null 2>&1
  printc -c $darkgray -t "    updated ca-trust"
else
  sudo sh -c "curl -s $CACRT > '/usr/local/share/ca-certificates/$CACRTNAME'"
  printc -c $darkgray -t "    added  $CACRT as $CACRTNAME"
  sudo sh -c "curl -s $CAINTERCRT > '/usr/local/share/ca-certificates/$CAINTERCRTNAME'"
  printc -c $darkgray -t "    added  $CAINTERCRT as $CAINTERCRTNAME"
  sudo update-ca-certificates >/dev/null 2>&1
  printc -c $darkgray -t "    updated ca-trust"
fi

# install tools
# change apt http sources to https
if $DEBIAN; then
  printc -c $darkgray -t "  changing debian http sources to https"
  sudo sed -i 's/http:/https:/g' /etc/apt/sources.list
fi

printc -c $darkgray -t "  upgrading system"
if $FEDORA; then
  sudo dnf makecache >/dev/null 2>&1
  sudo dnf check-update >/dev/null 2>&1
  sudo dnf upgrade -y >/dev/null 2>&1
  sudo dnf makecache >/dev/null 2>&1
  printc -c $darkgray -t "  installing tools itself"
  sudo dnf install -y ${TOOLS[@]} ${TOOLS_FEDORA[@]} >/dev/null 2>&1
else
  # need to install gpg for debian first
  sudo apt install gpg -y >/dev/null 2>&1
  trusted_gpg="/etc/apt/trusted.gpg.d"
  sources_list="/etc/apt/sources.list.d"

  # eza - check if repo is available (< debian 12 / ubuntu 24)
  if ! dpkg -s eza >/dev/null 2>&1; then
    keyring="gierens.gpg"
    wget -qO- https://raw.githubusercontent.com/eza-community/eza/main/deb.asc | sudo gpg --batch --yes --dearmor -o $trusted_gpg/$keyring
    echo "deb [signed-by=$trusted_gpg/$keyring] http://deb.gierens.de stable main" | sudo tee $sources_list/gierens.list >/dev/null
    sudo chmod 644 $trusted_gpg/$keyring $sources_list/gierens.list
  fi

  sudo apt update >/dev/null 2>&1
  sudo apt upgrade -y >/dev/null 2>&1

  printc -c $darkgray -t "  installing tools itself"
  sudo apt install -y ${TOOLS[@]} ${TOOLS_DEBIAN[@]} >/dev/null 2>&1
fi

# configure network time
printc -c $darkgray -t "  configuring network time with chrony"
if $FEDORA; then
  chronyfile="/etc/chrony.conf"
else
  chronyfile="/etc/chrony/chrony.conf"
fi

printc -c $darkgray -t "    removing existing ntp pool servers"
sudo sed -i '/^pool/d' $chronyfile
printc -c $darkgray -t "    adding default pool.ntp.org"
echo "server pool.ntp.org iburst" | sudo tee -a $chronyfile >/dev/null

# set timezone
printc -c $darkgray -t "    setting timezone to $TIMEZONE"
sudo timedatectl set-timezone $TIMEZONE

# ? https://itsfoss.com/wrong-time-dual-boot/
printc -c $darkgray -t "    seting local-rtc to 1"
sudo timedatectl set-local-rtc 1
printc -c $darkgray -t "    restart chronyd"
sudo systemctl restart chronyd

# add custom sudo config
printc -c $darkgray -t "  adding custom sudo config"
sudo sh -c "curl -s $SUDOCONF > '/etc/sudoers.d/10-sudo'"
sudo sh -c "curl -s $SUDOLECTURE > '/etc/sudoers.d/sudoers.lecture'"

# add custom motd
printc -c $darkgray -t "  adding motd ssh file"
sudo rm -f /etc/motd
if $FEDORA; then
  sudo sh -c "curl -s $MOTD > /etc/profile.d/mymotd.sh" >/dev/null
  sudo chmod +x /etc/profile.d/mymotd.sh
else
  # sudo mkdir -p /etc/motd.d
  # sudo sh -c "curl -s $MOTD > '/etc/motd.d/10-uname'" >/dev/null
  # sudo chmod 755 /etc/motd.d/10-uname
  sudo sh -c "curl -s $MOTD > '/etc/update-motd.d/10-uname'" >/dev/null
  sudo chmod 755 /etc/update-motd.d/10-uname
  sudo bash -c 'run-parts --lsbsysinit /etc/update-motd.d > /run/motd.dynamic'

fi

printc -c $darkgray -t "  change PrintMotd to yes in sshd_config"
sudo sed -i 's/^PrintMotd no/PrintMotd yes/' /etc/ssh/sshd_config

# vimrc
printc -c $darkgray -t "  configuring vim with awesome vimrc"
sudo rm -rf ${HOME}/.vim_runtime
git clone --depth=1 https://github.com/amix/vimrc.git ${HOME}/.vim_runtime >/dev/null 2>&1
sh ${HOME}/.vim_runtime/install_awesome_vimrc.sh

sudo rm -rf /root/.vim_runtime
sudo git clone --depth=1 https://github.com/amix/vimrc.git /root/.vim_runtime >/dev/null 2>&1
sudo sh /root/.vim_runtime/install_awesome_vimrc.sh

# zsh
printc -c $darkgray -t "  configuring zsh with oh-my-zsh"
printc -c $lightgray -t "   changing default shell to zsh for user $USER"
sudo rm -rf ${HOME}/.oh-my-zsh
sh -c "$(wget -O- https://install.ohmyz.sh/)" "" --unattended
sudo chsh -s "$(which zsh)" "$USER"

printc -c $lightgray -t "   add zshrc and bashrc"
curl -s $ZSHRC >${HOME}/.zshrc
curl -s $BASHRC >${HOME}/.bashrc

printc
printc -c $lightgreen -t "🎉 post-install finished 🎉"

printinfo
rm module.sh
exit 0
