sudo os-prober

THEMENAME="Elegant-mojave-window-left-dark"
THEMEPATH="/home/sl/wks/infrastructure/linux/grub/themes/Elegant-mojave-window-left-dark"

sudo cp -r $THEMEPATH /boot/grub2/themes/Elegant-mojave-window-left-dark

echo 'GRUB_THEME="/boot/grub2/themes/Elegant-mojave-window-left-dark/theme.txt"' | sudo tee -a /etc/default/grub

sudo sed -i 's/^GRUB_TERMINAL_OUTPUT=.*/GRUB_TERMINAL_OUTPUT="gfxterm"/' /etc/default/grub

sudo grub2-mkconfig -o /boot/grub2/grub.cfg
