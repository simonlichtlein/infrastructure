#!/bin/bash
printc -c $darkgray -t "    installing Telegram"
sudo flatpak install -y flathub org.telegram.desktop
printc -c $darkgray -t "    installing Spotify"
sudo flatpak install -y flathub com.spotify.Client

# AusweisApp2 necessary for  to connect to smartphone card reader
printc -c $darkgray -t "    installing AusweisApp2"
sudo flatpak install -y flathub de.bund.ausweisapp.ausweisapp2
printc -c $darkgray -t "        adding firewall rules (24727/tcp+udp)"
sudo firewall-cmd --permanent --zone=public --add-port=24727/udp

DOLPHINPLACES="https://lichtlein.dev/inf/linux/config/dolphin/user-places.xbel"
curl -s $DOLPHINPLACES >${HOME}/.local/share/user-places.xbel
