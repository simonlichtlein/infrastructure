#!/bin/bash

# ! **** BEGIN **** ! #
if ! command -v curl &>/dev/null; then
    echo "curl is not installed - please install curl and run this script again"
    exit 3
else
    curl -sSL https://lichtlein.dev/inf/linux/post-install/install.sh | bash
    curl -s "https://lichtlein.dev/inf/linux/post-install/module.sh" >module.sh
fi

chmod +x module.sh
source ./module.sh
pre

# **** CONFIG **** #
DRIVER=(akmod-nvidia xorg-x11-drv-nvidia-cuda libva-nvidia-driver xorg-x11-drv-nvidia-cuda-libs libva-utils vdpauinfo mesa-va-drivers vulkan)

UI_TOOLS=(alacritty wireguard-tools flatpak java-latest-openjdk thunderbird xca fastfetch virt-viewer plasma-browser-integration cpu-x gsmartcontrol kdiskmark vorta borgbackup)

DOLPHINRC="https://lichtlein.dev/inf/linux/config/dolphin/dolphinrc"
curl -s $DOLPHINRC >${HOME}/.config/dolphinrc
ALACRITTYCONF="https://lichtlein.dev/inf/linux/config/alacritty.toml"
curl -s $ALACRITTYCONF >${HOME}/.alacritty.toml

printc -c $darkgray -t "  creating directories"
for dir in wks VMs tools; do
    mkdir -p "${HOME}/${dir}"
done

printc -c $darkgray -t "  adding rpmfusion repos"
sudo dnf install -y https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm >/dev/null 2>&1
sudo dnf install -y https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm >/dev/null 2>&1

# drivers
printc -c $darkgray -t "  installing drivers"
sudo dnf install -y ${DRIVER[@]} >/dev/null 2>&1
sudo dnf mark install akmod-nvidia >/dev/null 2>&1
printc

# programs
printc -c $darkgray -t "  installing and configuring programs"
printc -c $darkgray -t "    removing google chrome repo"
if [ -f /etc/yum.repos.d/google-chrome.repo ]; then
    sudo rm /etc/yum.repos.d/google-chrome.repo >/dev/null 2>&1
fi

printc -c $darkgray -t "    adding brave browser repo"
sudo rpm --import https://brave-browser-rpm-release.s3.brave.com/brave-core.asc --overwrite >/dev/null 2>&1
sudo dnf config-manager addrepo --from-repofile=https://brave-browser-rpm-release.s3.brave.com/brave-browser.repo >/dev/null 2>&1

UI_TOOLS+=(brave-browser brave-browser-beta)

sudo rpm --import https://brave-browser-rpm-beta.s3.brave.com/brave-core-nightly.asc --overwrite >/dev/null 2>&1
sudo dnf config-manager addrepo --from-repofile=https://brave-browser-rpm-beta.s3.brave.com/brave-browser-beta.repo >/dev/null 2>&1

printc -c $darkgray -t "    adding VScode repo"
sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc --overwrite >/dev/null 2>&1
echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" | sudo tee /etc/yum.repos.d/vscode.repo >/dev/null 2>&1

UI_TOOLS+=(code)

printc -c $darkgray -t "    adding owncloud repo"
sudo rpm --import https://download.owncloud.com/desktop/ownCloud/stable/latest/linux/Fedora_39/repodata/repomd.xml.key --overwrite >/dev/null 2>&1
sudo dnf config-manager addrepo --from-repofile=https://download.owncloud.com/desktop/ownCloud/stable/latest/linux/Fedora_39/owncloud-client.repo >/dev/null 2>&1
UI_TOOLS+=(owncloud-client)

printc -c $darkgray -t "    dnf install"
sudo dnf makecache >/dev/null 2>&1
sudo dnf install -y ${UI_TOOLS[@]} >/dev/null 2>&1

printc -c $darkgray -t "    add java.desktop entry"
echo "
[Desktop Entry]
Name=Java
Comment=Java
GenericName=Java
Keywords=java
Exec=java -jar %f
Terminal=false
X-MultipleArgs=false
Type=Application
MimeType=application/x-java-archive
StartupNotify=true
" | sudo tee /usr/share/applications/java.desktop >/dev/null

printc -c $darkgray -t "  installing flatpacks"
sudo flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo >/dev/null 2>&1

printc -c $darkgray -t "    installing MissionCenter"
sudo flatpak install -y flathub io.missioncenter.MissionCenter >/dev/null 2>&1

printc -c $darkgray -t "    installing Switcheroo"
sudo flatpak install -y flathub io.gitlab.adhami3310.Converter >/dev/null 2>&1

# localsend needs port 53317 tcp and udp openm
printc -c $darkgray -t "    installing LocalSend"
sudo flatpak install -y flathub org.localsend.localsend_app >/dev/null 2>&1
printc -c $darkgray -t "        adding firewall rules (53317/tcp+udp)"
sudo firewall-cmd --zone=public --permanent --add-port=53317/tcp >/dev/null 2>&1
sudo firewall-cmd --zone=public --permanent --add-port=53317/udp >/dev/null 2>&1
# other
printc -c $darkgray -t "  adding wireguard firewall rule"
sudo firewall-cmd --permanent --add-service=wireguard --zone=public >/dev/null 2>&1
# reload firewall
sudo firewall-cmd --reload >/dev/null 2>&1

# multimedia support
sudo dnf install -y libavcodec-freeworld x264-libs x265-libs libde265 >/dev/null 2>&1
sudo dnf group install -y --with-optional "sound-and-video" >/dev/null 2>&1

# change Region Settings to German
echo "
[Formats]
LANG=en_US.UTF-8
LC_ADDRESS=de_DE.UTF-8
LC_MEASUREMENT=de_DE.UTF-8
LC_MONETARY=de_DE.UTF-8
LC_NAME=de_DE.UTF-8
LC_NUMERIC=de_DE.UTF-8
LC_PAPER=de_DE.UTF-8
LC_TELEPHONE=de_DE.UTF-8
LC_TIME=de_DE.UTF-8
" | tee ${HOME}/.config/plasma-localerc >/dev/null

# fonts
printc -c $darkgray -t "  installing fonts"
fontpathroot="/usr/share/fonts/"

printc -c $darkgray -t "    downloading SourceCodePro font"
fonturl="https://github.com/ryanoasis/nerd-fonts/releases/latest/download/SourceCodePro.tar.xz"
fontdir="$fontpathroot/SourceCodePro"
sudo mkdir -p $fontdir
sudo curl -L -o $fontdir/SourceCodePro.tar.xz $fonturl >/dev/null 2>&1
sudo tar -xf $fontdir/SourceCodePro.tar.xz -C $fontdir >/dev/null 2>&1
sudo rm $fontdir/SourceCodePro.tar.xz >/dev/null 2>&1

printc -c $darkgray -t "    updating font cache"
sudo fc-cache -fv >/dev/null 2>&1

printc
printc -c $lightgreen -t "🎉 fedora kde post-install finished 🎉"

printinfo
rm module.sh
exit 0
