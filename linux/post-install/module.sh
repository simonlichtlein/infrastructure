#!/bin/bash

# **** PRE **** #
function pre() {
    source /etc/os-release
    USER="$(whoami)"
    HOME="/home/$USER"

    detectOS
    installprereqs

    if [ "$(whoami)" = "root" ]; then
        printc -c $bold$red -t "🚨 please do not run this as root - it is only intended in user context 🚨"
        printc -c $red -t "all changes that need root privileges will be done with sudo and you will be asked for your password"
        exit 3
    else
        printc -c $darkgray -t "  pre authing with sudo - please enter your password"
        sudo su -c "echo 'pre authed'"
        printc -c $blue -t "🚀 This script will install some basic tools and configure the fedora or debian system for nicer usage"
        printc -c $red -t "   🚨 Please use with caution if you arent me ;) 🚨"
    fi

    printc
    printinfo
}
export pre

function detectOS() {
    if grep -q '^ID=fedora' /etc/os-release; then
        printc -c $darkgray -t "  fedora system"
        FEDORA=true
        DEBIAN=false
    elif grep -q '^ID=debian' /etc/os-release; then
        printc -c $darkgray -t "  debian system 🍥"
        FEDORA=false
        DEBIAN=true
    elif grep /q '^ID=ubuntu' /etc/os-release; then
        printc -c $darkgray -t "  ubuntu system"
        FEDORA=false
        DEBIAN=true
    else
        printc -c $bold$red -t "not using fedora or debian (or ubuntu) - abort"
        exit 3
    fi
}

# ! this doesnt work. the su -c doesnt wait for the password input
function installprereqs() {
    if ! command -v curl &>/dev/null; then
        printc -c $darkgray -t "  installing curl"
        if $FEDORA; then
            su -c "dnf install curl -y"
        else
            su -c "apt install curl -y"
        fi
    fi

    if ! (command -v sudo >/dev/null 2>&1); then
        printc -c $lightred -t "sudo is not installed.."
        printc -c $magenta -t "  installing sudo with 'su'"
        if $FEDORA; then
            su -c "dnf install sudo -y"
            printc -c $magenta -t "  adding user to sudo group"
            su -c "usermod -aG wheel $USER"
        else
            su -c "apt install sudo -y"
            printc -c $magenta -t "  adding user to sudo group"
            su -c "usermod -aG sudo $USER"
        fi
        printc -c $magenta -t "  please relogin to apply changes and run this script again"
        exit 2
    fi

    if ! (sudo -l >/dev/null 2>&1); then
        printc -c $red -t "User does not have sudo privileges - abort"
        exit 3
    fi
}

# **** VARS **** #
# font styles
export bold="\e[1m"
export dim="\e[2m"
export italic="\e[3m"
export underline="\e[4m"
export blink="\e[5m"
export overline="\e[6m"
export invert="\e[7m"
export hidden="\e[8m"
export strikeout="\e[9m"

# foreground colors
export default="\e[39m"
export white="\e[97m"
export black="\e[30m"
export red="\e[31m"
export green="\e[32m"
export yellow="\e[33m"
export blue="\e[34m"
export magenta="\e[35m"
export cyan="\e[36m"
export lightgray="\e[37m"
export darkgray="\e[90m"
export lightred="\e[91m"
export lightgreen="\e[92m"
export lightyellow="\e[93m"
export lightblue="\e[94m"
export lightmagenta="\e[95m"
export lightcyan="\e[96m"

# background
export b_default="\e[49m"
export b_white="\e[30;107m"
export b_black="\e[40m"
export b_red="\e[41m"
export b_green="\e[42m"
export b_yellow="\e[43m"
export b_blue="\e[44m"
export b_magenta="\e[45m"
export b_cyan="\e[46m"
export b_darkgray="\e[30;100m"
export b_lightgray="\e[30;47m"
export b_lightred="\e[30;101m"
export b_lightgreen="\e[30;102m"
export b_lightyellow="\e[30;103m"
export b_lightblue="\e[30;104m"
export b_lightmagenta="\e[30;105m"
export b_lightcyan="\e[30;106m"

# reset
reset_all="\e[0m"

function printc() {
    style=""
    color=""
    text=""
    middle=false
    nonewline=false
    OPTIND=1

    while getopts "s:c:t:n:m:" flag; do
        case $flag in
        s) style=${OPTARG} ;;
        c) color=${OPTARG} ;;
        t) text=${OPTARG} ;;
        n) nonewline=${OPTARG} ;;
        m) middle=${OPTARG} ;;
        esac
    done

    if [ "$middle" = true ]; then
        terminal_width=$(tput cols)
        text_length=${#text}
        padding_length=$(((terminal_width - text_length) / 2 - 1))
        padding=$(printf '%*s' $padding_length)
        text="${padding}${text}${padding}"
    fi

    if [ "$nonewline" = true ]; then
        echo -ne "${style}${color}${text}${reset_all}"
    else
        echo -e "${style}${color}${text}${reset_all}"
    fi
}
export printc

function printl() {
    printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' -
}
export printl

function printinfo() {
    printc
    printl
    printc -c $lightcyan -m true -t "💡 lichtlein.dev | vires in numeris | simon@lichtlein.dev 💡"
    printl
}
export printinfo
