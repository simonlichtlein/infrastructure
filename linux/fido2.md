# FIDO2 authentication for Linux
Im using Fedora 41 with KDE Plasma 6

To get an FIDO2 key working for Linux login and sudo auth you only need a thew tools and commands.

I personally use an Yubikey Security Key NFC.

<span style="color:red">Please note that Yubikey Series 5 could have a security risk</span> due to the use of security chips from Infineon which have a vulnerability and can't be patched because they sold Yubikey the idea that you shouldn't be able to update the firmware for security reasons.
More information on this german article: [blog.fefe.de](https://blog.fefe.de/?ts=99ccc8dc)

### create the fido key pin
This can be established with `fido2-tools` or the `yubikey-manager-qt` then using Yubikeys.

```bash
# im using ~/tools as my tools directory
mkdir -p ~/tools

# download latest AppImage from yubikey website
curl -L -o ~/tools/yubikey-manager-qt.AppImage https://developers.yubico.com/yubikey-manager-qt/Releases/yubikey-manager-qt-latest-linux.AppImage

chmod +x ~/tools/yubikey-manager-qt.AppImage

~/tools/yubikey-manager-qt.AppImage
```
Here goto `Applications` -> `FIDO2` and set your PIN
![image](fido2.png)

Now install the required packages for linux

```bash
sudo dnf install pam-u2f pamu2fcfg 
```

Add your key to the system. It doesnt matter in what directory you save the key. It just simply a file with the keyphrase in it and it will be pasted into the pam config file by `pamu2fcfg`.
```bash
mkdir -p ~/.config/Yubico
pamu2fcfg --pin-verification > ~/.config/Yubico/u2f_keys
```

Now Select the auth process to use sssd with u2f 

**NOTE: on my system the Password Login still works IF the key isnt plugged in**
```bash
sudo authselect select sssd with-pam-u2f with-fingerprint

# show if changes succeeded
authselect current
```