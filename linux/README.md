# infrastructure - linux

# post install
## prerequesists

```bash
ssh sl@debian.lan
su -
apt install -y sudo wget curl
usermod -aG sudo sl
# Ctrl+D exit root
# Ctrl+D exit/logoff sl
```

## start all in one script
```shell
sudo sh -c "$(wget --no-check-certificate --no-cache --no-cookies -O- https://lichtlein.dev/pub/linux/init.sh)"
```
for testing
```shell
curl -s -O http://cwp-war.lan:5500/infrastructure/linux/init.sh
./init.sh
```